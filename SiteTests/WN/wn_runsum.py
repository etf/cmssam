#!/usr/bin/python3
# ########################################################################### #
#
# SAM WorkerNode Apptainer/Singularity runner and summary evaluation script
#
# ########################################################################### #



import os, sys
import io
import platform
import time, calendar
import signal
import socket
import re
import pwd
import subprocess
import json
import random
import shutil
import traceback
import argparse
# ########################################################################### #



# global variables:
SWNRS_VERSION = "v1.01.09"
#
SWNRS_PROBE_LIST = {
    'x86_64': { 'prerq': [ "wn_basic", "wn_cvmfs", "wn_siteconf",
                           "wn_apptainer" ],
                'run':   [ "rhel6", "rhel7", "rhel8"],
                'rhel6': [ ("tests/wn_squid.sh", "", True),
                           ("tests/wn_dataaccess.py", "-S", True) ],
                'rhel7': [ ("tests/wn_dataaccess.py", "-S", True) ],
                'rhel8': [ ("tests/wn_frontier.sh", "-S", True),
                           ("tests/wn_dataaccess.py", "-S", True) ],
                'sumry': [ "wn_basic", "wn_cvmfs", "wn_siteconf",
                           "wn_apptainer",
                           "wn_squid", "wn_frontier", "wn_dataaccess" ] },
    'ppc64le': { 'prerq': [ "wn_basic", "wn_cvmfs", "wn_siteconf" ],
                 'run':   [ "on-os"],
                 'on-os': [ ("tests/wn_dataaccess.py", "-S", True) ],
                 'sumry': [ "wn_basic", "wn_cvmfs", "wn_siteconf",
                           "CE-cms-squid", "wn_dataaccess" ] },
    'aarch64': { 'prerq': [ "wn_basic", "wn_cvmfs", "wn_siteconf" ],
                 'run':   [ "on-os"],
                 'on-os': [ ("tests/wn_dataaccess.py", "-S", True) ],
                 'sumry': [ "wn_basic", "wn_cvmfs", "wn_siteconf",
                           "CE-cms-squid", "wn_dataaccess" ] }
    }
#
SWN_MODE = "interactive"
SWN_VERBOSITY = 0
SWN_STDOUT = None
SWN_STDERR = None
SWN_STRINGIO = None
#
SWNRS_MARCH = None
SWNRS_SRVC_HOST = None
SWNRS_SRVC_CE = None
SWNRS_JSON_DIR = None
SWNRS_STARTTIME = None
SWNRS_ENDTIME = None
#
SWNRS_TIMEOUT_REACHED = False
#
SWNRS_APPTNR_EXEC = None
SWNRS_APPTNR_BIND = None
SWNRS_APPTNR_ENV = {}
SWNRS_APPTNR_IMG_STEM = "${CVMFS_MOUNT_DIR}/singularity.opensciencegrid.org/cmssw/cms:"
SWNRS_APPTNR_POPEN = []
# ########################################################################### #



# CMS SAM Worker Node definitions:
class swn:
    unknown = -1
    ok = 0
    warning = 1
    error = 2
    critical = 3

    @staticmethod
    def code2name(code):
        swnNames = ["ok", "warning", "error", "critical"]
        if (( code >= 0 ) and ( code < len(swnNames) )):
            return swnNames[code]
        else:
            return "unknown"

    @staticmethod
    def code2sam(code):
        samRC = [ 0, 1, 2, 2 ]
        if (( code >= 0 ) and ( code < len(samRC) )):
            return samRC[code]
        else:
            return 3

    @staticmethod
    def sam2name(code):
        samNames = ["OK", "WARNING", "CRITICAL"]
        if (( code >= 0 ) and ( code < len(samNames) )):
            return samNames[code]
        else:
            return "UNKNOWN"

    @staticmethod
    def samName2code(name):
        samNames = ["OK", "WARNING", "CRITICAL"]
        try:
            return samNames.index(name)
        except ValueError:
            return 3

    def samName2swnCode(name):
        samNames = ["OK", "WARNING", "CRITICAL"]
        try:
            return samNames.index(name)
        except ValueError:
            return swn.unknown

    @staticmethod
    def samName2order(name):
        samCodeSeverity = [ "OK", "WARNING", "UNKNOWN", "CRITICAL"]
        try:
            return samCodeSeverity.index(name)
        except ValueError:
            return samCodeSeverity.index("UNKNOWN")

    @staticmethod
    def code2psst(code):
        psstRC = [ 0, 0, 0, 1 ]
        if (( code >= 0 ) and ( code < len(psstRC) )):
            return psstRC[code]
        else:
            return 0

    @staticmethod
    def code2order(code):
        codeSeverity = [ swn.ok, swn.warning, swn.unknown, \
                                                      swn.error, swn.critical ]
        try:
            return codeSeverity.index(code)
        except ValueError:
            return codeSeverity.index(swn.unknown)


    @staticmethod
    def log(level, indent, message):
        levelNames = { 'C': "Critical", 'E': "Error", 'W': "Warning",
                       'N': "Notice",   'I': "Info",  'D': "Debug",
                       'X': "X-Debug",  '?': "Unknown" }
        printControl = [ ("CEW", "plain"),
                         ("CEWN", "indent"), ("CEWNI", "indent"),
                         ("CEWNID", "time"), ("CEWNIDX", "time") ]
        verbosityLevel = SWN_VERBOSITY
        #
        if (( message is None ) or ( len(message) == 0 )):
            return
        #
        msg = None
        try:
            level = levelNames[level][0]
        except:
            level = '?'
        try:
            indent = min( 8, max(0, int(indent)))
        except:
            indent = 0
        newline = 0
        while( message[0] == "\n" ):
            message = message[1:]
            newline += 1
        trailing = 0
        while( message[-1] == "\n" ):
            message = message[:-1]
            trailing += 1
        #
        if ( verbosityLevel >= 0 ):
            if ( verbosityLevel < len(printControl) ):
                if ( level not in printControl[verbosityLevel][0] ):
                    return
            else:
                verbosityLevel = len(printControl) - 1
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    if ( printControl[verbosityLevel][1] == "time" ):
                        msg += time.strftime("%H:%M:%S",
                                             time.gmtime(time.time())) \
                               + ( "   " * indent ) \
                               + ( " [%s] " % level ) + line
                    elif ( printControl[verbosityLevel][1] == "indent" ):
                        if ( level in "CEW" ):
                            msg += ( "   " * indent ) \
                                   + ( "[%s] " % level ) + line
                        else:
                            msg += ( "   " * indent ) + line
                    else:
                        msg += ( "[%s] " % level ) + line
                else:
                    if ( printControl[verbosityLevel][1] == "time" ):
                        msg += "\n" + "        " \
                               + ( "   " * indent ) \
                               + ( " [%s] " % level.lower() ) + line
                    elif ( printControl[verbosityLevel][1] == "indent" ):
                        if ( level in "CEW" ):
                            msg += "\n" + ( "   " * indent ) \
                                   + ( "[%s] " % level.lower() ) + line
                        else:
                            msg += "\n" + ( "   " * indent ) + line
                    else:
                        msg += "\n" + ( "[%s] " % level.lower() ) + line
                firstLine = False
        elif ( verbosityLevel < 0 ):
            if ( level not in "CE" ):
               return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += time.strftime("%Y-%b-%d %H:%M:%S",
                                         time.gmtime(time.time())) \
                           + ( " %s: " % levelNames[level] ) + line
                else:
                    msg += "\n                       " \
                           + ( " " * len(levelNames[level]) ) + line
                firstLine = False
        msg += "\n" * trailing
        #
        print( msg )


    @staticmethod
    def exit(code, summary):
        exitCode = None
        #
        if ( SWN_MODE == "sam" ):
            print("\n%s: %s\n</PRE>" %
                                   (swn.code2name(code).capitalize(), summary))
            #
            sys.stderr = SWN_STDERR
            sys.stdout = SWN_STDOUT
            #
            exitCode = swn.code2sam(code)
            #
            print("%s: %s\n" % (swn.sam2name(exitCode), summary))
            print( SWN_STRINGIO.getvalue() )
            SWN_STRINGIO.close()
        elif ( SWN_MODE == "psst" ):
            exitCode = swn.code2psst(code)
        else:
            exitCode = code
            #
            print("\n%s: %s" % (swn.code2name(code).upper(), summary))
        #
        sys.exit(exitCode)
# ########################################################################### #



def swnrs_signal_children(mySignal=signal.SIGKILL):
    """send a signal to all subprocesses, ignoring setuid subprocesses"""
    #
    lineRegex = re.compile(r"^\s*(\d+)\s+(\d+)\s*$")

    try:
        myEUID = str( os.geteuid() )
        myPID = os. getpid()
        #
        myCMD = ["/bin/ps", "-u", myEUID, "-o", "ppid=", "-o", "pid="]
        cmplProc = subprocess.run(myCMD, stdout=subprocess.PIPE, \
                                         stderr=subprocess.DEVNULL, \
                                         timeout=3, encoding="utf-8")
        if ( cmplProc.returncode != 0 ):
            swn.log("E", 1, ("Failed to run /bin/ps to get subprocesses," + \
                                               " rc=%d") % cmplProc.returncode)
            return swn.error
        #
        myProcs = []
        myChildren = []
        for myLine in cmplProc.stdout.splitlines():
            matchObj = lineRegex.match( myLine )
            if ( matchObj is not None ):
                myProcs.append( (int(matchObj[1]), int(matchObj[2])) )
                if ( int(matchObj[1]) == myPID ):
                    myChildren.append( int(matchObj[2]) )
        if ( len(myChildren) == 0 ):
            # no subprocesses, all done
            return swn.ok
        previousLen = 0
        while ( len(myChildren) > previousLen ):
            previousLen = len(myChildren)
            for myTuple in myProcs:
                if (( myTuple[0] in myChildren ) and
                    ( myTuple[1] not in myChildren )):
                    myChildren.append( myTuple[1] )
        #
        swn.log("D", 1, "killing sub-processes %s" % str(myChildren))
        for myChild in myChildren:
            try:
                os.kill( myChild, mySignal )
            except:
                pass

    except subprocess.TimeoutExpired:
        swn.log("E", 1, "execution of /bin/ps to get subprocesses timed out")
        return swn.error
    except Exception as excptn:
        swn.log("E", 1, "signaling subprocesses failed: %s" % str(excptn))
        return swn.error

    return swn.ok



def swnrs_process_lineage(myGenerations=1):
    """return list of Tuples with pid,cmd,birth of current+parent processes"""
    #
    lineRegex = re.compile(r"^\s*(\d+)\s+(\d+)\s+(\w{3}\s+\w{3}\s+\d+\s+\d" + \
                                                    "+:\d+:\d+\s+\d+)\s+(.*)$")

    myParents = []
    try:
        myEUID = str( os.geteuid() )
        #
        myCMD = ["/bin/ps", "-u", myEUID, "--cols", "4096", \
                            "-o", "pid=", "-o", "ppid=", \
                            "-o", "lstart=", "-o", "cmd="]
        myEnv = os.environ.copy()
        myEnv['TZ'] = "UTC"
        cmplProc = subprocess.run(myCMD, stdout=subprocess.PIPE, \
                                         stderr=subprocess.DEVNULL, \
                                         env=myEnv, \
                                         timeout=5, encoding="utf-8")
        if ( cmplProc.returncode != 0 ):
            swn.log("E", 1, ("Failed to run /bin/ps to get subprocesses," + \
                                               " rc=%d") % cmplProc.returncode)
            return myParents
        #
        myProcs = {}
        for myLine in cmplProc.stdout.splitlines():
            matchObj = lineRegex.match( myLine )
            if ( matchObj is not None ):
                myProcs[ int(matchObj[1]) ] = (int(matchObj[2]), matchObj[3],
                                                                   matchObj[4])
        myPID = os.getpid()
        for myGen in range(myGenerations + 1):
            try:
                myProcInfo = myProcs[ myPID ]
            except KeyError:
                break
            try:
                myStartTime = calendar.timegm( time.strptime(myProcInfo[1] + \
                                           " UTC", "%a %b %d %H:%M:%S %Y %Z") )
            except ValueError as excptn:
                swn.log("E", 1, "Process start time decoding failed: %s" %
                                                                   str(excptn))
                myStartTime = int(time.time()) - (myGen * 60)
            myParents.append( (myPID, myStartTime, myProcInfo[2]) )
            #
            myPID = myProcInfo[0]

    except subprocess.TimeoutExpired:
        swn.log("E", 1, "execution of /bin/ps to get subprocesses timed out")
        return myParents
    except Exception as excptn:
        swn.log("E", 1, "execution of /bin/ps to get subprocesses failed: %s" %
                                                                   str(excptn))
        return myParents

    return myParents
# ########################################################################### #



def swnrs_subprocess_kill():
    """SIGALRM then SIGKILL subprocesses"""

    swnrs_signal_children(signal.SIGALRM)
    time.sleep(1)
    swnrs_signal_children(signal.SIGKILL)

    return



def swnrs_timeout_handler(signum, frame):
    """SIGALRM then SIGKILL subprocesses and raise TimeoutError"""
    global SWNRS_TIMEOUT_REACHED

    SWNRS_TIMEOUT_REACHED = True

    now = time.time()
    swn.log("D", 0, "Timeout Handler: %d sec runtime, %d sec remaining" % \
                        (int(now - SWNRS_STARTTIME), int(SWNRS_ENDTIME - now)))
    #
    swnrs_subprocess_kill()
    #
    raise TimeoutError("Signal %d caught" % signum)

    return
# ########################################################################### #



def swnrs_basic_info():
    """lookup/check basic information"""
    global SWNRS_MARCH
    global SWNRS_SRVC_HOST
    global SWNRS_SRVC_CE
    global SWNRS_JSON_DIR
    global SWNRS_ENDTIME
    #
    returnCode = swn.ok


    # get machine architecture:
    # =========================
    SWNRS_MARCH = "unknown"
    try:
        SWNRS_MARCH = platform.machine()
    except Exception as excptn:
        swn.log("E", 1, "determining machine architecture failed: %s" %
                                                                   str(excptn))
        returnCode = swn.warning


    # get host name for metric JSONs:
    # ===============================
    SWNRS_SRVC_HOST = "unknown"
    try:
        SWNRS_SRVC_HOST = socket.gethostname().lower()
    except Exception as excptn:
        swn.log("E", 1, "determining hostname failed: %s" % str(excptn))
        returnCode = swn.warning


    # get CE name for metric JSONs and ETF-job/probe end time for timeout:
    # ====================================================================
    SWNRS_SRVC_CE = "unknown"
    SWNRS_ENDTIME = time.time() + 86400
    try:
        # etf_run.sh (job) --> etf_wnfm --> etf_wnfm --> wn_runsum.py (probe)
        lineageInfo = swnrs_process_lineage( 3 )
        probeStart = lineageInfo[0][1]
        probeTimeout = None
        probeEnd = None
        jobStart = None
        jobTimeout = None
        jobEnd = None
        #
        for pInfo in lineageInfo:
            swn.log("D", 1, "parent proc info: %s" % pInfo[2])
            myCmd = pInfo[2].split()
            myType = None
            for indx in range( len(myCmd) -1 ):
                if (( myType is None ) and ( myCmd[indx][-8:] == "etf_wnfm" )):
                    myType = "wnfm"
                    if ( probeTimeout is None ):
                        probeTimeout = 570 - 1
                elif (( myType is None ) and
                      ( myCmd[indx][-10:] == "etf_run.sh" )):
                    myType = "job"
                    if ( jobStart is None ):
                        jobStart = pInfo[1]
                        jobTimeout = 600 - 3
                #
                if (( myType == "wnfm" ) and
                    ( SWNRS_SRVC_CE == "unknown" ) and
                    ( myCmd[indx] == "--service-uri" )):
                    SWNRS_SRVC_CE = myCmd[indx+1]
                elif (( myType == "job" ) and
                      ( SWNRS_SRVC_CE == "unknown" ) and
                      ( myCmd[indx] == "-c" )):
                    SWNRS_SRVC_CE = myCmd[indx+1]
                #
                if (( myType == "wnfm" ) and ( myCmd[indx] == "-t" )):
                    probeTimeout = int(myCmd[indx+1]) - 1
                elif (( myType == "job" ) and ( myCmd[indx] == "-t" )):
                    jobTimeout = int(myCmd[indx+1]) - 3
        if (( probeStart is not None ) and ( probeTimeout is not None )):
            probeEnd = probeStart + probeTimeout
        if (( jobStart is not None ) and ( jobTimeout is not None )):
            jobEnd = probeStart + jobTimeout
        if (( probeEnd is not None ) and ( jobEnd is not None )):
            SWNRS_ENDTIME = min( probeEnd, jobEnd )
        elif ( probeEnd is not None ):
            SWNRS_ENDTIME = probeEnd
        elif ( jobEnd is not None ):
            SWNRS_ENDTIME = jobEnd
        #
        if ( SWNRS_SRVC_CE == "unknown" ):
            try:
                swn.log("E", 1, "unable to determining CE-name, \"%s\"" %
                                                             lineageInfo[1][2])
            except IndexError:
                swn.log("E", 1, "unable to determining CE-name, no proce" + \
                                                                  "ss lineage")
            returnCode = swn.warning
        else:
            swn.log("D", 1, "determined CE-name to be %s" % SWNRS_SRVC_CE)
    except Exception as excptn:
        swn.log("E", 1, "determining CE-name failed: %s" % str(excptn))
        returnCode = swn.warning


    # get ETF metric JSON file output directory:
    # ==========================================
    try:
        SWNRS_JSON_DIR = os.environ['ETF_OUT_DIR']
        if (( SWNRS_JSON_DIR[-1] == "/" ) and ( len(SWNRS_JSON_DIR) > 1 )):
            SWNRS_JSON_DIR = SWNRS_JSON_DIR[:-1]
    except Exception as excptn:
        if ( SWN_MODE == "sam" ):
            swn.log("C", 1, "No ETF metric JSON file output directory set")
            return swn.critical
        else:
            swn.log("W", 1, "No ETF metric JSON file output directory set")
            returnCode = swn.warning
            #
            SWNRS_JSON_DIR = os.path.abspath("./msg-outgoing")
            os.environ['ETF_OUT_DIR'] = SWNRS_JSON_DIR
            if ( os.path.exists( SWNRS_JSON_DIR ) == False ):
                os.mkdir(SWNRS_JSON_DIR, mode=0o755)
            swn.log("N", 1, ("ETF metric JSON file output directory set to" + \
                                                   " \"%s\"") % SWNRS_JSON_DIR)


    # check, set, and print CVMFS environmental variable:
    # ===================================================
    try:
        swn.log("N", 1, "CVMFS_MOUNT_DIR is set to \"%s\"" %
                                                 os.environ['CVMFS_MOUNT_DIR'])
    except:
        os.environ['CVMFS_MOUNT_DIR'] = "/cvmfs"
        swn.log("I", 1, "CVMFS_MOUNT_DIR set to \"/cvmfs\"")


    # check and print Apptainer/Singularity environmental variables:
    # ==============================================================
    try:
        swn.log("N", 1, "GLIDEIN_SINGULARITY_BINARY_OVERRIDE is set to \"%s\""
                           % os.environ['GLIDEIN_SINGULARITY_BINARY_OVERRIDE'])
    except:
        swn.log("I", 1, "GLIDEIN_SINGULARITY_BINARY_OVERRIDE not set")
    #
    try:
        swn.log("N", 1, "APPTAINER_BINDPATH is set to \"%s\"" %
                                              os.environ['APPTAINER_BINDPATH'])
    except:
        try:
            swn.log("N", 1, "SINGULARITY_BINDPATH is set to \"%s\"" %
                                            os.environ['SINGULARITY_BINDPATH'])
        except:
            swn.log("I", 1, "APPTAINER_BINDPATH and SINGULARITY_BINDPATH n" + \
                                                                      "ot set")


    return returnCode
# ########################################################################### #



def swnrs_read_mtrcJSON(metricName):
    """read the ETF metric JSON file, trim, and return dictionary"""
    #
    returnDict = { 'status': "UNKNOWN", 'nagios_name': metricName, \
                   'summary': "metric JSON file access failed" }


    try:
        metricFile = "%s/%s.json" % (SWNRS_JSON_DIR, metricName)
        #
        with open(metricFile, 'r') as myFile:
            myData = myFile.read()
        #
        myJSON = json.loads(myData)
        #
        try:
            del myJSON['detail']
        except:
            pass
        returnDict = myJSON

    except json.JSONDecodeError as excptn:
        swn.log("E", 2, "decoding ETF metric JSON file of %s failed: %s" %
                                                     (metricName, str(excptn)))
    except Exception as excptn:
        swn.log("E", 2, "accessing ETF metric JSON file of %s failed: %s" %
                                                     (metricName, str(excptn)))


    return returnDict



def swnrs_wait_mtrcJSON(metricName):
    """wait for ETF metric JSON file to appear and then read/return contents"""
    loopCnt = 0

    metricFile = "%s/%s.json" % (SWNRS_JSON_DIR, metricName)
    while ( int(time.time()) < SWNRS_ENDTIME ):
        try:
            if ( os.path.getsize( metricFile ) >= 64 ):
                break
        except OSError:
                pass
        if ( (loopCnt % 60) == 0 ):
            swn.log("N", 1, "waiting for %s metric JSON file" % metricName)
            swn.log("D", 1, "ETF metric JSON file directory:")
            for myFile in os.listdir(SWNRS_JSON_DIR):
                myStat = os.lstat(SWNRS_JSON_DIR + "/" + myFile)
                swn.log("D", 2, "%s %6d %s" %
                                            (time.strftime("%Y-%b-%d %H:%M:%S",
                        time.gmtime(myStat.st_mtime)), myStat.st_size, myFile))
        #
        loopCnt += 1
        time.sleep(1)
    return swnrs_read_mtrcJSON(metricName)

# ########################################################################### #



def swnrs_write_prereqFailJSONs(metricList = None):
    """write prerequisite metric JSON files for probes handled by wn_runsum"""
    #
    returnCode = swn.ok
    # DEBUG ...................................................................
    swn.log("I", 2, "executing swnrs_write_prereqFailJSONs: %s" % str(metricList))
    # DEBUG ...................................................................


    if ( metricList is None ):
        # compile list of probes handled by wn_runsum:
        metricList = set()
        try:
            osList = SWNRS_PROBE_LIST[ SWNRS_MARCH ]['run']
            for osEntry in osList:
                for mtrcEntry in SWNRS_PROBE_LIST[SWNRS_MARCH][ osEntry ]:
                    mtrcName = mtrcEntry[0].split("/")[-1].rsplit(".",1)[0]
                    metricList.add( mtrcName )
        except Exception as excptn:
            swn.log("E", 2, "compiling list of runsum probes failed: %s" %
                                                                   str(excptn))
            returnCode = swn.error
        metricList = sorted( metricList )


    # loop over metrics and write prerequisite-failure metric JSON files:
    now = time.time()
    for mtrcName in metricList:
        try:
            jsonString = ("{\"status\": \"UNKNOWN\", \"retcode\": 3, \"nag" + \
                          "ios_name\": \"%s\", \"timestamp\": %.6f, \"gath" + \
                          "eredAt\": \"%s\", \"summary\": \"UNKNOWN: prere" + \
                          "quisite check failed\", \"service_uri\": \"%s\"" + \
                          ", \"details\": \"prerequisite probe(s) not succ" + \
                          "essful, skipping execution of %s\"}") % (mtrcName,
                             now, SWNRS_SRVC_HOST, SWNRS_SRVC_CE, mtrcName)
            #
            mtrcFile = "%s/%s.json" % (SWNRS_JSON_DIR, mtrcName)
            #
            try:
                with open(mtrcFile, 'w') as myFile:
                    myFile.write( jsonString )
            except Exception as excptn:
                swn.log("E", 2, "writing metric JSON file of %s failed: %s" %
                                                       (mtrcName, str(excptn)))
                returnCode = swn.error

        except Exception as excptn:
            swn.log("E", 2, "composing metric JSON file of %s failed: %s" %
                                                       (mtrcName, str(excptn)))
            returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnrs_apptainer_mksrv(dirName=None):
    """make Apptainer run directory and populate"""
    #
    CMD_HEAD="#!/bin/sh\n#\numask 022\nTZ=UTC;export TZ\nLC_COLLATE=C;expo" + \
                                                           "rt LC_COLLATE\n#\n"
    CMD_TEST="#\necho -e \"\\nSWNRS `pwd` SWNRS\\n\"\nexit 0\n"
    #
    returnCode = swn.ok


    # generic probe id in case no run directory name is provided:
    # ===========================================================
    if (( dirName is None ) or ( dirName == "" )):
        return swn.error


    # check if directory exists and if so remove/re-create:
    # =====================================================
    if ( os.path.exists(dirName) == True ):
        swn.log("W", 1, "removing existing \"%s\" run directory" % dirName)
        returnCode = swn.warning
        try:
            shutil.rmtree(dirName)
        except Exception as excptn:
            swn.log("E", 1, ("removal of existing run directory \"%s\" fai" + \
                                           "led: %s") % (dirName, str(excptn)))
    try:
        os.mkdir(dirName, mode=0o755)
    except Exception as excptn:
        swn.log("E", 1, "creating run directory \"%s\" failed: %s" %
                                                        (dirName, str(excptn)))
        return swn.error


    # create bind points inside Apptainer /srv directory:
    # ===================================================
    flgX509 = flgTokn = False
    for myBind in SWNRS_APPTNR_BIND.split(","):
        try:
            myApptnrPath = myBind.split(":")[1]
            if ( myApptnrPath[:5] == "/srv/" ):
                if ( myApptnrPath == "/srv/user_proxy.x509" ):
                    flgX509 = True
                elif ( myApptnrPath == "/srv/user_token.b64" ):
                    flgTokn = True
                os.close( os.open(dirName + myApptnrPath[4:],
                                             os.O_CREAT | os.O_APPEND, 0o644) )
        except OSError as excptn:
            swn.log("E", 1, "creation of \"%s\" failed: %s" %
                                     (dirName + myApptnrPath[4:], str(excptn)))
            returnCode = swn.error
        except IndexError:
            pass


    # write command script:
    # =====================
    cmdFile = dirName + "/cmd.sh"
    try:
        with open(cmdFile, 'w') as myFile:
            myFile.write( CMD_HEAD )
            #
            if (( flgX509 == True ) and ( dirName != "on-os" )):
                myFile.write( "X509_USER_PROXY=/srv/user_proxy.x509;export" + \
                                                         " X509_USER_PROXY\n" )
            if (( flgTokn == True ) and ( dirName != "on-os" )):
                myFile.write( "BEARER_TOKEN_FILE=/srv/user_token.b64;expor" + \
                                                      "t BEARER_TOKEN_FILE\n" )
            #
            if ( dirName == "swnrs" ):
                myFile.write( CMD_TEST )
            else:
                myFile.write( "#\n" )
                try:
                    sensorDir = os.environ['SAME_SENSOR_HOME']
                    if ( sensorDir[-1] != "/" ):
                        sensorDir += "/"
                except KeyError:
                    sensorDir = "./"
                swn.log("D", 1, "SAME_SENSOR_HOME is \"%s\"" % sensorDir)
                try:
                    for mtrcEntry in SWNRS_PROBE_LIST[SWNRS_MARCH][dirName]:
                        exePath = sensorDir + mtrcEntry[0]
                        exeName = "/" + mtrcEntry[0].split("/")[-1]
                        logFile = exeName.rsplit(".",1)[0] + ".log"
                        # copy executable into Apptainer directory:
                        shutil.copyfile(exePath, dirName + exeName)
                        os.chmod(dirName + exeName, 0o755)
                        # patch for wn_squid.sh wrapper of CE-cms-squid
                        if ( exeName == "/wn_squid.sh" ):
                            shutil.copyfile(sensorDir + "tests/CE-cms-squid", \
                                                     dirName + "/CE-cms-squid")
                            os.chmod(dirName + "/CE-cms-squid", 0o755)
                        # add execution to command script:
                        myFile.write( "/srv" + exeName + " " + mtrcEntry[1] + \
                                          " 1>/srv" + logFile + "_tmp 2>&1\n" )
                        myFile.write( "echo \"" + dirName + exeName + \
                                                        " complete, rc=$?\"\n")
                        myFile.write( "/bin/mv /srv" + logFile + "_tmp /srv" +\
                                                             logFile + "\n#\n")
                except KeyError:
                    swn.log("E", 1, "no probe config for \"%s\" on \"%s\"" %
                                                        (dirName, SWNRS_MARCH))
                    returnCode = swn.error
            myFile.write( "#\nexit 0\n" )
        os.chmod(cmdFile, 0o755)
        swn.log("I", 1, "command file for %s written" % dirName)
    except Exception as excptn:
            swn.log("E", 1, "creation of command script \"%s\" failed: %s" %
                                                        (cmdFile, str(excptn)))
            returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnrs_env_assemble():
    """assemble the environment for the Apptainer/Singularity run"""
    #
    CLASH_LIST = ["TMP", "TMPDIR", "TEMP", "TEMPDIR", "OSG_WN_TMP",
                  "X509_USER_PROXY", "X509_USER_KEY", "X509_USER_CERT",
                  "X509_CERT_DIR",
                  "XDG_RUNTIME_DIR",
                  "LD_LIBRARY_PATH", "LD_PRELOAD",
                  "PYTHONPATH", "PERL5LIB",
                  "GFAL_CONFIG_DIR", "GFAL_PLUGIN_DIR",
                  "SCRAM_ARCH" ]
    #
    #
    myRunEnv = os.environ.copy()
    #
    myRunEnv['PATH'] = "/usr/bin:/bin"
    #
    for myEnv in CLASH_LIST:
        try:
            del myRunEnv[myEnv]
        except:
            pass
    #
    myRunEnv['TZ'] = "UTC"
    #
    return myRunEnv
# ########################################################################### #



def swnrs_apptainer_init():
    """check bind areas and locate working Apptainer/Singularity binary"""
    global SWNRS_APPTNR_IMG_STEM
    global SWNRS_APPTNR_BIND
    global SWNRS_APPTNR_EXEC
    global SWNRS_APPTNR_ENV
    #
    BINDLIST = "/storage /lfs_roots /cms /hadoop /hdfs /mnt/hadoop \
                /etc/cvmfs/SITECONF \
                ${CVMFS_MOUNT_DIR}/grid.cern.ch/etc/grid-security:/etc/grid-security \
                ${CVMFS_MOUNT_DIR}:/cvmfs /etc/hosts /etc/localtime"
    EXECLIST = "${GLIDEIN_SINGULARITY_BINARY_OVERRIDE} \
                ${CVMFS_MOUNT_DIR}/oasis.opensciencegrid.org/mis/singularity/current/bin/singularity \
                WHICH:apptainer WHICH:singularity \
                MODULE:singularitypro:singularity \
                MODULE:singularity:singularity"
    #
    varRegex = re.compile(r"^\$\{(\w+)\}(.*)")
    functionalRegex = re.compile(r"^SWNRS /srv SWNRS$", re.MULTILINE)
    #
    returnCode = swn.ok


    # select image used for testing:
    # ==============================
    myImage = None
    for myImage in SWNRS_PROBE_LIST[ SWNRS_MARCH ]['run']:
        if ( myImage != "on-os" ):
            # take first OS image, i.e. not running on the base OS
            break
    if (( myImage is None ) or ( myImage == "on-os" )):
        # no Apptainer execution required:
        swn.log("N", 1, "no Apptainer invocation for %s required" %
                                                                   SWNRS_MARCH)
        return swn.ok


    # check/update image stem:
    # ========================
    matchObj = varRegex.match( SWNRS_APPTNR_IMG_STEM )
    if ( matchObj is not None ):
        try:
            varValue = os.environ[ matchObj[1] ]
            try:
                SWNRS_APPTNR_IMG_STEM = varValue + matchObj[2]
            except IndexError:
                SWNRS_APPTNR_IMG_STEM = varValue
        except:
            swn.log("E", 1, "env.variable translation of \"%s\" failed: %s" %
                                          (SWNRS_APPTNR_IMG_STEM, str(excptn)))
            return swn.critical
    #
    # set full image path:
    myImagePath = SWNRS_APPTNR_IMG_STEM + myImage
    #
    # check image exists:
    if ( os.path.exists( myImagePath ) == False ):
        swn.log("E", 1, "image file \"%s\" inaccessible" % myImagePath)
        return swn.critical


    # check bind areas:
    # =================
    SWNRS_APPTNR_BIND = ""
    for myBind in BINDLIST.split():
        matchObj = varRegex.match( myBind )
        if ( matchObj is not None ):
            try:
                varValue = os.environ[ matchObj[1] ]
                try:
                    myBind = varValue + matchObj[2]
                except IndexError:
                    myBind = varValue
            except:
                swn.log("W", 1, ("Skipping bind area %s due to env.variabl" + \
                                             "e translation failure" % myBind))
                returnCode = swn.warning
                continue
        myHostPath = myBind.split(":")[0]
        if ( os.path.exists( myHostPath ) == True ):
            SWNRS_APPTNR_BIND += "," + myBind
    # 
    # x509 certificate and IAM-issued token files:
    myCert = myToken = None
    try:
        myCert = os.environ['X509_USER_PROXY']
        if ( os.path.exists( myCert ) == False ):
            raise OSError
        SWNRS_APPTNR_BIND += "," + myCert + ":/srv/user_proxy.x509"
    except:
        try:
            myCert = "/tmp/x509up_u%d" % os.geteuid()
            if ( os.path.exists( myCert ) == False ):
                raise OSError
            SWNRS_APPTNR_BIND += "," + myCert + ":/srv/user_proxy.x509"
        except:
            swn.log("W", 1, "no x509 certificate")
            returnCode = swn.warning
    try:
        myToken = os.environ['BEARER_TOKEN_FILE']
        if ( os.path.exists( myToken ) == False ):
            raise OSError
        SWNRS_APPTNR_BIND += "," + myToken + ":/srv/user_token.b64"
    except:
        try:
            myToken = os.environ['XDG_RUNTIME_DIR'] + "/bt_u%d" % os.geteuid()
            if ( os.path.exists( myToken ) == False ):
                raise OSError
            SWNRS_APPTNR_BIND += "," + myToken + ":/srv/user_token.b64"
        except:
            try:
                myToken = "/tmp/bt_u%d" % os.geteuid()
                if ( os.path.exists( myToken ) == False ):
                    raise OSError
                SWNRS_APPTNR_BIND += "," + myToken + ":/srv/user_token.b64"
            except:
                if ( 'BEARER_TOKEN' not in os.environ ):
                    swn.log("N", 1, "no IAM-issued token")
    #
    SWNRS_APPTNR_BIND = SWNRS_APPTNR_BIND.strip(",")


    # prepare Apptainer run subdirectory:
    # ===================================
    status = swnrs_apptainer_mksrv("swnrs")
    if ( swn.code2order(status) >= swn.code2order(swn.error) ):
        return swn.critical
    elif ( swn.code2order(status) > swn.code2order(returnCode) ):
        returnCode = status


    # assemble the environment for the Apptainer/Singularity run:
    # ===========================================================
    SWNRS_APPTNR_ENV = swnrs_env_assemble()


    # loop over potential Apptainer binaries and check if it works:
    # =============================================================
    SWNRS_APPTNR_EXEC = None
    for myExec in EXECLIST.split():
        swn.log("D", 1, "checking Apptainer binary %s" % myExec)

        # handle environmental variables:
        matchObj = varRegex.match( myExec )
        if ( matchObj is not None ):
            try:
                varValue = os.environ[ matchObj[1] ]
                try:
                    myExec = varValue + matchObj[2]
                except IndexError:
                    myExec = varValue
            except:
                continue

        # handle "which"/locating binary in PATH:
        elif ( (myExec + "      ")[:6] == "WHICH:" ):
            swn.log("X", 2, "handling WHICH")
            myExec = shutil.which( myExec[6:] )
            swn.log("X", 2, "handled WHICH, binary now %s" % myExec)
            if ( myExec is None ):
                continue

        # handle environment modules:
        elif ( (myExec + "       ")[:7] == "MODULE:" ):
            if ( os.path.exists( "/usr/bin/modulecmd" ) == False ):
                continue
            try:
                myModName = myExec.split(":")[1]
                myModExec = myExec.split(":")[2]
            except IndexError:
                continue
            myExec = "/usr/bin/modulecmd sh load %s; %s" % \
                                                         (myModName, myModExec)

        # paranoia check before test execution:
        if (( myExec is None ) or ( myExec == "" )):
            continue

        # prepare options string:
        myOpts = "--home " + os.getcwd() + "/swnrs:/srv --pwd /srv --bind " + \
                                   SWNRS_APPTNR_BIND + " --ipc --contain --pid"


        # run Apptainer test:
        # ===================
        myCmd = myExec + " exec " + myOpts + " " + myImagePath + " /srv/cmd.sh"
        swn.log("D", 1, "attempting \"%s\"" % myCmd)
        #
        try:
            cmplProc = subprocess.run(myCmd, stdout=subprocess.PIPE, \
                                             stderr=subprocess.STDOUT, \
                                             shell=True, env=SWNRS_APPTNR_ENV,\
                                             timeout=90, encoding="utf-8")
            if ( cmplProc.returncode != 0 ):
                swn.log("N", 1, "return code of %s is %d" % (myExec,
                                                          cmplProc.returncode))
                swn.log("I", 2, cmplProc.stdout)
                swn.log("D", 2, str(cmplProc))
                continue
            #
            matchObj = functionalRegex.search( cmplProc.stdout )
            if ( matchObj is None ):
                swn.log("N", 1, "output of %s indicates failure" % myExec)
                swn.log("I", 2, cmplProc.stdout)
                continue
            #
            SWNRS_APPTNR_EXEC = myExec
            swn.log("I", 1, "using Apptainer \"%s\"" % myExec)
            break
        except subprocess.TimeoutExpired:
            swn.log("E", 1, "execution of %s timed out" % myExec)
            continue
        except Exception as excptn:
            swn.log("E", 1, "execution of %s failed: %s" % (myExec,
                                                                  str(excptn)))
            continue


    if ( SWNRS_APPTNR_EXEC is None ):
        swn.log("C", 1, "No working Apptainer/Singularity found")
        returnCode = swn.critical

    return returnCode
# ########################################################################### #



def swnrs_prerequisite():
    """check metric pre-requisites of probe"""
    #
    returnCode = swn.ok


    try:
        myPreqList = SWNRS_PROBE_LIST[ SWNRS_MARCH ]['prerq']
        #
        for myMetric in myPreqList:
            samDict = swnrs_wait_mtrcJSON( myMetric )
            # DEBUG ...........................................................
            swn.log("I", 2, "DEBUG: %s: %s" % (samDict['nagios_name'],samDict['status']))
            # DEBUG ...........................................................
            if ( samDict['status'] == "CRITICAL" ):
                swn.log("C", 1, ("prerequisite %s failed (%s), skipping ex" + \
                                 "ecution of runsum-handled probe(s)") % \
                                                  (myMetric,samDict['status']))
                returnCode = swn.critical
                break
    except TimeoutError as excptn:
        swn.log("C", 1, ("Maximum execution time reached during prerequisi" + \
                                                 "te check: %s") % str(excptn))
        raise TimeoutError("Timeout during prerequisite check")
    except Exception as excptn:
        swn.log("E", 1, "checking probe prerequisites failed: %s" %
                                                                   str(excptn))
        swn.log("D", 1, traceback.format_exc() )
        returnCode = swn.unknown

    return returnCode
# ########################################################################### #



def swnrc_apptainer_launch():
    """start subprocesses with metrics handled by runsum in OS Apptainer"""
    global SWNRS_APPTNR_POPEN
    #
    returnCode = swn.ok


    for myOS in SWNRS_PROBE_LIST[ SWNRS_MARCH ]['run']:
        # prepare Apptainer run subdirectory:
        status = swnrs_apptainer_mksrv(myOS)
        if ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        if ( swn.code2order(status) >= swn.code2order(swn.error) ):
            continue

        # full image path:
        if ( myOS != "on-os" ):
            imagePath = SWNRS_APPTNR_IMG_STEM + myOS
            # check image exists:
            if ( os.path.exists( imagePath ) == False ):
                swn.log("E", 1, "image file \"%s\" inaccessible" % imagePath)
                continue
            # prepare options string:
            myOpts = "--home " + os.getcwd() + "/" + myOS + \
                     ":/srv --pwd /srv " + "--bind " + SWNRS_APPTNR_BIND + \
                     " --ipc --contain --pid"
            # prepare command string:
            myCmd = SWNRS_APPTNR_EXEC + " exec " + myOpts + " " + imagePath + \
                                                                 " /srv/cmd.sh"
        else:
            # prepare environment:
            runEnv = os.environ.copy()
            runEnv['TZ'] = "UTC"
            # prepare command list:
            myCmd = [myOS + "/cmd.sh"]

        try:
            if ( myOS != "on-os" ):
                myPopen = subprocess.Popen(myCmd, stdout=subprocess.PIPE, \
                                                  stderr=subprocess.STDOUT, \
                                                  shell=True, \
                                                  env=SWNRS_APPTNR_ENV,\
                                                  encoding="utf-8")
            else:
                myPopen = subprocess.Popen(myCmd, stdout=subprocess.PIPE, \
                                                  stderr=subprocess.STDOUT, \
                                                  env=runEnv, encoding="utf-8")
            #
            SWNRS_APPTNR_POPEN.append( (myOS, myPopen) )

        except Exception as excptn:
            swn.log("E", 1, "subprocess launch for \"%s\" failed: %s" %
                                                           (myOS, str(excptn)))
            returnCode = swn.error

    return returnCode
# ########################################################################### #



def swnrs_write_evaluationJSON(metricName, osTuple):
    """write the metric JSON file based on the probe log files of the OSes"""
    #
    returnCode = swn.ok


    # evaluate overal status and concatenate logs into metric detail:
    # ===============================================================
    mtrcStatus = "OK"
    mtrcSummry = "all OS runs successful"
    mtrcDetail = ""
    #
    for myTuple in osTuple:
        myOS = myTuple[0]
        myEval = myTuple[1]
        logFile = myOS + "/" + metricName + ".log"
        try:
            with open(logFile, 'r') as myFile:
                myData = myFile.read()
        except OSError as excptn:
            swn.log("E", 1, "open/read of probe log file %s failed: %s" %
                                                        (logFile, str(excptn)))
            returnCode = swn.error
            myData = ("UNKNOWN: probe log file inaccessible\n\nopen/read o" + \
                      "f probe log file %s failed: %s\n") % \
                                                         (logFile, str(excptn))
        myLine = myData.split("\n",1)[0]
        myStatus = myLine.split(":",1)[0].strip().upper()
        mySummry = myLine.split(":",1)[-1].strip()
        if (( swn.samName2order(myStatus) > swn.samName2order(mtrcStatus) ) and
                ( myEval == True )):
            mtrcStatus = myStatus
            mtrcSummry = "[" + myOS + "] " + mySummry
        elif (( myStatus == mtrcStatus ) and ( myEval == True ) and
              ( "] " + mySummry == mtrcSummry[-(len(mySummry)+2):] )):
            myLen = len(mySummry)+2
            mtrcSummry = mtrcSummry[:-myLen] + "," + myOS + mtrcSummry[-myLen:]
        elif (( mtrcStatus == "OK" ) and ( myStatus != "OK" ) and
              ( myEval == False )):
            mtrcSummry = "OS runs successful"
        #
        mtrcDetail += "\n\n\n %s:\n=%s=\n\n%s\n\n%s\n" % (myOS, "="*len(myOS), \
                                          myData[len(myLine):].strip(), myLine)


    # compose metric dictionary:
    # ==========================
    now = time.time()
    mtrcJSON = {}
    mtrcJSON['status'] = mtrcStatus
    mtrcJSON['retcode'] = swn.samName2code(mtrcStatus)
    mtrcJSON['nagios_name'] = metricName
    mtrcJSON['timestamp'] = now
    mtrcJSON['gatheredAt'] = SWNRS_SRVC_HOST
    mtrcJSON['service_uri'] = SWNRS_SRVC_CE
    mtrcJSON['summary'] = mtrcSummry
    mtrcJSON['details'] = mtrcDetail.lstrip()


    # write JSON file:
    # ================
    mtrcFile = "%s/%s.json" % (SWNRS_JSON_DIR, metricName)
    #
    try:
        with open(mtrcFile, 'w') as myFile:
            json.dump(mtrcJSON, myFile)
    except Exception as excptn:
        swn.log("E", 2, "writing metric JSON file of %s failed: %s" %
                                                     (metricName, str(excptn)))
        returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnrs_evaluate_probes():
    """check probe executions are complete and evaluate metric status"""
    #
    returnCode = swn.ok


    # compile a dictionary with OS and evaluation flag for each metric:
    # =================================================================
    metricDict = {}
    try:
        osList = SWNRS_PROBE_LIST[ SWNRS_MARCH ]['run']
        for myOS in osList:
            for myProbe in SWNRS_PROBE_LIST[ SWNRS_MARCH ][ myOS ]:
                myMetric = myProbe[0].split("/")[-1].rsplit(".",1)[0]
                try:
                    myEval = myProbe[2]
                except:
                    myEval = True
                try:
                    # metric: [ OS, evaluation flag, probe results available ]
                    metricDict[myMetric].append( [myOS, myEval, False] )
                except KeyError:
                    metricDict[myMetric] = [ [myOS, myEval, False] ]
    except Exception as excptn:
        swn.log("E", 1, "compiling dictionary of runsum probes failed: %s" %
                                                                   str(excptn))
        return swn.critical


    # analyze the metric log file for an OS run as soon as it is available:
    # =====================================================================
    try:
        mySleep = 15
        #
        while( len(metricDict) > 0 ):
            time.sleep(mySleep)
            #
            mySleep = 0
            #
            # catch incomplete Apptainer runs:
            allDone = True
            for myTuple in SWNRS_APPTNR_POPEN:
                if ( myTuple[1].poll() is None ):
                    # OS run not complete
                    allDone = False
                    break
            #
            # check for new results:
            for myMetric in list( metricDict.keys() ):
                allAvail = True
                for myEntry in metricDict[myMetric]:
                    if ( myEntry[2] is False ):
                        logFile = myEntry[0] + "/" + myMetric + ".log"
                        if ( os.path.exists( logFile ) == True ):
                            myEntry[2] = True
                        else:
                            allAvail = False
                            mySleep += 1
                if ( allAvail == True ):
                    # write metric JSON file:
                    osTuple = [ (e[0],e[1]) for e in metricDict[myMetric] ]
                    status = swnrs_write_evaluationJSON(myMetric, osTuple)
                    if ( swn.code2order(status) > swn.code2order(returnCode) ):
                        returnCode = status
                    # remove from metric to-do list:
                    del metricDict[myMetric]
            mySleep = max(1, mySleep)
            #
            if ( allDone == True ):
                break

    except TimeoutError as excptn:
        swn.log("E", 0, "Execution timeout while checking probe log files")
        #
        # check for and print incomplete probe log files:
        osList = SWNRS_PROBE_LIST[ SWNRS_MARCH ]['run']
        for myOS in osList:
            for myProbe in SWNRS_PROBE_LIST[ SWNRS_MARCH ][ myOS ]:
                myMetric = myProbe[0].split("/")[-1].rsplit(".",1)[0]
                logFile = myOS + "/" + myMetric + ".log_tmp"
                try:
                    with open(logFile, 'r') as myFile:
                        myData = myFile.read()
                    swn.log("E", 1, "\ntemporary log file %s found:" % logFile)
                    swn.log("E", 2, myData)
                except FileNotFoundError:
                    pass
                except Exception as excptn:
                    swn.log("E", 1, ("open/read of probe temporary log fil" + \
                                   "e %s failed: %s") % (logFile, str(excptn)))
                    returnCode = swn.error
        #
        if ( swn.code2order(swn.unknown) > swn.code2order(returnCode) ):
            returnCode = swn.unknown
    except Exception as excptn:
        swn.log("E", 0, "Execution of probe log file check failed: %s" %
                                                                   str(excptn))
        returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnrs_apptainer_collect():
    """get stdout/stderr from Apptainer subprocesses and print"""
    #
    returnCode = swn.ok

    for myTuple in SWNRS_APPTNR_POPEN:
        try:
            myOut, myErr = myTuple[1].communicate()
            myRC = myTuple[1].returncode
            if ( myRC == 0 ):
                mySevrityChar = "I"
            else:
                mySevrityChar = "E"
            swn.log(mySevrityChar, 1, "\n%sexecution log:\n%s\n%s" % \
                                 (myTuple[0], "="*(14+len(myTuple[0])), myOut))
        except Exception as excptn:
            swn.log("E", 1, "collecting output of %s execution failed: %s" %
                                                                   str(excptn))
            returnCode = swn.error

    return returnCode
# ########################################################################### #



def swnrs_summary_eval():
    """check status of worker node metrics and make summary evaluation"""
    #
    returnCode = swn.ok
    returnStrng = "WorkerNode probes successful"


    def samName2swnCapName(statusStrng):
        if ( statusStrng == "OK" ):
            return "Ok"
        elif ( statusStrng == "WARNING" ):
            return "Warning"
        elif ( statusStrng == "CRITICAL" ):
            return "Error"
        else:
            return "Unknown"

    def samClnSummary(summaryStrng):
        if ( summaryStrng[:4] == "OK, " ):
            return summaryStrng[4:]
        elif ( summaryStrng[:9] == "WARNING, " ):
            return summaryStrng[9:]
        elif ( summaryStrng[:10] == "CRITICAL, " ):
            return summaryStrng[10:]
        elif ( summaryStrng[:9] == "UNKNOWN, " ):
            return summaryStrng[9:]
        else:
            return summaryStrng


    # check worker node metric results and issue summary result:
    print("\n")
    try:
        mySummaryList = SWNRS_PROBE_LIST[ SWNRS_MARCH ]['sumry']
        #
        for myMetric in mySummaryList:
            try:
                samDict = swnrs_read_mtrcJSON( myMetric )
                #
                print("   %-13.13s: %-7.7s   (%s)" % (myMetric,
                                         samName2swnCapName(samDict['status']),
                                            samClnSummary(samDict['summary'])))
                #
                status = swn.samName2swnCode( samDict['status'] )
                if ( swn.code2order(status) > swn.code2order(returnCode) ):
                    returnCode = status
                    if  ( status == swn.unknown ):
                        returnStrng = "%s probe status unknown" % myMetric
                    elif ( status == swn.warning ):
                        returnStrng = "%s probe with warning" % myMetric
                    else:
                        returnStrng = "%s probe with error" % myMetric
            except Exception as excptn:
                swn.log("E", 1, "status access of probe %s failed: %s" %
                                                       (myMetric, str(excptn)))
                if ( swn.code2order(swn.unknown) > swn.code2order(returnCode) ):
                    returnCode = swn.unknown
                    returnStrng = "%s probe status access failed" % myMetric

    except Exception as excptn:
        swn.log("E", 1, "evaluating summary status failed: %s" % str(excptn))
        returnCode = swn.unknown
        returnStrng = "summary evaluation failed"

    return returnCode, returnStrng
# ########################################################################### #



if __name__ == '__main__':
    #
    try:
        parserObj = argparse.ArgumentParser(description="CMS SAM WorkerNode " +
                                         "probe runner and summary evaluation")
        parserObj.add_argument("-S", dest="sam", action="store_true",
                                     default=False,
                                     help="ETF configuration, SAM mode")
        parserObj.add_argument("-P", dest="psst", action="store_true",
                                     default=False,
                                     help="fast, minimal-output probing, PSS" +
                                                                      "T mode")
        parserObj.add_argument("-t", dest="timeout", type=int, default=-5,
                                     help="maximum time probe is allowed to " +
                                                        "take in seconds [-5]")
        parserObj.add_argument("-v", dest="verbose", action="count",
                                     help="increase verbosity")
        argStruct = parserObj.parse_args()
        #
        #
        #
        if ( argStruct.sam == True ):
            SWN_STDOUT = sys.stdout
            SWN_STDERR = sys.stderr
            sys.stderr = sys.stdout = SWN_STRINGIO = io.StringIO()
            #
            print("<PRE>")
            #
            SWN_MODE = "sam"
            SWN_VERBOSITY = 2
        elif ( argStruct.psst == True ):
            SWN_MODE = "psst"
            SWN_VERBOSITY = -1
        elif ( len(os.getenv("SAME_SENSOR_HOME", default="")) > 0 ):
            SWN_STDOUT = sys.stdout
            SWN_STDERR = sys.stderr
            sys.stderr = sys.stdout = SWN_STRINGIO = io.StringIO()
            #
            print("<PRE>")
            #
            SWN_MODE = "sam"
            SWN_VERBOSITY = 2
        if ( argStruct.verbose is not None ):
            SWN_VERBOSITY = argStruct.verbose
        #
        #
        #
        # initialize to ok:
        returnCode = swn.ok
        summaryMSG = "abnormal execution"
        SWNRS_STARTTIME = time.time()
        swn.log("N", 0, ("Starting CMS probe run and summary evaluation of" + \
                         " %s at %s UTC") % (socket.gethostname(),
             time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(SWNRS_STARTTIME))))
        swn.log("N", 1, "WN-99summary version %s" % SWNRS_VERSION)
        swn.log("N", 1, "python version %d.%d.%d\n" % sys.version_info[0:3])
        #
        #
        #
        # identify machine architecture, gather various information:
        swn.log("N", 0, "\nLooking up/checking basic information:")
        status = swnrs_basic_info()
        if ( status == swn.critical ):
            swn.exit(swn.unknown, "configuration information missing")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        #
        #
        # set timeout:
        if ( argStruct.timeout < 0 ):
            # timeout relative to ETF worker node job time limit:
            myTimeout = int( SWNRS_ENDTIME + argStruct.timeout - time.time() )
            if ( myTimeout > 0 ):
                argStruct.timeout = myTimeout
        if ( argStruct.timeout > 0 ):
            signal.signal(signal.SIGALRM, swnrs_timeout_handler)
            signal.alarm( argStruct.timeout )
            swn.log("N", 1, "Timeout in %s seconds" % myTimeout)
        elif ( argStruct.timeout == 0 ):
            swn.log("I", 1, "No timeout set")
        else:
            swn.log("C", 1, "insufficient time to run probe")
            swn.exit(swn.unknown, "insufficient time remaining")
        #
        #
        # initialize Apptainer/Singularity environment:
        # =============================================
        swn.log("N", 0, "\nInitializing apptainer/singularity:")
        status = swnrs_apptainer_init()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( status == swn.critical ):
            swn.exit(swn.unknown, "Apptainer initialization failed")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        #
        #
        # probe pre-requisite check:
        # ==========================
        swn.log("N", 0, "\nChecking probe pre-requisites:")
        status = swnrs_prerequisite()
        # DEBUG .............xx.......xxxxxxxx......................x..........
        swn.log("I", 1, "==> %d %s" % (status, swn.code2name(status)) )
        if ( status == swn.critical ):
            swnrs_write_prereqFailJSONs()
            swn.exit(swn.critical, "prerequisite check failed")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        #
        #
        # launch Apptainer/Singularity subprocesses:
        # ==========================================
        swn.log("N", 0, "\nLaunching Apptainer subprocesses:")
        status = swnrc_apptainer_launch()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        if ( status == swn.critical ):
            swnrs_subprocess_kill()
        #
        #
        # evaluate status of runsum-handled probes as execution completes:
        # ================================================================
        swn.log("N", 0, "\nEvaluating metrics as probes complete:")
        status = swnrs_evaluate_probes()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
        if ( status == swn.critical ):
            swnrs_subprocess_kill()
        #
        #
        # get log files from Apptainer subprocesses and print:
        # ====================================================
        swnrs_apptainer_collect()
        #
        #
        # evaluate runsum status and write summary:
        # =========================================
        swn.log("N", 0, "\nEvaluating WorkerNode probe summary:")
        returnCode,summaryMSG = swnrs_summary_eval()
        swn.log("I", 1, "==> %s" % swn.code2name(returnCode) )
        if (( returnCode == swn.unknown ) and ( SWNRS_TIMEOUT_REACHED )):
            returnCode = swn.error
            summaryMSG = "execution timeout"


        now = time.time()
        swn.log("I", 0, ("\nEnding CMS probe run and summary evaluation at" + \
                         " %s UTC (%d sec)") %
                         (time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(now)),
                                                 int( now - SWNRS_STARTTIME )))
    except TimeoutError as excptn:
        delta = time.time() - SWNRS_STARTTIME
        swn.log("E", 0, "Maximum execution time exceeded, %.3f sec: %s" %
                                                          (delta, str(excptn)))
        #
        returnCode = swn.error
        summaryMSG = "execution timeout"

    except Exception as excptn:
        swn.log("E", 0, "Execution failed: %s" % str(excptn))
        swn.log("E", 0, traceback.format_exc() )
        #
        returnCode = swn.error
        summaryMSG = "execution failure"


    swn.exit(returnCode, summaryMSG)
    #import pdb; pdb.set_trace()
