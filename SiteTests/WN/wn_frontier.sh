#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode DB FroNtier probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if running:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; wait ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'swn_exit ${SWN_CRITICAL} "Execution timeout"' 14
#
#
#
SWNFT_VERSION="v1.00.04"
#
SWNFT_CMSSW_VRSN="latest"
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      echo 0
   elif [ $1 -eq 1 ]; then
      echo 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      echo 2
   else
      echo 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      echo 1
   else
      echo 0
   fi
}
#
swn2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Unknown" ;;
   3) echo "Error" ;;
   *) echo "Critical" ;;
   esac
}
#
sam2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Critical" ;;
   *) echo "Unknown" ;;
   esac
}
#
swn_exit () {
   if [ "${SWN_MODE}" = "sam" ]; then
      echo -e "\n</PRE>"
      #
      # switch stdout/stderr back:
      exec 1>&3 2>&4
      cd ${SWN_HOME}
      #
      # translate SWN code into SAM codes:
      MYRC=`swn2sam $1`
      #
      # print summary followed by detailed log:
      MYNAME=`sam2name ${MYRC}`
      echo -e "${MYNAME^^}: $2\n"
      if [ -f swnft.$$.log ]; then
         /bin/cat swnft.$$.log
         /bin/rm -f swnft.$$.log
      fi
      #
      exit ${MYRC}

   elif [ "${SWN_MODE}" = "psst" ]; then
      # translate return code into PSST codes:
      MYRC=`swn2psst $1`
      #
      exit ${MYRC}

   else
      exit $1 
   fi
}
#
timeout_handler () {
   /bin/sleep ${SWN_TIMEOUT}
   echo -e "\ntimeout of ${SWN_TIMEOUT} seconds reached\n"
   ALARM_PID=${BASHPID}
   PID_LIST=`/bin/ps --ppid $1 -o pid= | /bin/grep -v ${ALARM_PID}`
   if [ -n "${PID_LIST}" ]; then
      kill -9 ${PID_LIST//$'\n'/ }
   fi
   kill -ALRM $1
}
#
SWNRC=${SWN_OK}
SUMMRY="DB FroNtier check ok"
# ########################################################################### #



# handle command line options, setup timeout, and print basic information:
if [ -n "${SAME_SENSOR_HOME}" ]; then
   SWN_MODE="sam"
   SWN_VERBOSE=2
else
   SWN_MODE="interactive"
   SWN_VERBOSE=0
fi
SWN_TIMEOUT=300
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SWN_MODE="sam"
       SWN_VERBOSE=2
       ;;
   -P) SWN_MODE="psst"
       SWN_VERBOSE=-1
       ;;
   -t) IARG=`expr ${IARG} + 1`
       SWN_TIMEOUT=${!IARG}
       ;;
   -v) SWN_VERBOSE=`expr ${SWN_VERBOSE} + 1`
       ;;
   -vv) SWN_VERBOSE=2
       ;;
   -vvv) SWN_VERBOSE=3
       ;;
   -vvvv) SWN_VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode DB FroNtier probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [180]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
#
if [ "${SWN_MODE}" = "sam" ]; then
   # switch stdout/stderr to file (for later output after summary message):
   SWN_HOME=`/bin/pwd`
   exec 3>&1 4>&2 1>swnft.$$.log 2>&1
   #
   echo "<PRE>"
fi
#
timeout_handler $$ &
ALARM_PID=$!
#
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   TMP1=`(/bin/hostname -f || /bin/hostname) 2>/dev/null`
   TMP2=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   echo -e "\nStarting CMS DB FroNtier check of ${TMP1} at ${TMP2} UTC"
   echo "   WN-22frontier version ${SWNFT_VERSION}"
   echo "   machine hardware is `/bin/uname -m`"
   if [ -f /etc/os-release ]; then
      MY_TMP=`/usr/bin/awk -F= '{if(($1=="NAME")||($1=="VERSION")){gsub("\"","",$2);printf " %s",$2}}' /etc/os-release`
      echo "   operating system is${MY_TMP}"
   elif [ -f /etc/redhat-release ]; then
      echo "   operating system is `/bin/cat /etc/redhat-release`"
   else
      echo "   no /etc/os-release or /etc/redhat-release file"
   fi
   unset TMP2
   unset TMP1
   MY_START=`/bin/date -u +'%s' 2>/dev/null`
fi
#
#
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo -e "\n\nsetting up environment:"
   echo "-----------------------"
   #
   if [ "${CVMFS_MOUNT_DIR+x}" = "x" ]; then
      echo "   env.variable CVMFS_MOUNT_DIR is set to ${CVMFS_MOUNT_DIR}"
   else
      echo "   env.variable CVMFS_MOUNT_DIR is not set"
   fi
   if [ "${CVMFS+x}" = "x" ]; then
      echo "   env.variable CVMFS is set to ${CVMFS}"
   else
      echo "   env.variable CVMFS is not set"
   fi
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
#
if [ "${CVMFS_MOUNT_DIR+x}" != "x" ]; then
   if [ "${CVMFS+x}" = "x" ]; then
      CVMFS_MOUNT_DIR=${CVMFS}
   else
      CVMFS_MOUNT_DIR=/cvmfs
   fi
fi
#
if [ -f ${CVMFS_MOUNT_DIR}/cms.cern.ch/cmsset_default.sh ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo "   sourcing cmsset_default.sh ..."
   fi
   . ${CVMFS_MOUNT_DIR}/cms.cern.ch/cmsset_default.sh
   #
   if [ "${SCRAM_ARCH+x}" = "x" ]; then
      echo "   env.variable SCRAM_ARCH is set to ${SCRAM_ARCH}"
   else
      echo "   env.variable SCRAM_ARCH is not set"
   fi
   #
   MY_TMP=`/usr/bin/which scram 2>/dev/null`
   if [ $? -ne 0 ]; then
      echo "[C] scram not defined after sourcing cmsset_default.sh"
      SWNRC=${SWN_CRITICAL}
      SUMMRY="no scram command"
      swn_exit ${SWNRC} "${SUMMRY}"
   else
      echo "      ${MY_TMP}"
   fi
else
   echo "[C] no cmsset_default.sh on CVMFS of cms.cern.ch"
   SWNRC=${SWN_CRITICAL}
   SUMMRY="no cmsset_default.sh"
   swn_exit ${SWNRC} "${SUMMRY}"
fi
#
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo "   env.variable SITECONFIG_PATH is set to \"${SITECONFIG_PATH}\""
   echo "   env.variable CMS_PATH is set to \"${CMS_PATH}\""
fi
#
# select CMSSW version, if not fixed:
if [ "${SWNFT_CMSSW_VRSN}" = "latest" ]; then
   SWNFT_CMSSW_VRSN=`scram list | /bin/grep -B 1 "cms.cern.ch" | /bin/grep " CMSSW" | /usr/bin/tail -1 | /usr/bin/awk '{print $2;exit}'`
   if [ $? -ne 0 ]; then
      echo "   [E] error selecting CMSSW version"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="error selecting CMSSW"
      fi
   fi
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      echo "   ${SWNFT_CMSSW_VRSN} selected"
   fi
fi
SWNFT_CMSSW_VRSN="${SWNFT_CMSSW_VRSN%% *}"
if [ "${SWNFT_CMSSW_VRSN%%_*}" != "CMSSW" ]; then
   echo "[C] no valid CMSSW version found but \"${SWNFT_CMSSW_VRSN}\""
   SWNRC=${SWN_CRITICAL}
   SUMMRY="no valid CMSSW"
   swn_exit ${SWNRC} "${SUMMRY}"
fi
#
# make execution subdirectory:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo "   making and changing to swnft subdirectory"
fi
mkdir swnft
cd swnft
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo "   creating ${SWNFT_CMSSW_VRSN} project..."
fi
scram project CMSSW ${SWNFT_CMSSW_VRSN}
if [ $? -ne 0 ]; then
   echo "   [E] error creating ${SWNFT_CMSSW_VRSN} project"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="error creating CMSSW project"
   fi
fi
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo "   setting up ${SWNFT_CMSSW_VRSN} environment..."
fi
cd ${SWNFT_CMSSW_VRSN}/src
eval `scram runtime -sh`
if [ $? -ne 0 ]; then
   echo "[C] failed to set up ${SWNFT_CMSSW_VRSN} environment"
   SWNRC=${SWN_CRITICAL}
   SUMMRY="failed to setup CMSSW environment"
   swn_exit ${SWNRC} "${SUMMRY}"
fi
cd ..
#
if [ ${SWN_VERBOSE} -ge 3 ]; then
   FRONTIER_LOG_LEVEL=debug;export FRONTIER_LOG_LEVEL
else
   FRONTIER_LOG_LEVEL=warning;export FRONTIER_LOG_LEVEL
fi
# ########################################################################### #



# write cmsRun config file:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${MY_START}" | /usr/bin/bc`
   echo -e "\n\nwriting cmsRun config file (@${MY_TMP} sec):"
   echo "---------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
/bin/cat > ecal_pedestals.py <<EOF
import FWCore.ParameterSet.Config as cms

process = cms.Process("TEST")
process.PoolDBESSource = cms.ESSource("PoolDBESSource",
    DBParameters = cms.PSet(
        messageLevel = cms.untracked.int32(0)
    ),
    timetype = cms.string('runnumber'),
    toGet = cms.VPSet(cms.PSet(
        record = cms.string('EcalPedestalsRcd'),
        tag = cms.string('EcalPedestals_mc')
    )),
    connect = cms.string('frontier://FrontierProd/CMS_CONDITIONS')
)

process.source = cms.Source("EmptySource",
    numberEventsInRun = cms.untracked.uint32(1),
    firstRun = cms.untracked.uint32(1)
)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(5)
)
process.get = cms.EDAnalyzer("EventSetupRecordDataGetter",
    verbose = cms.untracked.bool(True),
    toGet = cms.VPSet(cms.PSet(
        record = cms.string('EcalPedestalsRcd'),
        data = cms.vstring('EcalPedestals')
    ))
)

process.dump = cms.OutputModule("AsciiOutputModule")

process.p = cms.Path(process.get)
process.ep = cms.EndPath(process.dump)

EOF
if [ $? -ne 0 ]; then
   echo "   [E] error writing cmsRun config file"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="error writing cmsRun config file"
   fi
fi
#
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo "   `/bin/ls -l ecal_pedestals.py`"
   if [ ${SWN_VERBOSE} -ge 3 ]; then
      /bin/cat ecal_pedestals.py
      /usr/bin/awk '{printf "   %s\n",$0}' ecal_pedestals.py
   fi
fi
# ########################################################################### #



# check DB FroNtier access in cmsRun:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${MY_START}" | /usr/bin/bc`
   echo -e "\n\nchecking DB FroNtier access in cmsRun (@${MY_TMP} sec):"
   echo "------------------------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
#
cmsRun ecal_pedestals.py 1>output.log 2>&1
MY_RC=$?
if [ ${MY_RC} -ne 0 ]; then
   /usr/bin/awk '{printf "   %s\n",$0}' output.log
   echo "   [E] cmsRun failed, rc=${MY_RC}"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="cmsRun failed"
   fi
elif [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${MY_START}" | /usr/bin/bc`
   echo "   cmsRun complete (@${MY_TMP} sec)"
fi
# ########################################################################### #



# check cmsRun output:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   echo -e "\n\nchecking cmsRun output (@${MY_TMP} sec):"
   echo "---------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
#
# check for direct/non-proxy connection to DB FroNtier server:
/bin/grep -q "Trying direct connect to server" output.log
if [ ${MY_RC} -ne 0 ]; then
   echo "   [E] direct/non-proxy connection to CERN DB FroNtier server(s)"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="direct DB FroNtier connection"
   fi
fi
#
# check for proxy fail-over:
/bin/grep -q "Trying next proxy" output.log
if [ ${MY_RC} -ne 0 ]; then
   echo "   [W] connection via one (or more) squid proxy services failed"
   if [ ${SWNRC} -eq ${SWN_OK} ]; then
      SWNRC=${SWN_WARNING}
      SUMMRY="a squid proxy failed"
   fi
fi
#
cd ../..
# ########################################################################### #



if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_END=`/bin/date -u +'%s' 2>/dev/null`
   TMP1=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   TMP2=`echo "${MY_END} - ${MY_START}" | /usr/bin/bc`
   echo -e "\n\nEnding CMS DB FroNtier check at ${TMP1} UTC (${TMP2} sec)"
   MY_TMP=`swn2name ${SWNRC}`
   echo "   ${MY_TMP}: ${SUMMRY}"
else
   MY_TMP=`swn2name ${SWNRC}`
   echo "${MY_TMP^^}: ${SUMMRY}"
fi
swn_exit ${SWNRC} "${SUMMRY}"
