#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode basic probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if alive:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; wait ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'swn_exit ${SWN_CRITICAL} "Execution timeout"' 14
#
#
#
export PATH
#
#
SWNBC_VERSION="v1.01.10"
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      echo 0
   elif [ $1 -eq 1 ]; then
      echo 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      echo 2
   else
      echo 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      echo 1
   else
      echo 0
   fi
}
#
swn2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Unknown" ;;
   3) echo "Error" ;;
   *) echo "Critical" ;;
   esac
}
#
sam2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Critical" ;;
   *) echo "Unknown" ;;
   esac
}
#
swn_exit () {
   if [ "${SWN_MODE}" = "sam" ]; then
      echo -e "\n</PRE>"
      #
      # switch stdout/stderr back:
      exec 1>&3 2>&4
      #
      # translate SWN code into SAM codes:
      MYRC=`swn2sam $1`
      #
      # print summary followed by detailed log:
      MYNAME=`sam2name ${MYRC}`
      echo -e "${MYNAME^^}: $2\n"
      if [ -f swnbs.$$.log ]; then
         /bin/cat swnbs.$$.log
         /bin/rm -f swnbs.$$.log
      fi
      #
      exit ${MYRC}

   elif [ "${SWN_MODE}" = "psst" ]; then
      # translate return code into PSST codes:
      MYRC=`swn2psst $1`
      #
      exit ${MYRC}

   else
      exit $1
   fi
}
#
timeout_handler () {
   /bin/sleep ${SWN_TIMEOUT}
   echo -e "\ntimeout of ${SWN_TIMEOUT} seconds reached\n"
   ALARM_PID=${BASHPID}
   PID_LIST=`/bin/ps --ppid $1 -o pid= | /bin/grep -v ${ALARM_PID}`
   if [ -n "${PID_LIST}" ]; then
      kill -9 ${PID_LIST//$'\n'/ }
   fi
   kill -ALRM $1
}
#
SWNRC=${SWN_OK}
SUMMRY="Basic check ok"
# ########################################################################### #



# handle command line options, setup timeout, and print OS:
if [ -n "${SAME_SENSOR_HOME}" ]; then
   SWN_MODE="sam"
   SWN_VERBOSE=2
else
   SWN_MODE="interactive"
   SWN_VERBOSE=0
fi
SWN_TIMEOUT=60
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SWN_MODE="sam"
       SWN_VERBOSE=2
       ;;
   -P) SWN_MODE="psst"
       SWN_VERBOSE=-1
       ;;
   -t) IARG=`expr ${IARG} + 1`
       SWN_TIMEOUT=${!IARG}
       ;;
   -v) SWN_VERBOSE=`expr ${SWN_VERBOSE} + 1`
       ;;
   -vv) SWN_VERBOSE=2
       ;;
   -vvv) SWN_VERBOSE=3
       ;;
   -vvvv) SWN_VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode Basic probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [60]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
#
if [ "${SWN_MODE}" = "sam" ]; then
   # switch stdout/stderr to file (for later output after summary message):
   exec 3>&1 4>&2 1>swnbs.$$.log 2>&1
   #
   echo "<PRE>"
fi
#
timeout_handler $$ &
ALARM_PID=$!
#
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   TMP1=`/bin/hostname -f`
   TMP2=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   TMP3=`/bin/date +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   echo -e "\nStarting CMS basic check of ${TMP1} at ${TMP2} UTC"
   echo -e "   WN-01basic version ${SWNBC_VERSION}"
   echo -e "   local time ${TMP3}"
   unset TMP3
   unset TMP2
   unset TMP1
fi
SWN_START=`/bin/date -u +'%s' 2>/dev/null`
#
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo -e "\n\nOperatingSystem information:"
   echo "----------------------------"
   /bin/uname -a
   TMP1=`/bin/uname -r | /usr/bin/awk -F. '{print $(NF-1);exit}'`
   if [ "${TMP1}" = "el7" ]; then
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="outmoded/antiquated OS version"
      fi
   fi
   unset TMP1
   if [ -r /etc/os-release ]; then
      TMP=`. /etc/os-release; echo ${PRETTY_NAME}`
      if [ -n "${TMP}" ]; then
         echo "${TMP}"
      else
         /bin/sh -c '. /etc/os-release; echo "${NAME} ${VERSION}"'
      fi
   elif [ -r /etc/redhat-release ]; then
      /bin/cat /etc/redhat-release
   elif [ -r /etc/centos-release ]; then
      /bin/cat /etc/centos-release
   elif [ -r /etc/almalinux-release ]; then
      /bin/cat /etc/almalinux-release
   elif [ -r /etc/SuSE-release ]; then
      /bin/cat /etc/SuSE-release
   fi
   if [ -x /lib64/ld-linux-x86-64.so.2 ]; then
      /lib64/ld-linux-x86-64.so.2 --help | /usr/bin/awk 'BEGIN{p=0}{if((p==1)&&(length($0)>0)){print $0}else{if(index($0,"--version")>0){p=1}}}'
   fi
fi
# ########################################################################### #



# assume commands equivalent to coreutils are available and probe any other
#   commands we need/use:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo -e "\n\nChecking commands beyond coreutils:"
   echo "-----------------------------------"
fi
CMD_LIST="/bin/ps /usr/bin/which /usr/bin/awk /usr/bin/uptime /bin/hostname /usr/bin/curl /usr/bin/bc /usr/bin/attr /bin/grep"
for CMD in ${CMD_LIST}; do
   if [ ! -x ${CMD} ]; then
      echo "[C] OS command \"${CMD}\" unavailable"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="Missing command"
      fi
   fi
done
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
elif [ ${SWN_VERBOSE} -ge 2 ]; then
   if [ "${PATH+x}" = "x" ]; then
      echo "env.variable PATH includes"
      echo -e "   ${PATH//:/$'\n   '}"
   else
      echo "env.variable PATH is not set"
   fi
   #
   if [ "${LD_LIBRARY_PATH+x}" = "x" ]; then
      echo "env.variable LD_LIBRARY_PATH is set to \"${LD_LIBRARY_PATH}\""
   else
      echo "env.variable LD_LIBRARY_PATH is not set"
   fi
   if [ "${LD_PRELOAD+x}" = "x" ]; then
      echo "env.variable LD_PRELOAD      is set to \"${LD_PRELOAD}\""
   else
      echo "env.variable LD_PRELOAD      is not set"
   fi
   #
   echo "Required commands available"
fi
# ########################################################################### #



# try to identify worker node environment, container, VM, machine, etc.:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nIdentifying worker node environment (@${MY_TMP} sec):"
   echo "---------------------------------------------"
   #
   if [ -n "${APPTAINER_CONTAINER}" ]; then
      echo -n -e "We seem to be running inside an Apptainer container\n   "
      SWNB_ENV=Apptainer
   elif [ -n "${SINGULARITY_CONTAINER}" ]; then
      echo -n -e "We seem to be running inside a Singularity container\n   "
      SWNB_ENV=Singularity
   elif [ -n "${SHIFTER}" -a -n "${SHIFTER_RUNTIME}" ]; then
      echo "We seem to be running inside a Shifter container"
      SWNB_ENV=Shifter
   elif [ -f /.dockerenv ]; then
      if [ -r /proc/1/cgroup ]; then
         SWNB_ENV=`/usr/bin/awk -F: '{n=split($2,a,",");for(i=1;i<=n;i++){if(a[i]=="cpu"){n=split($3,b,"/");for(j=1;j<=n;j++){if((b[j]=="lxc")||(b[j]=="docker")){print "Docker"}}exit}}}' /proc/1/cgroup`
         if [ "${SWNB_ENV}" = "Docker" ]; then
            echo "We seem to be running inside a Docker container"
         fi
      else
         echo "We could be running inside a Docker container"
      fi
   fi
   if [ -z "${SWNB_ENV}" ]; then
      SWNB_ENV=`/usr/bin/awk -F: '/^flags[ \t]*:/ {n=split($2,a," ");for(i=1;i<=n;i++) {if(a[i]=="hypervisor"){print "Hypervisor"}}exit}' /proc/cpuinfo`
      if [ "${SWNB_ENV}" = "Hypervisor" ]; then
         echo -n -e "We seem to be run by a Hypervisor\n   "
         if [ -e /sys/devices/virtual/dmi/id/product_name ]; then
            /usr/bin/cat /sys/devices/virtual/dmi/id/product_name
         else
            echo "no DMI product name"
         fi
      fi
   fi
   if [ -z "${SWNB_ENV}" -a ${SWN_VERBOSE} -ge 2 ]; then
      echo "We are most likely running on a physical machine"
   fi
fi
# ########################################################################### #



# get processor/memory configuration and check CPU load/memory utilization:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nChecking CPU and memory load (@${MY_TMP} sec):"
   echo "--------------------------------------"
   #
fi
/usr/bin/awk -F: 'BEGIN{np=0}{if(substr($1,1,10)=="processor\t"){np+=1};if(substr($1,1,11)=="model name\t"){mn=$2;gsub(/^[ \t]+/,"",mn)};if(substr($1,1,8)=="cpu MHz\t"){sp=$2;gsub(/^[ \t]+/,"",sp)}}END{printf "   %d %s currently at %s MHz\n",np,mn,sp}' /proc/cpuinfo
SWNB_CPU=`/usr/bin/awk 'BEGIN{np=0}/^processor[ \t]*:/{np+=1}END{print np}' /proc/cpuinfo`
if [ "${SWN_MODE}" = "sam" ]; then
   SWNB_SLOT=1
elif [ "${SWN_MODE}" = "psst" ]; then
   if [ -n "${GLIDEIN_CPUS}" ]; then
      SWNB_SLOT=${GLIDEIN_CPUS}
   else
      SWNB_SLOT=8
   fi
else
   SWNB_SLOT=${SWNB_CPU}
fi
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo "Assuming ${SWNB_CPU} cores of which ${SWNB_SLOT} are for our use"
   #
   /usr/bin/awk 'BEGIN{mt=0;ma=0;st=0;sf=0}{if($1=="MemTotal:"){mt=$2};if($1=="MemAvailable:"){ma=$2};if($1=="SwapTotal:"){st=$2};if($1=="SwapFree:"){sf=$2}}END{printf "   Memory:   total %.3f GiB   available %.3f GiB\n   Swap:     total %.3f GiB   used %.3f GiB\n",mt/1048576,ma/1048576,st/1048576,(st-sf)/1048576}' /proc/meminfo
fi
TMP=`/usr/bin/awk -vslot=${SWNB_SLOT} '/^MemAvailable:/{a=int($2/slot/1024);print a;exit}' /proc/meminfo`
if [ ${TMP} -lt 1000 ]; then
   echo "[C] Inusufficient memory available, ${TMP} MiB/core"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
      SWNRC=${SWN_CRITICAL}
      SUMMRY="critical low memory issue"
   fi
elif [ ${TMP} -lt 2000 ]; then
   echo "[E] Incomplete memory available, ${TMP} MiB/core"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="low memory error"
   fi
fi
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo -n -e "Current system load:\n  "
   /usr/bin/uptime
fi
# check control group setup:
TMP=""
if [ -r /proc/$$/cgroup ]; then
   TMP=`/usr/bin/awk -F: '{n=split($2,a,",");for(i=1;i<=n;i++){if(a[i]=="cpu"){if(($3!="/")&&($3!="/-.slice")){print "cgroups"}exit}}}' /proc/$$/cgroup`
fi
if [ -n "${TMP}" ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo "   control groups setup for \"cpu\" scheduling"
   fi
else
   # count non-niced CPU usage:
   TMP=`/bin/ps -e -o ppid= -o nice= -o pcpu= -o uid= | /usr/bin/awk -vme=$$ 'BEGIN{u=0.0;l=":"}{if(($1!=me)&&(int($2)<=0)){u+=$3};if(index(l,":"$4":")<=0){l=l$4":"}}END{n=split(l,a,":");if(n>3){print u/100}else{print "noaccess"}}'`
   if [ "${TMP}" = "noaccess" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "   no access to process info of other users, using 1 min load"
      fi
      # fallback to 1 min load average
      TMP=`/usr/bin/uptime | /usr/bin/awk '{print substr($(NF-2),1,length($(NF-2))-1);exit}'`
   else
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "   non-niced CPU usage: ${TMP} cores of ${SWNB_CPU} cores total"
      fi
   fi
   TMP1=`echo ${TMP} | /usr/bin/awk -vcpu=${SWNB_CPU} '{print int(100*($1)/cpu);exit}'`
   TMP2=`echo ${TMP} | /usr/bin/awk -vcpu=${SWNB_CPU} -vslot=${SWNB_SLOT} '{print int(100*cpu/($1+slot));exit}'`
   if [ ${TMP1} -gt 200 ]; then
      echo "[C] CPU overloaded, 1 min load ${TMP}%"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="critical CPU overload issue"
      fi
   elif [ ${TMP2} -lt 67 ]; then
      if [ ${SWN_VERBOSE} -ge 0 ]; then
         echo "[W] High CPU load, expect CPU usage only ${TMP2}%"
      fi
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="high CPU load issue"
      fi
   fi
   unset TMP2
   unset TMP1
fi
unset TMP
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
echo ""
# ########################################################################### #



# find out user, account, and authentication information:
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nChecking user, account, and credentials (@${MY_TMP} sec):"
      echo "-------------------------------------------------"
   fi
   #
   /usr/bin/id
   echo "#"
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      if [ "${HOME+x}" = "x" ]; then
         echo "env.variable HOME set to ${HOME}"
         /bin/ls -ld "${HOME}"
      else
         echo "env.variable HOME not set"
      fi
      echo "#"
      #
      ulimit -a
      echo "#"
   fi
   #
   if [ -n "${X509_USER_PROXY}" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "X509_USER_PROXY is set to ${X509_USER_PROXY}:"
         if [ -x /usr/bin/voms-proxy-info -a ${SWN_VERBOSE} -ge 2 ]; then
            /usr/bin/voms-proxy-info -all -file ${X509_USER_PROXY}
         fi
      fi
   elif [ -r "/tmp/x509up_u`/usr/bin/id -u`" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "/tmp/x509up_u`/usr/bin/id -u` exists:"
         if [ -x /usr/bin/voms-proxy-info -a ${SWN_VERBOSE} -ge 2 ]; then
            /usr/bin/voms-proxy-info -all -file /tmp/x509up_u`/usr/bin/id -u`
         fi
      fi
   else
      echo "[E] no x509 certificate"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="no x509 certificate"
      fi
   fi
   #
   if [ -n "${BEARER_TOKEN}" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "BEARER_TOKEN is set:"
         if [ -x /usr/bin/httokendecode -a ${SWN_VERBOSE} -ge 2 ]; then
            echo "${BEARER_TOKEN}" | /usr/bin/httokendecode -H -
         else
            echo "${BEARER_TOKEN}" | /usr/bin/awk \
            '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}'
         fi
      fi
   elif [ -n "${BEARER_TOKEN_FILE}" -a \
          -r "${BEARER_TOKEN_FILE}" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "BEARER_TOKEN_FILE set and readable:"
         if [ -x /usr/bin/httokendecode -a ${SWN_VERBOSE} -ge 2 ]; then
            /usr/bin/httokendecode -H ${BEARER_TOKEN_FILE}
         else
            /usr/bin/awk \
               '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
               ${BEARER_TOKEN_FILE}
         fi
      fi
   elif [ -n "${XDG_RUNTIME_DIR}" -a \
          -r "${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "XDG_RUNTIME_DIR set and euid-based file readable:"
         if [ -x /usr/bin/httokendecode -a ${SWN_VERBOSE} -ge 2 ]; then
            /usr/bin/httokendecode -H ${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`
         else
            /usr/bin/awk \
               '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
               ${XDG_RUNTIME_DIR}/bt_u`/usr/bin/id -u`
         fi
      fi
   elif [ -r "/tmp/bt_u`/usr/bin/id -u`" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "default euid-based file readable:"
         if [ -x /usr/bin/httokendecode -a ${SWN_VERBOSE} -ge 2 ]; then
            /usr/bin/httokendecode -H /tmp/bt_u`/usr/bin/id -u`
         else
            /usr/bin/awk \
               '{printf "%.8s...%s\n",$1,substr($1,length($1)-7);exit}' \
               /tmp/bt_u`/usr/bin/id -u`
         fi
      fi
   else
      echo "[N] no IAM-issued token"
      #if [ ${SWNRC} -eq ${SWN_OK} ]; then
      #   SWNRC=${SWN_WARNING}
      #   SUMMRY="no token warning"
      #fi
   fi
   #
   echo ""
fi
# ########################################################################### #



# check available disk space:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nChecking disk space (@${MY_TMP} sec):"
   echo "-----------------------------"
fi
MYPWD=`/usr/bin/pwd`
echo "Current working directory \"${MYPWD}\""
if [ ${SWN_VERBOSE} -gt 1 ]; then
   /bin/df -k ${MYPWD} | /usr/bin/awk '{getline; printf "   %s\n",$0;exit}'
   /usr/bin/stat -f -c %T ${MYPWD} | /usr/bin/awk '{getline; printf "   filesystem type: %s\n",$0;exit}'
fi
TMP1=`/usr/bin/df -k ${MYPWD} | /usr/bin/awk 'BEGIN{a="?.???"}{for(i=1;i<NF;i++){if($i=="Available"){getline; a=sprintf("%.3f",$i/1048576);exit}}}END{print a}'`
if [ "${TMP1}" != "?.???" ]; then
   echo "   its filesystem has ${TMP1} GiB free space"
   TMP2=`echo "(${TMP1} / ${SWNB_SLOT})/1" | /usr/bin/bc`
   if [ ${TMP2} -lt 10 ]; then
      echo "   [C] insufficient free scratch/current working directory space"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="insufficient free scratch space"
      fi
   elif [ ${TMP2} -lt 20 ]; then
      if [ ${SWN_VERBOSE} -ge 0 ]; then
         echo "   [W] incomplete free scratch/current working directory space"
      fi
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="incomplete free scratch space"
      fi
   fi
else
   echo "   [N] failed to get available scratch/current working directory space"
fi
TMP1=`/usr/bin/df -k /tmp | /usr/bin/awk 'BEGIN{a="?.???"}{for(i=1;i<NF;i++){if($i=="Available"){getline; a=sprintf("%.3f",$i/1048576);exit}}}END{print a}'`
if [ "${TMP1}" != "?.???" ]; then
   echo "System /tmp filesystem has ${TMP1} GiB free space"
   TMP2=`echo "(${TMP1} * 1024) / 1" | /usr/bin/bc`
   if [ ${TMP2} -lt 512 ]; then
      echo "   [C] insufficient free /tmp space"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="insufficient free /tmp space"
      fi
   elif [ ${TMP2} -lt 1024 ]; then
      echo "   [E] not enough free /tmp space"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="not enough free /tmp space"
      fi
   fi
else
   echo "   [N] failed to get available system /tmp filesystem space"
fi
unset TMP2
unset TMP1
#
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo ""
   echo "ETFROOT           at ${ETFROOT}"
   echo "ETF_OUT_DIR       at ${ETF_OUT_DIR}"
   echo "SAME_SENSOR_HOME  at ${SAME_SENSOR_HOME}"
fi
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
# ########################################################################### #



# check IPv4 and v6 connectivity to cern and system clock:
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nChecking IP connectivity (@${MY_TMP} sec):"
   echo "----------------------------------"
fi
SWN_IPv4=`/usr/bin/awk 'BEGIN{c=""}{if(($2=="00000000")&&(and(strtonum("0x"$4),2)==2)){c=$1;exit}}END{print c}' /proc/net/route 2>/dev/null`
SWN_IPv6=`/usr/bin/awk 'BEGIN{c=""}{if(($1=="00000000000000000000000000000000")&&(and(strtonum("0x"$9),2)==2)){c=$10;exit}}END{print c}' /proc/net/ipv6_route 2>/dev/null`
if [ -n "${SWN_IPv4}" -a -n "${SWN_IPv6}" ]; then
   echo "Network config: dual stack"
elif [ -n "${SWN_IPv4}" -a -z "${SWN_IPv6}" ]; then
   echo "Network config: IPv4-only"
elif [ -z "${SWN_IPv4}" -a -n "${SWN_IPv6}" ]; then
   echo "Network config: IPv6-only"
else
   echo "Network config: no global IP gateway"
fi
#
if [ "${SWN_MODE}" != "psst" ]; then
   CERNNOW=""
   if [ -n "${SWN_IPv4}" ]; then
      TMP=`/usr/bin/curl -s -S -k -4 --max-time 10 --head https://www.cern.ch/`
      if [ $? -eq 0 ]; then
         MYNOW=`/bin/date -u +'%s' 2>/dev/null`
         echo "IPv4 connectivity to CERN"
         TMP=`echo "${TMP}" | /usr/bin/awk '/^Date: .*/ {print substr($0,7)}'`
         if [ -n "${TMP}" ]; then
            echo "   timestamp in header ${TMP}"
            CERNNOW=`/bin/date -u --date="${TMP}" +'%s'`
         else
            echo "   no timestamp in header"
         fi
      else
         echo "[E] No IPv4 connectivity to CERN"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="No IPv4 connectivity to CERN"
         fi
      fi
   else
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="No IPv4 access"
      fi
   fi
   #
   if [ -n "${SWN_IPv6}" ]; then
      TMP=`/usr/bin/curl -s -S -k -6 --max-time 10 --head https://www.cern.ch/`
      if [ $? -eq 0 ]; then
         echo "IPv6 connectivity to CERN"
         if [ -z "${CERNNOW}" ]; then
            MYNOW=`/bin/date -u +'%s' 2>/dev/null`
            TMP=`echo "${TMP}" | /usr/bin/awk '/^Date: .*/ {print substr($0,7)}'`
            if [ -n "${TMP}" ]; then
               echo "   timestamp in header ${TMP}"
               CERNNOW=`/bin/date -u --date="${TMP}" +'%s'`
            else
               echo "   no timestamp in header"
            fi
         fi
      else
         echo "[W] No IPv6 connectivity to CERN"
         if [ ${SWNRC} -eq ${SWN_OK} ]; then
            SWNRC=${SWN_WARNING}
            SUMMRY="No IPv6 connectivity to CERN"
         fi
      fi
   else
      if [ ${SWNRC} -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="No IPv6 access"
      fi
   fi
   #
   if [ -n "${CERNNOW}" ]; then
      TMP=`echo "d=${CERNNOW}-${MYNOW};if(d<0){-d}else{d}" | /usr/bin/bc`
      if [ ${TMP} -le 15 ]; then
         echo "Node and CERN time within ${TMP} seconds"
      else
         echo "Node and CERN time are off by ${TMP} seconds"
         TMP=`/usr/bin/curl -s -S -k --max-time 10 --head https://www.ntppool.org/ | /usr/bin/awk '/^[Dd]ate: .*/ {print substr($0,7)}'`
         if [ $? -eq 0 ]; then
            MYNOW=`/bin/date -u +'%s' 2>/dev/null`
            NTPNOW=`/bin/date -u --date="${TMP}" +'%s'`
            TMP=`echo "d=${NTPNOW}-${MYNOW};if(d<0){-d}else{d}" | /usr/bin/bc`
            if [ ${TMP} -gt 900 ]; then
               echo "[E] Wrong time on the node, ${TMP} seconds off"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="Node date/time error"
               fi
            else
               echo "[W] Time on the node off by ${TMP} seconds"
               if [ ${SWNRC} -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="Node date/time issue"
               fi
            fi
         else
            echo "[W] Failed to get from ntppool.org"
            if [ ${SWNRC} -eq ${SWN_OK} ]; then
               SWNRC=${SWN_WARNING}
               SUMMRY="Node date/time issue"
            fi
         fi
      fi
   fi
fi
# ########################################################################### #



# check environment settings:
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nChecking environment settings (@${MY_TMP} sec):"
      echo  "--------------------------------------"
   fi
   MY_OS=`/bin/uname -r | /usr/bin/awk -F. '{print $(NF-1),$NF; exit}'`
   if [ -n "${MY_OS}" ]; then
      MY_TMP=`/usr/bin/printenv | /bin/grep ${MY_OS%% *} | /bin/grep ${MY_OS##* } | /usr/bin/awk -F= '{printf " %s",$1}' 2>/dev/null`
      if [ -n "${MY_TMP}" ]; then
         echo "[N] Environment contains variable(s) with OS reference"
         echo "  ${MY_TMP}"
      else
         echo "no env.variables with OS reference found"
      fi
   fi
   #
   MY_TMP=`/usr/bin/printenv | /bin/grep "/osg-wn-client/" | /usr/bin/awk -F= '{printf " %s",$1}' 2>/dev/null`
   if [ -n "${MY_TMP}" ]; then
      echo "[N] Environment contains variable(s) with OSG Worker Node reference"
      echo "  ${MY_TMP}"
   else
      echo "no env.variables with OSG Worker Node reference found"
   fi
   #
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      if [ "${VO_CMS_SW_DIR+x}" = "x" ]; then
         echo "env.variable VO_CMS_SW_DIR   is set to \"${VO_CMS_SW_DIR}\""
      else
         echo "env.variable VO_CMS_SW_DIR   is not set"
      fi
      if [ "${OSG_APP+x}" = "x" ]; then
         echo "env.variable OSG_APP         is set to \"${OSG_APP}\""
      else
         echo "env.variable OSG_APP         is not set"
      fi
      #
      if [ "${CVMFS_MOUNT_DIR+x}" = "x" ]; then
         echo "env.variable CVMFS_MOUNT_DIR is set to \"${CVMFS_MOUNT_DIR}\""
      else
         echo "env.variable CVMFS_MOUNT_DIR is not set"
      fi
      if [ "${CVMFS+x}" = "x" ]; then
         echo "env.variable CVMFS           is set to \"${CVMFS}\""
      else
         echo "env.variable CVMFS           is not set"
      fi
      #
      if [ "${PYTHONPATH+x}" = "x" ]; then
         echo "env.variable PYTHONPATH      is set to \"${PYTHONPATH}\""
      else
         echo "env.variable PYTHONPATH      is not set"
      fi
      if [ "${PYTHON3PATH+x}" = "x" ]; then
         echo "env.variable PYTHON3PATH     is set to \"${PYTHON3PATH}\""
      else
         echo "env.variable PYTHON3PATH     is not set"
      fi
      #
      if [ "${PERL5LIB+x}" = "x" ]; then
         echo "env.variable PERL5LIB        is set to \"${PERL5LIB}\""
      else
         echo "env.variable PERL5LIB        is not set"
      fi
      #
      if [ "${GFAL_CONFIG_DIR+x}" = "x" ]; then
         echo "env.variable GFAL_CONFIG_DIR is set to \"${GFAL_CONFIG_DIR}\""
      else
         echo "env.variable GFAL_CONFIG_DIR is not set"
      fi
      if [ "${GFAL_PLUGIN_DIR+x}" = "x" ]; then
         echo "env.variable GFAL_PLUGIN_DIR is set to \"${GFAL_PLUGIN_DIR}\""
      else
         echo "env.variable GFAL_PLUGIN_DIR is not set"
      fi
   fi
fi
# ########################################################################### #



# check for python3:
# ==================
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo -e "\n\nchecking python3:"
   echo "-----------------"
fi
MYCMD=`/usr/bin/which python3 2>/dev/null`
if [ $? -ne 0 ]; then
   echo "[C] No python3 available"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
      SWNRC=${SWN_CRITICAL}
      SUMMRY="Python3 unavailable"
   fi
else
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      # get python version for log file:
      ${MYCMD} -V
   fi
   TMP=`${MYCMD} -c "import os,sys,pwd,shutil,io,platform,time,calendar,socket,argparse,traceback,subprocess,re,random,base64,xml.etree.ElementTree,json,urllib.request,urllib.error,difflib,signal;sys.exit(0)" 2>&1`
   if [ $? -ne 0 ]; then
      echo "[C] Missing python3 module\(s\):"
      echo -e "${TMP}" | /usr/bin/awk -F: '/ModuleNotFoundError:/ {printf "   %s\n",$2}'
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="Python3 missing module(s)"
      fi
   fi
   #for MYMOD in platform time socket argparse traceback subprocess re xml.etree.ElementTree json urllib.request urllib.error difflib dateutil.parser signal; do
   #   TMP=`${MYCMD} -c "import os,sys;import ${MYMOD};sys.exit(0)" 2>&1`
   #   if [ $? -ne 0 ]; then
   #      # try a local user pip install:
   #      ${MYCMD} -m pip install --user ${MYMOD} 1>/dev/null
   #      if [ $? -ne 0 ]; then
   #         echo "[C] Missing python3 module ${MYMOD}"
   #         if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
   #            SWNRC=${SWN_CRITICAL}
   #            SUMMRY="Python3 missing module"
   #         fi
   #      elif [ ${SWN_VERBOSE} -ge 1 ]; then
   #         echo "[N] Missing python3 module ${MYMOD} user pip installed"
   #      fi
   #   elif [ ${SWN_VERBOSE} -ge 3 ]; then
   #      echo "[D] Python3 module ${MYMOD} available"
   #   fi
   #fi
   unset TMP
fi
unset MYCMD
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
elif [ ${SWN_VERBOSE} -ge 2 ]; then
   echo "Python3 with required modules available"
fi
# ########################################################################### #



if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_END=`/bin/date -u +'%s' 2>/dev/null`
   TMP1=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   TMP2=`echo "${MY_END} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\nEnding CMS basic check at ${TMP1} UTC (${TMP2} sec)"
   echo -e "\n\nEnding CMS CVMFS check at ${TMP1} UTC (${TMP2} sec)"
   MY_TMP=`swn2name ${SWNRC}`
   echo "   ${MY_TMP}: ${SUMMRY}"
else
   MY_TMP=`swn2name ${SWNRC}`
   echo "${MY_TMP^^}: ${SUMMRY}"
fi
swn_exit ${SWNRC} "${SUMMRY}"
