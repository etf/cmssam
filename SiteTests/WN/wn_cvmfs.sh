#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode CVMFS probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if alive:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; wait ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'swn_exit ${SWN_CRITICAL} "Execution timeout"' 14
#
#
#
export PATH
#
#
SWNCV_VERSION="v1.01.12"
SWN_CVMFSES="cms.cern.ch cms-griddata.cern.ch oasis.opensciencegrid.org singularity.opensciencegrid.org grid.cern.ch"
SWN_VERSION_BAD="0. 1. 2.0. 2.1. 2.2. 2.3. 2.4."
SWN_VERSION_WARN="2.5. 2.6. 2.7. 2.8."
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      echo 0
   elif [ $1 -eq 1 ]; then
      echo 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      echo 2
   else
      echo 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      echo 1
   else
      echo 0
   fi
}
#
swn2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Unknown" ;;
   3) echo "Error" ;;
   *) echo "Critical" ;;
   esac
}
#
sam2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Critical" ;;
   *) echo "Unknown" ;;
   esac
}
#
swn_exit () {
   if [ "${SWN_MODE}" = "sam" ]; then
      echo -e "\n</PRE>"
      #
      # switch stdout/stderr back:
      exec 1>&3 2>&4
      cd ${SWN_HOME}
      #
      # translate SWN code into SAM codes:
      MYRC=`swn2sam $1`
      #
      # print summary followed by detailed log:
      MYNAME=`sam2name ${MYRC}`
      echo -e "${MYNAME^^}: $2\n"
      if [ -f swncv.$$.log ]; then
         /bin/cat swncv.$$.log
         /bin/rm -f swncv.$$.log
      fi
      #
      exit ${MYRC}

   elif [ "${SWN_MODE}" = "psst" ]; then
      # translate return code into PSST codes:
      MYRC=`swn2psst $1`
      #
      exit ${MYRC}

   else
      exit $1
   fi
}
#
timeout_handler () {
   /bin/sleep ${SWN_TIMEOUT}
   echo -e "\ntimeout of ${SWN_TIMEOUT} seconds reached\n"
   ALARM_PID=${BASHPID}
   PID_LIST=`/bin/ps --ppid $1 -o pid= | /bin/grep -v ${ALARM_PID}`
   if [ -n "${PID_LIST}" ]; then
      kill -9 ${PID_LIST//$'\n'/ }
   fi
   kill -ALRM $1
}
#
SWNRC=${SWN_OK}
SUMMRY="CVMFS check ok"
# ########################################################################### #



# handle command line options:
if [ -n "${SAME_SENSOR_HOME}" ]; then
   SWN_MODE="sam"
   SWN_VERBOSE=2
else
   SWN_MODE="interactive"
   SWN_VERBOSE=0
fi
SWN_TIMEOUT=120
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SWN_MODE="sam"
       SWN_VERBOSE=2
       ;;
   -P) SWN_MODE="psst"
       SWN_VERBOSE=-1
       ;;
   -t) IARG=`expr ${IARG} + 1`
       SWN_TIMEOUT=${!IARG}
       ;;
   -v) SWN_VERBOSE=`expr ${SWN_VERBOSE} + 1`
       ;;
   -vv) SWN_VERBOSE=2
       ;;
   -vvv) SWN_VERBOSE=3
       ;;
   -vvvv) SWN_VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode CVMFS probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [120]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
#
if [ "${SWN_MODE}" = "sam" ]; then
   # switch stdout/stderr to file (for later output after summary message):
   SWN_HOME=`/bin/pwd`
   exec 3>&1 4>&2 1>swncv.$$.log 2>&1
   #
   echo "<PRE>"
fi
#
timeout_handler $$ &
ALARM_PID=$!
#
#
SWNCV_CPU=`/usr/bin/awk 'BEGIN{np=0}/^processor[ \t]*:/{np+=1}END{print np}' /proc/cpuinfo`
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   TMP1=`/bin/hostname -f`
   TMP2=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   echo -e "\nStarting CMS CVMFS check of ${TMP1} at ${TMP2} UTC"
   echo "   WN-02cvmfs version ${SWNCV_VERSION}"
   if [ "${CVMFS_MOUNT_DIR+x}" = "x" ]; then
      echo "   env.variable CVMFS_MOUNT_DIR is set to ${CVMFS_MOUNT_DIR}"
   else
      echo "   env.variable CVMFS_MOUNT_DIR is not set"
   fi
   if [ "${CVMFS+x}" = "x" ]; then
      echo "   env.variable CVMFS is set to ${CVMFS}"
   else
      echo "   env.variable CVMFS is not set"
   fi
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo "   node has ${SWNCV_CPU} CPU cores"
   fi
   unset TMP2
   unset TMP1
fi
SWN_START=`/bin/date -u +'%s' 2>/dev/null`
SWN_HOME=`/bin/pwd`
if [ "${CVMFS_MOUNT_DIR+x}" != "x" ]; then
   if [ "${CVMFS+x}" = "x" ]; then
      CVMFS_MOUNT_DIR=${CVMFS}
   else
      CVMFS_MOUNT_DIR=/cvmfs
   fi
fi
# ########################################################################### #



# check filesystem implementation of each CVMFS area of interest:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo -e "\n\nCheck availability and implementation of areas:"
   echo "-----------------------------------------------"
else
   echo ""
fi
MY_CVMFSES=""
for MY_AREA in ${SWN_CVMFSES}; do
   MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
   #
   # trigger mount in case of auto-mounted area and check access:
   /usr/bin/timeout -k 5 30 /bin/ls -ld ${MY_PATH} 1>/dev/null
   MY_RC=$?
   if [ ${MY_RC} -eq 0 ]; then
      # hold mount in case of auto-mounted area:
      cd ${MY_PATH}
      if [ $? -eq 0 ]; then
         # get filesystem type:
         MY_MTAB=`/usr/bin/awk -vfs=${MY_PATH} 'BEGIN{e="";m=-1}{if(NF>=3){l=length($2);if(($2==substr(fs,1,l))&&(l>m)){e=$0;m=l}}}END{print e}' /etc/mtab`
         MY_FSTYPE=`echo "${MY_MTAB}" | /usr/bin/awk '{if($3=="fuse"){print $1}else{print $3};exit}'`
         echo "Filesystem type of ${MY_AREA} is ${MY_FSTYPE}"
         if [ "${MY_FSTYPE}" = "cvmfs" -o "${MY_FSTYPE}" = "cvmfs2" \
                                       -o "${MY_FSTYPE}" = "/dev/fuse" ]; then
            MY_CVMFSES="${MY_CVMFSES} ${MY_AREA}"
         elif [ "${MY_FSTYPE}" == "autofs" ]; then
            # automount probably failed
            MY_TMP=`echo "${MY_MTAB}" | /usr/bin/awk '{print $2;exit}'`
            if [ "${MY_TMP}" == "${CVMFS_MOUNT_DIR}" ]; then
               echo "   [C] automount of CVMFS filesystem ${MY_PATH} failed"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
                  SWNRC=${SWN_CRITICAL}
                  SUMMRY="critical CVMFS filesystem unreachable"
               fi
            else
               echo "   [W] CVMFS filesystem implementation info unreachable"
               echo "      /etc/mtab: ${MY_MTAB}"
               if [ ${SWNRC} -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="CVMFS filesystem implementation unreachable"
               fi
            fi
         elif [ "${MY_FSTYPE}" != "nfs" -a "${MY_FSTYPE}" != "nfs4" ]; then
            echo "   [E] Unsupported CVMFS filesystem implementation"
            echo "      /etc/mtab: ${MY_MTAB}"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="unsupported CVMFS filesystem implementation"
            fi
         fi
         MY_FSTYPE=""
         MY_MTAB=""
      else
         echo "[C] CVMFS filesystem ${MY_PATH} unreachable"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
            SWNRC=${SWN_CRITICAL}
            SUMMRY="critical CVMFS filesystem unreachable"
         fi
      fi
   else
      if [ ${MY_RC} -eq 124 -o ${MY_RC} -eq 137 ]; then
         echo "[C] ls of ${MY_PATH} timed out"
      else
         echo "[C] ls of ${MY_PATH} failed, rc=${MY_RC}"
      fi
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="critical CVMFS filesystem inaccessible"
      fi
   fi
   cd ${SWN_HOME}
   MY_PATH=""
done
MY_CVMFSES="${MY_CVMFSES# *}"
#
if [ -z "${MY_CVMFSES}" ]; then
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      echo "[N] No CVMFS mounts on the node"
   fi
fi
if [ -z "${MY_CVMFSES}" -o ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
# ########################################################################### #



# check CVMFS software version:
MY_VRSN=""
#
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo -e "\n\nCheck active/installed software versions:"
      echo        "-----------------------------------------"
   else
      echo ""
   fi
   #
   if [ -x /usr/bin/attr ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "active CVMFS versions:"
      fi
      MY_ACTV="999.0.0.0"
      for MY_AREA in ${MY_CVMFSES}; do
         MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
         #
         cd ${MY_PATH}
         if [ $? -eq 0 ]; then
            MY_TMP=`/usr/bin/attr -q -g version ${MY_PATH}`
            if [ ${SWN_VERBOSE} -ge 1 ]; then
               echo "   ${MY_TMP} for ${MY_AREA}"
            fi
            #
            if [ -n "${MY_TMP}" ]; then
               # lowest active version:
               MY_ACTV=`echo "${MY_TMP}" | /usr/bin/awk -vc=${MY_ACTV} '{split($0".0.0.0",a,".");split(c".0.0.0",b,".");x=int(sprintf("%3.3d%3.3d%3.3d%3.3d",a[1],a[2],a[3],a[4]));y=int(sprintf("%3.3d%3.3d%3.3d%3.3d",b[1],b[2],b[3],b[4]));if(x<y){print $0}else{print c};exit}'`
            else
               echo "[N] failed to xattr-query active CVMFS version of ${MY_AREA}"
            fi
            MY_TMP=""
         else
            echo "[E] failed to cd to ${MY_PATH} to xattr-query active CVMFS version"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="cd to CVMFS area failed"
            fi
         fi
         cd ${SWN_HOME}
         MY_PATH=""
      done
      if [ "${MY_ACTV}" != "999.0.0.0" ]; then
         MY_VRSN="${MY_ACTV}"
      fi
      MY_ACTV=""
   else
      echo "[W] no /usr/bin/attr command, cannot query active CVMFS versions"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no extended attribute command"
      fi
   fi
   #
   if [ -x /usr/bin/cvmfs2 ]; then
      MY_TMP=`/usr/bin/cvmfs2 -V`
      if [ ${SWN_VERBOSE} -ge 2 ]; then
         echo -e "Installed CVMFS version:\n   ${MY_TMP}"
      fi
      #
      if [ -z "${MY_VRSN}" ]; then
         MY_VRSN=`echo "${MY_TMP}" | /usr/bin/awk 'BEGIN{v=""}{for(i=1;i<NF;i++){if(tolower($i)=="version"){v=$(i+1);exit}}}END{print v}'`
      fi
   fi
   #
   if [ -x /usr/bin/rpm ]; then
      if [ ${SWN_VERBOSE} -ge 3 ]; then
         echo -e "\nInstalled RPMs:"
         /usr/bin/timeout 10 /usr/bin/rpm -q -a | /usr/bin/awk '/^cvmfs-/ {printf "   %s\n",$1}'
      fi
      #
      if [ -z "${MY_VRSN}" ]; then
         MY_VRSN=`/usr/bin/rpm -q -a | /usr/bin/awk -F- '/^cvmfs-[0-9]/ {n=split($3,a,".");if(n>=3){printf "%s-%s\n", $2,a[1]}else{print $2};exit}'`
      fi
   fi
   #
   #
   if [ -n "${MY_VRSN}" ]; then
      MY_TMP=`echo "${SWN_VERSION_BAD}" | /usr/bin/awk -vv=${MY_VRSN} 'BEGIN{e="ok"}{for(i=1;i<=NF;i++){l=length($i);if($i==substr(v,1,l)){e="bad"}}}END{print e}'`
      if [ "${MY_TMP}" != "ok" ]; then
         echo "   [E] obsolete/unsafe CVMFS version"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="obsolete/unsafe CVMFS version"
         fi
      else
         MY_TMP=`echo "${SWN_VERSION_WARN}" | /usr/bin/awk -vv=${MY_VRSN} 'BEGIN{e="ok"}{for(i=1;i<=NF;i++){l=length($i);if($i==substr(v,1,l)){e="warn"}}}END{print e}'`
         if [ "${MY_TMP}" != "ok" ]; then
            echo "   [W] outmoded/antiquated CVMFS version"
            if [ ${SWNRC} -eq ${SWN_OK} ]; then
               SWNRC=${SWN_WARNING}
               SUMMRY="outmoded/antiquated CVMFS version"
            fi
         fi
      fi
      MY_TMP=""
   else
      echo "[W] failed to determine CVMFS version"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="unknown CVMFS version"
      fi
   fi
fi
# ########################################################################### #



# analyze CVMFS config of areas:
MY_ALIEN_CACHE=""
MY_CACHE_BASE=""
MY_SHARED_CACHE="yes"
MY_QUOTA_LIMIT=""
MY_NFILES=""
MY_HIDE_MAGIC_XATTRS=""
#
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nAnalyzing CVMFS config of areas (@${MY_TMP} sec):"
   echo "-----------------------------------------"
else
   echo ""
fi
#
if [ -e /etc/cvmfs/config.sh -a -x /usr/bin/cvmfs_config ]; then
   for MY_AREA in ${MY_CVMFSES}; do
      if [ ${SWN_VERBOSE} -ge 2 ]; then
         echo "Area ${MY_AREA}:"
      fi
      MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
      cd ${MY_PATH}
      if [ $? -eq 0 ]; then
         MY_CFG=`(. /etc/cvmfs/config.sh; /usr/bin/cvmfs_config showconfig ${MY_AREA} | /usr/bin/awk -F# '{gsub("[ \t]+$","",$1);print $1}') 2>/dev/null`
         if [ -n "${MY_CFG}" ]; then
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_SERVER_URL"){gsub("[,;|]"," ",$2);n=split($2,a," ");for(i=1;i<=n;i++){j=match(a[i],"http[s]*://([^/]+)/.*",b);if(j!=0){printf " %s",b[1]}}exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -z "${MY_TMP}" ]; then
               echo "[E] no Stratum-1 servers for CVMFS area ${MY_AREA}"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                    ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="no Stratum-1 servers configured"
               fi
            elif [ ${SWN_VERBOSE} -ge 2 ]; then
               echo "   Stratum-1 servers: ${MY_TMP}"
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_HTTP_PROXY"){gsub("[,;|]"," ",$2);n=split($2,a," ");for(i=1;i<=n;i++){if(index(a[i],"auto")<=0){printf " %s",a[i]}};exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -z "${MY_TMP}" ]; then
               echo "[E] no proxy server for CVMFS area ${MY_AREA}"
               echo "${MY_CFG}" | /usr/bin/awk -F= '{if(($1=="CVMFS_HTTP_PROXY")||($1=="CVMFS_FALLBACK_PROXY")||($1=="CVMFS_PAC_URLS")){printf "   %s\n",$0;exit}}'
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                    ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="no proxy server configured"
               fi
            elif [ "${MY_TMP}" == "DIRECT" ]; then
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                    ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="direct Stratum-1 access configured"
               fi
            elif [ ${SWN_VERBOSE} -ge 2 ]; then
               echo "   proxy servers: ${MY_TMP}"
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_ALIEN_CACHE"){print $2;exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -n "${MY_TMP}" ]; then
               if [ ${SWN_VERBOSE} -ge 2 ]; then
                  echo "   alien cache: ${MY_TMP}"
               fi
               if [ -z "${MY_ALIEN_CACHE}" ]; then
                  MY_ALIEN_CACHE=${MY_TMP}
               fi
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_CACHE_BASE"){print $2;exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -z "${MY_TMP}" ]; then
               echo "[E] no CVMFS_CACHE_BASE for CVMFS area ${MY_AREA}"
               if [ "${SWNRC}" -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="no CVMFS_CACHE_BASE configured"
               fi
            else
               if [ ${SWN_VERBOSE} -ge 2 ]; then
                  echo "   cache base: ${MY_TMP}"
               fi
               if [ -z "${MY_CACHE_BASE}" ]; then
                  MY_CACHE_BASE="${MY_TMP%% *}"
               fi
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_SHARED_CACHE"){print $2;exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -n "${MY_TMP}" ]; then
               if [ ${SWN_VERBOSE} -ge 2 ]; then
                  echo "   shared cache: ${MY_TMP}"
               fi
               if [ "${MY_TMP}" == "no" ]; then
                  MY_SHARED_CACHE="no"
                  #
                  MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_CACHE_DIR"){print $2;exit}}'`
                  MY_TMP="${MY_TMP# *}"
                  if [ -n "${MY_TMP}" -a ${SWN_VERBOSE} -ge 2 ]; then
                     echo "   cache area: ${MY_TMP}"
                  fi
               fi
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_QUOTA_LIMIT"){print int($2);exit}}'`
            if [ -z "${MY_TMP}" ]; then
               echo "[W] no CVMFS_QUOTA_LIMIT for CVMFS area ${MY_AREA}"
               if [ "${SWNRC}" -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="no CVMFS_QUOTA_LIMIT configured"
               fi
            else
               if [ ${SWN_VERBOSE} -ge 1 ]; then
                  echo "   quota limit: ${MY_TMP} MBytes"
               fi
               if [ -z "${MY_QUOTA_LIMIT}" ]; then
                  MY_QUOTA_LIMIT=${MY_TMP}
               fi
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_NFILES"){print int($2);exit}}'`
            if [ -z "${MY_TMP}" ]; then
               echo "[W] no CVMFS_NFILES for CVMFS area ${MY_AREA}"
               if [ "${SWNRC}" -eq ${SWN_OK} ]; then
                  SWNRC=${SWN_WARNING}
                  SUMMRY="no CVMFS_NFILES configured"
               fi
            else
               if [ ${SWN_VERBOSE} -ge 1 ]; then
                  echo "   max no files: ${MY_TMP}"
               fi
               if [ -z "${MY_NFILES}" ]; then
                  MY_NFILES=${MY_TMP}
               fi
            fi
            #
            MY_TMP=`echo "${MY_CFG}" | /usr/bin/awk -F= '{if($1=="CVMFS_HIDE_MAGIC_XATTRS"){print $2;exit}}'`
            MY_TMP="${MY_TMP# *}"
            if [ -n "${MY_TMP}" ]; then
               if [ ${SWN_VERBOSE} -ge 1 ]; then
                  echo "   xattr hidden: ${MY_TMP}"
               fi
               if [ -z "${MY_HIDE_MAGIC_XATTRS}" ]; then
                  MY_HIDE_MAGIC_XATTRS="${MY_TMP%% *}"
               fi
            fi
         else
            echo "[E] failed to cvmfs_config showconfig ${MY_AREA}"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="cvmfs_config showconfig failed"
            fi
         fi
         MY_CFG=""
      else
         echo "[E] failed to cd to ${MY_PATH} to get config"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
              ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="cd to CVMFS area failed"
         fi
      fi
      cd ${SWN_HOME}
      MY_PATH=""
   done
else
   echo "[E] No config.sh and/or cvmfs_config command available"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
        ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="config.sh/cvmfs_config missing"
   fi
fi
#
if [ -z "${MY_CACHE_BASE}" -o -z "${MY_QUOTA_LIMIT}" \
                           -o -z "${MY_NFILES}" ]; then
   echo "[E] failed to get CVMFS config information"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
        ${SWNRC} -ne ${SWN_ERROR} ]; then
      SWNRC=${SWN_ERROR}
      SUMMRY="config information inaccessible"
   fi
else
   #
   if [ -z "${MY_ALIEN_CACHE}" -a ${MY_QUOTA_LIMIT} -lt 20000 ]; then
      echo "[E] insufficient CVMFS cache quota of ${MY_QUOTA_LIMIT} Megabytes"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
           ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="insufficient CVMFS cache quota"
      fi
   fi
   #
   # CVMFS seems to "reserve" 512 file descriptors for itself
   MY_TMP=`echo "( 200 * ${SWNCV_CPU} ) + 512" | /usr/bin/bc`
   if [ ${MY_NFILES} -lt ${MY_TMP} ]; then
      echo "[E] insufficient CVMFS file descriptors ${MY_NFILES} for ${SWNCV_CPU} cores"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
           ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="insufficient CVMFS file descriptors"
      fi
   fi
   #
   if [ -n "${MY_ALIEN_CACHE}" -a -d "${MY_ALIEN_CACHE}" ]; then
      MY_ALIEN_SIZE=`/bin/df -k ${MY_ALIEN_CACHE} | /usr/bin/awk 'BEGIN{s=0}{for(i=1;i<NF;i++){if($i=="1K-blocks"){getline;s=int($i*0.001024);exit}}}END{print s}'`
      if [ ${MY_ALIEN_SIZE} -gt 0 ]; then
         MY_TMP1=`echo "scale=1; ${MY_ALIEN_SIZE} / 1000" | /usr/bin/bc`
         if [ ${SWN_VERBOSE} -ge 1 ]; then
            echo "CVMFS alien cache size ${MY_TMP1} GBytes"
         fi
         MY_TMP1=""
      else
         echo "[N] df command of ${MY_ALIEN_CACHE} failed, alien cache sizes unknown"
      fi
      MY_ALIEN_SIZE=""
   fi
   if [ -d ${MY_CACHE_BASE} ]; then
      # we assume all CVMFS area cache is shared (df -k is in KiB, we want MB):
      MY_CACHE_SIZE=`/bin/df -k ${MY_CACHE_BASE} | /usr/bin/awk 'BEGIN{s=0}{for(i=1;i<NF;i++){if($i=="1K-blocks"){getline;s=int($i*0.001024);exit}}}END{print s}'`
      #
      if [ ${MY_CACHE_SIZE} -gt 0 ]; then
         MY_TMP1=`echo "scale=1; ${MY_QUOTA_LIMIT} / 1000" | /usr/bin/bc`
         MY_TMP2=`echo "scale=1; ${MY_CACHE_SIZE} / 1000" | /usr/bin/bc`
         if [ ${SWN_VERBOSE} -ge 1 ]; then
            echo "CVMFS cache quota ${MY_TMP1} GBytes, cache size ${MY_TMP2} GBytes"
         fi
         #
         MY_TMP3=`echo "(( 1.10 * ${MY_QUOTA_LIMIT} ) + 1000)/1" | /usr/bin/bc`
         MY_TMP4=`echo "(( 1.20 * ${MY_QUOTA_LIMIT} ) + 1000)/1" | /usr/bin/bc`
         if [ ${MY_CACHE_SIZE} -lt ${MY_TMP3} ]; then
            echo "   [E] excessive CVMFS cache quota for cache size, ${MY_TMP2} GB"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="excessive CVMFS cache quota"
            fi
         elif [ ${MY_CACHE_SIZE} -lt ${MY_TMP4} ]; then
            echo "   [W] inappropriate CVMFS cache quota for cache size, ${MY_TMP2} GB"
            if [ ${SWNRC} -eq ${SWN_OK} ]; then
               SWNRC=${SWN_WARNING}
               SUMMRY="inappropriate CVMFS cache quota"
            fi
         fi
         MY_TMP4=""
         MY_TMP3=""
         MY_TMP2=""
         MY_TMP1=""
      else
         echo "[N] df command of ${MY_CACHE_BASE} failed, cache size unknown"
      fi
      MY_CACHE_SIZE=""
   else
      echo "[N] ${MY_CACHE_BASE} is not an accessible directory"
   fi
fi
# ########################################################################### #



# Check filesystem revisions of areas:
if [ "${SWN_MODE}" != "psst" ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nChecking CVMFS resisions  (@${MY_TMP} sec):"
      echo "-----------------------------------"
   else
      echo ""
   fi
   #
   if [ ! -x /usr/bin/attr ]; then
      echo "[W] no /usr/bin/attr command, cannot query filelsystem revisions"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="no extended attribute command"
      fi
   else
      # fetch CVMFS revisions summary file:
      MY_REVL=`/usr/bin/curl -f -s -S -k -m 30 https://cmssst.web.cern.ch/cvmfs/revisions.txt`
      MY_RC=$?
      if [ ${MY_RC} -ne 0 -o -z "${MY_REVL}" ]; then
         echo "[W] failed to fetch revisions list file, rc=${MY_RC}"
         if [ ${SWNRC} -eq ${SWN_OK} ]; then
            SWNRC=${SWN_WARNING}
            SUMMRY="failed to fetch revisions list file"
         fi
      else
         #
         for MY_AREA in ${MY_CVMFSES}; do
            MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
            #
            # get current revision of local CVMFS area:
            MY_REVN=`(cd ${MY_PATH}; /usr/bin/attr -q -g revision ${MY_PATH}) 2>/dev/null`
            if [ -z "${MY_REVN}" ]; then
               if [ "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
                  echo "[E] xattr-query of filesystem revision on ${MY_AREA} failed"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                       ${SWNRC} -ne ${SWN_ERROR} ]; then
                     SWNRC=${SWN_ERROR}
                     SUMMRY="filesystem version xattr-query failed"
                  fi
               else
                  echo "[N] xattr-query of filesystem revision on ${MY_AREA} failed"
               fi
            else
               #
               # get latest filesystem revision of area:
               MY_TMP=`echo "${MY_REVL}" | /usr/bin/awk -F: -va=${MY_AREA} '{if($1==a){print $2;exit}}'`
               if [ ${SWN_VERBOSE} -ge 1 ]; then
                  echo "   ${MY_AREA}: ${MY_REVN} (local)   ${MY_TMP} (Stratum-0)"
               fi
               #
               # find date of revision after that of local filesystem:
               MY_REVD=`echo "${MY_REVL}" | /usr/bin/awk -F: -va=${MY_AREA} -vr=${MY_REVN} -vn=${MY_NOW} 'BEGIN{d=n}{if(($1==a)&&($2>r)){d=$3}}END{print d}'`
               #
               # calculate time since release publication data:
               MY_TMP=`echo "( ${MY_NOW} - ${MY_REVD} ) / 3600" | /usr/bin/bc`
               if [ ${MY_TMP} -ge 24 ]; then
                  # over 24 hours old:
                  echo "[E] local filesystem out-dated, newer revision since ${MY_TMP} hours"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                       ${SWNRC} -ne ${SWN_ERROR} ]; then
                     SWNRC=${SWN_ERROR}
                     SUMMRY="local filesytem revision out-dated"
                  fi
               elif [ ${MY_TMP} -ge 4 ]; then
                  echo "   [W] local filesystem not up-to-date, newer revision since ${MY_TMP} hours"
                  if [ ${SWNRC} -eq ${SWN_OK} ]; then
                     SWNRC=${SWN_WARNING}
                     SUMMRY="local filesytem revision not up-to-date"
                  fi
               fi
            fi
            MY_REVD=""
            MY_REVN=""
            MY_PATH=""
         done
      fi
      MY_REVL=""
   fi
fi
# ########################################################################### #



# check recent I/O errors:
MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
if [ ${SWN_VERBOSE} -ge 1 ]; then
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nChecking for I/O errors (@${MY_TMP} sec):"
   echo "---------------------------------"
else
   echo ""
fi
#
if [ ! -x /usr/bin/attr ]; then
   echo "[W] no /usr/bin/attr command, cannot query I/O errors"
   if [ "${SWNRC}" -eq ${SWN_OK} ]; then
      SWNRC=${SWN_WARNING}
      SUMMRY="no extended attribute command"
   fi
else
   # loop over CVMFS areas and check I/O errors:
   for MY_AREA in ${MY_CVMFSES}; do
      MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
      #
      # get I/O error count:
      MY_IOEC=`(cd ${MY_PATH}; /usr/bin/attr -q -g nioerr ${MY_PATH}) 2>/dev/null`
      if [ -z "${MY_IOEC}" ]; then
         if [ "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
            echo "[E] xattr-query of I/O error count on ${MY_AREA} failed"
            if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                 ${SWNRC} -ne ${SWN_ERROR} ]; then
               SWNRC=${SWN_ERROR}
               SUMMRY="I/O error count xattr-query failed"
            fi
         else
            echo "[N] xattr-query of I/O error count on ${MY_AREA} failed"
         fi
      else
         #
         if [ ${SWN_VERBOSE} -ge 1 ]; then
            echo "   ${MY_AREA}: ${MY_IOEC} I/O error(s) since mount"
         fi
         #
         if [ ${MY_IOEC} -gt 0 ]; then
            #
            # get time of last I/O error:
            MY_IOED=`(cd ${MY_PATH}; /usr/bin/attr -q -g timestamp_last_ioerr ${MY_PATH}) 2>/dev/null`
            if [ -z "${MY_IOED}" ]; then
               MY_TMP=`echo "${MY_VRSN}" | /usr/bin/awk -F. 'BEGIN{e="notime"}{if((int($1)>2)||(($1=="2")&&(int($2)>=9))){e="ioerr_time"};exit}END{print e}'`
               if [ "${MY_HIDE_MAGIC_XATTRS}" != "yes" -a \
                    "${MY_TMP}" == "ioerr_time" ]; then
                  echo "[E] xattr-query of I/O error time on ${MY_AREA} failed"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                       ${SWNRC} -ne ${SWN_ERROR} ]; then
                     SWNRC=${SWN_ERROR}
                     SUMMRY="I/O error count xattr-query failed"
                  fi
               else
                  echo "[N] xattr-query of I/O error time on ${MY_AREA} failed"
               fi
            else
               #
               if [ ${SWN_VERBOSE} -ge 1 ]; then
                  MY_TMP=`/bin/date -u --date="@${MY_IOED}" +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
                  echo "      last I/O error at ${MY_IOED}, ${MY_TMP} UTC"
               fi
               #
               # calculate time since last I/O error:
               MY_TMP=`echo "${MY_NOW} - ${MY_IOED}" | /usr/bin/bc`
               if [ ${MY_TMP} -le 900 ]; then
                  echo "[C] I/O error ${MY_TMP} seconds ago"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
                     SWNRC=${SWN_CRITICAL}
                     SUMMRY="critical I/O error(s) in progress"
                  fi
               elif [ ${MY_TMP} -le 3600 ]; then
                  echo "      [E] I/O error ${MY_TMP} seconds ago"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                       ${SWNRC} -ne ${SWN_ERROR} ]; then
                     SWNRC=${SWN_ERROR}
                     SUMMRY="recent I/O error(s)"
                  fi
               fi
            fi
            MY_IOED=""
         fi
      fi
      MY_IOEC=""
      MY_PATH=""
   done
fi
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
# ########################################################################### #



# check CVMFS cache usage and statistics:
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nChecking CVMFS cache usage/statistics (@${MY_TMP} sec):"
      echo "-----------------------------------------------"
   else
      echo ""
   fi
   #
   MY_CACHEUSE=""
   MY_NCLEANUP24=""
   #
   # assume shared cache:
   MY_AREA="${MY_CVMFSES%% *}"
   MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
   #
   MY_CACHEUSE=`(cd ${MY_PATH} && /bin/df -P . | /usr/bin/awk '{getline;if(($3+$4)<=0){print "?.?"}else{printf "%.1f\n",(($3*100)/($3+$4))}exit}')`
   if [ -z "${MY_CACHEUSE}" ]; then
      echo "[E] df command of ${MY_AREA} cache failed, cache usage unknown"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
           ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="df of CVMFS cache failed"
      fi
   elif [ "${MY_CACHEUSE}" != "?.?" -o -z "${MY_ALIEN_CACHE}" ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "${MY_CACHEUSE}% of cache used"
      fi
   fi
   #
   MY_NCLEANUP24=`(cd ${MY_PATH}; /usr/bin/attr -q -g ncleanup24 ${MY_PATH})`
   if [ -z "${MY_NCLEANUP24}" ]; then
      if [ -z "${MY_ALIEN_CACHE}" -a "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
         echo "[E] xattr-query of cache cleanups on ${MY_AREA} failed"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
              ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="cache cleanup xattr-query failed"
         fi
      else
         echo "[N] xattr-query of cache cleanups on ${MY_AREA} failed"
      fi
   elif [ "${MY_NCLEANUP24}" == "-1" ]; then
       echo "[N] xattr-query of cache cleanups not supported"
   elif [ ${SWN_VERBOSE} -ge 1 ]; then
       echo "${MY_NCLEANUP24} cache cleanups in the last day"
   fi
   #
   if [ ${MY_NCLEANUP24} -ge 12 ]; then
      echo "[E] short, less than 2 hour CVMFS cache lifetime"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
           ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="cd to CVMFS area failed"
      fi
   elif [ ${MY_NCLEANUP24} -gt 4 ]; then
      echo "[W] short, less than 6 hour CVMFS cache lifetime"
      if [ "${SWNRC}" -eq ${SWN_OK} ]; then
         SWNRC=${SWN_WARNING}
         SUMMRY="short CVMFS cache lifetime"
      fi
   fi
fi
# ########################################################################### #



# check file descriptor usage:
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nChecking file descriptor usage (@${MY_TMP} sec):"
      echo "----------------------------------------"
   else
      echo ""
   fi
   MY_MAXFD=""
   MY_USEDFD=0
   #
   # get list of mounted CVMFS filesystems:
   MY_MOUNTED=`(. /etc/cvmfs/config.sh; /usr/bin/cvmfs_config status | /usr/bin/awk '{if(($2=="mounted")&&($3=="on")){print $4}}') 2>/dev/null`
   if [ -z "${MY_MOUNTED}" ]; then
      echo "[N] failed to cvmfs_config status"
      MY_MOUNTED=`/usr/bin/awk -vs=${CVMFS_MOUNT_DIR} 'BEGIN{l=length(s);if(substr(s,l,1)!="/"){s=s"/";l=l+1}}{if(substr($2,1,l)==s){print $2}}' /etc/mtab`
   fi
   if [ -z "${MY_MOUNTED}" ]; then
      echo "[E] failed to get a list of mounted CVMFS filesystems"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
           ${SWNRC} -ne ${SWN_ERROR} ]; then
         SWNRC=${SWN_ERROR}
         SUMMRY="no list of mounted CVMFS filesystems"
      fi
   else
      #
      for MY_PATH in ${MY_MOUNTED}; do
         MY_AREA="${MY_PATH##*/}"
         #
         if [ -z "${MY_MAXFD}" ]; then
            MY_TMP=`(cd ${MY_PATH}; /usr/bin/attr -q -g maxfd ${MY_PATH}) 2>/dev/null`
            if [ -z "${MY_TMP}" ]; then
               echo "${SWN_CVMFSES}" | /bin/grep -q "${MY_AREA}"
               if [ $? -eq 0 -a "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
                  echo "[E] xattr-query of max file descriptors on ${MY_AREA} failed"
                  if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                       ${SWNRC} -ne ${SWN_ERROR} ]; then
                     SWNRC=${SWN_ERROR}
                     SUMMRY="max file descriptors xattr-query failed"
                  fi
               else
                  echo "[N] xattr-query of max file descriptors on ${MY_AREA} failed"
               fi
            else
               #
               if [ ${SWN_VERBOSE} -ge 2 ]; then
                  echo "   ${MY_TMP} file descriptors for clients, ${MY_AREA}"
               fi
               #
               MY_MAXFD=${MY_TMP}
            fi
         fi
         #
         MY_TMP=`(cd ${MY_PATH}; /usr/bin/attr -q -g usedfd ${MY_PATH}) 2>/dev/null`
         if [ -z "${MY_TMP}" ]; then
            echo "${SWN_CVMFSES}" | /bin/grep -q "${MY_AREA}"
            if [ $? -eq 0 -a "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
               echo "[E] xattr-query of used file descriptors on ${MY_AREA} failed"
               if [ ${SWNRC} -ne ${SWN_CRITICAL} -a \
                    ${SWNRC} -ne ${SWN_ERROR} ]; then
                  SWNRC=${SWN_ERROR}
                  SUMMRY="used file descriptors xattr-query failed"
               fi
            else
               echo "[N] xattr-query of used file descriptors on ${MY_AREA} failed"
            fi
         else
            #
            if [ ${SWN_VERBOSE} -ge 2 -a "${MY_TMP}" != "0" ]; then
               echo "   ${MY_AREA} has ${MY_TMP} file descriptors issued to clients"
            elif [ ${SWN_VERBOSE} -ge 3 ]; then
               echo "   ${MY_AREA} has ${MY_TMP} file descriptors issued to clients"
            fi
            #
            MY_USEDFD=`echo "${MY_USEDFD} + ${MY_TMP}" | /usr/bin/bc`
         fi
      done
      #
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo -e "${MY_USEDFD} file descriptors in use by monted CVMFS areas"
         echo "${MY_MAXFD} file descriptors available to CVMFS clients"
      fi
      #
      if [ -n "${MY_MAXFD}" ]; then
         MY_TMP=`echo "( 100 * ${MY_USEDFD} / ${MY_MAXFD} )/1" | /usr/bin/bc`
      elif [ -n "${MY_NFILES}" ]; then
         MY_TMP=`echo "( 100 * ${MY_USEDFD} / ( ${MY_NFILES} + 512 ) )/1" | /usr/bin/bc`
      else
         MY_TMP=0
      fi
      if [ ${MY_TMP} -ge 95 ]; then
         echo "[E] critical file descriptor usage, ${MY_USEDFD} of ${MY_NFILES}"
         if [ ${SWNRC} -ne ${SWN_CRITICAL} -a ${SWNRC} -ne ${SWN_ERROR} ]; then
            SWNRC=${SWN_ERROR}
            SUMMRY="critical file descriptor usage"
         fi
      elif [ ${MY_TMP} -ge 66 ]; then
         echo "[W] high file descriptor usage, ${MY_USEDFD} of ${MY_NFILES}"
         if [ "${SWNRC}" -eq ${SWN_OK} ]; then
            SWNRC=${SWN_WARNING}
            SUMMRY="high file descriptor usage"
         fi
      fi
   fi
fi
# ########################################################################### #



# check file descriptor usage:
if [ "${SWN_MODE}" != "psst" ]; then
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "\n\nPrinting recent cms.cern.ch log entries (@${MY_TMP} sec):"
      echo "-------------------------------------------------"
   else
      echo ""
   fi
   if [ -x /usr/bin/attr ]; then
      MY_TMP=`echo "${MY_VRSN}" | /usr/bin/awk -F. 'BEGIN{e="nolog"}{if((int($1)>2)||(($1=="2")&&(int($2)>=9))){e="logbuffer"};exit}END{print e}'`
      if [ "${MY_TMP}" == "logbuffer" ]; then
         for MY_AREA in ${MY_CVMFSES}; do
            MY_PATH=${CVMFS_MOUNT_DIR}/${MY_AREA}
            #
            echo -e "\n${MY_AREA}:"
            (cd ${MY_PATH}; /usr/bin/attr -q -g logbuffer ${MY_PATH} | /usr/bin/tail -64)
            if [ $? -ne 0 ]; then
               if [ "${MY_HIDE_MAGIC_XATTRS}" != "yes" ]; then
                  echo "[W] xattr-query of log-buffer on ${MY_AREA} failed"
                  if [ ${SWNRC} -eq ${SWN_OK} ]; then
                     SWNRC=${SWN_WARNING}
                     SUMMRY="log-buffer xattr-query failed"
                  fi
               else
                  echo "[N] xattr-query of log-buffer on ${MY_AREA} failed"
               fi
            fi
         done
      else
         echo "[N] xattr-query of log-buffer not available in ${MY_VRSN}"
      fi
   else
      echo "[N] no /usr/bin/attr command, cannot print log-buffer"
   fi
fi
# ########################################################################### #



if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_END=`/bin/date -u +'%s' 2>/dev/null`
   TMP1=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   TMP2=`echo "${MY_END} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nEnding CMS CVMFS check at ${TMP1} UTC (${TMP2} sec)"
   MY_TMP=`swn2name ${SWNRC}`
   echo "   ${MY_TMP}: ${SUMMRY}"
else
   MY_TMP=`swn2name ${SWNRC}`
   echo "${MY_TMP^^}: ${SUMMRY}"
fi
swn_exit ${SWNRC} "${SUMMRY}"
