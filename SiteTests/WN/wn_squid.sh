#!/bin/sh
# simple wrapper to enforce timeout and put CE-cms-squid into non-legacy mode

TMP1=`(/bin/hostname -f || /bin/hostname) 2>/dev/null`
TMP2=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
MY_START=`/bin/date -u +'%s' 2>/dev/null`

if [ -f ./CE-cms-squid ]; then
   # get probe version:
   SWNSQ_VERSION=`/usr/bin/awk -F= '{gsub(" ","",$1);if($1=="SCRIPT_REVISION"){split($2,a,"\"");print a[2];exit}}' ./CE-cms-squid 2>/dev/null`
   #
   # run probe:
   SAME_OK=${SAME_OK:-10};export SAME_OK
   SAME_WARNING=${SAME_WARNING:-40};export SAME_WARNING
   SAME_ERROR=${SAME_ERROR:-50}; export SAME_ERROR
   SAME_CRITICAL=${SAME_CRITICAL:-60}; export SAME_CRITICAL
   #
   /usr/bin/timeout 180 ./CE-cms-squid 1>swnsq.$$.log 2>&1
   MY_RC=$?
   if [ ${MY_RC} -eq 124 ]; then
      MY_EXIT=2
      MY_NAME="Critical"
      MY_SUMRY="CE-cms-squid script timeout"
   elif [ ${MY_RC} -eq ${SAME_OK} ]; then
      MY_EXIT=0
      MY_NAME="Ok"
      MY_SUMRY="Squid check ok"
   elif [ ${MY_RC} -eq ${SAME_WARNING} ]; then
      MY_EXIT=1
      MY_NAME="Warning"
      MY_LINE=`/usr/bin/awk -F: 'END{sub("[^:]*:","");gsub("^[ \t]+","");gsub("[ \t]+$","");print $0}' swnsq.$$.log 2>/dev/null`
   elif [ ${MY_RC} -eq ${SAME_ERROR} -o \
          ${MY_RC} -eq ${SAME_CRITICAL} ]; then
      MY_EXIT=2
      MY_NAME="Critical"
      MY_SUMRY=`/usr/bin/awk -F: 'END{sub("[^:]*:","");gsub("^[ \t]+","");gsub("[ \t]+$","");print $0}' swnsq.$$.log 2>/dev/null`
   else
      MY_EXIT=3
      MY_NAME="Unknown"
      MY_SUMRY=`/usr/bin/awk -F: 'END{sub("[^:]*:","");gsub("^[ \t]+","");gsub("[ \t]+$","");print $0}' swnsq.$$.log 2>/dev/null`
   fi
   #
   echo -e "${MY_NAME^^}: ${MY_SUMRY}\n"
   #
   echo -e "\nStarting CMS Squid check of ${TMP1} at ${TMP2} UTC"
   echo "   WN-21squid version ${SWNSQ_VERSION}"
   echo "   machine hardware is `/bin/uname -m`"
   if [ -f /etc/os-release ]; then
      MY_TMP=`/usr/bin/awk -F= '{if(($1=="NAME")||($1=="VERSION")){gsub("\"","",$2);printf " %s",$2}}' /etc/os-release`
      echo "   operating system is${MY_TMP}"
   elif [ -f /etc/redhat-release ]; then
      echo "   operating system is `/bin/cat /etc/redhat-release`"
   else
      echo "   no /etc/os-release or /etc/redhat-release file"
   fi
   echo -e "\nCE-cms-squid:\n-------------"
   #
   /bin/cat swnsq.$$.log
   /bin/rm -f swnsq.$$.log
else
   echo -e "UNKNOWN: missing script file\n"
   #
   echo -e "\nStarting CMS Squid check of ${TMP1} at ${TMP2} UTC\n\n"
   echo "[E] no ./CE-cms-squid file"
   MY_EXIT=3
fi

MY_END=`/bin/date -u +'%s' 2>/dev/null`
TMP1=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
TMP2=`echo "${MY_END} - ${MY_START}" | /usr/bin/bc`
echo -e "\n\nEnding CMS Squid check at ${TMP1} UTC (${TMP2} sec)"

exit ${MY_EXIT}
