#!/usr/bin/python3
# ########################################################################### #
#
# SAM WorkerNode data-access probe of CMS
#
# ########################################################################### #



import os, sys
import io
import platform
import time, calendar
import socket
import signal
import re
import pwd
import subprocess
import base64
import json
import xml.etree.ElementTree
import random
import shutil
import traceback
import argparse
# ########################################################################### #



# global variables:
SWNDA_VERSION="v1.01.08"
#
SWNDA_FILES = [ "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "A64CCCF2-5C76-E711-B359-0CC47A78A3F8.root",
                "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "AE237916-5D76-E711-A48C-FA163EEEBFED.root",
                "/store/mc/SAM/GenericTTbar/AODSIM/" + \
                    "CMSSW_9_2_6_91X_mcRun1_realistic_v2-v1/00000/" + \
                    "CE860B10-5D76-E711-BCA8-FA163EAA761A.root" ]
#
SWNDA_CMSSW_LIST = {
    ('x86_64', 'rhel6'): [("CMSSW_10_5_0", "slc6_amd64_gcc700", "xml")],
    ('x86_64', 'rhel7'): [("CMSSW_12_4_9", "slc7_amd64_gcc10",  "xml")], 
    ('x86_64', 'rhel8'): [("CMSSW_14_0_9", "el8_amd64_gcc12",   "json")] }
#
SWN_MODE = "interactive"
SWN_VERBOSITY = 0
SWN_STDOUT = None
SWN_STDERR = None
SWN_STRINGIO = None
#
SWNDA_CENTRAL_ISSUE = False
#
SWNDA_OSID = (None, None)
#
SWNDA_SC_SITE = None
SWNDA_SC_SUBSITE = None
SWNDA_SC_XML = None
SWNDA_SC_JSON = None
# ########################################################################### #



# CMS SAM Worker Node definitions:
class swn:
    unknown = -1
    ok = 0
    warning = 1
    error = 2
    critical = 3

    @staticmethod
    def code2name(code):
        swnNames = ["ok", "warning", "error", "critical"]
        if (( code >= 0 ) and ( code < len(swnNames) )):
            return swnNames[code]
        else:
            return "unknown"

    @staticmethod
    def code2sam(code):
        samRC = [ 0, 1, 2, 2 ]
        if (( code >= 0 ) and ( code < len(samRC) )):
            return samRC[code]
        else:
            return 3

    @staticmethod
    def sam2name(code):
        samNames = ["OK", "WARNING", "CRITICAL"]
        if (( code >= 0 ) and ( code < len(samNames) )):
            return samNames[code]
        else:
            return "UNKNOWN"

    @staticmethod
    def code2psst(code):
        psstRC = [ 0, 0, 0, 1 ]
        if (( code >= 0 ) and ( code < len(psstRC) )):
            return psstRC[code]
        else:
            return 0

    @staticmethod
    def code2order(code):
        codeSeverity = [ swn.ok, swn.warning, swn.unknown, \
                                                      swn.error, swn.critical ]
        try:
            return codeSeverity.index(code)
        except ValueError:
            return codeSeverity.index(swn.unknown)


    @staticmethod
    def log(level, indent, message):
        levelNames = { 'C': "Critical", 'E': "Error", 'W': "Warning",
                       'N': "Notice",   'I': "Info",  'D': "Debug",
                       'X': "X-Debug",  '?': "Unknown" }
        printControl = [ ("CEW", "plain"),
                         ("CEWN", "indent"), ("CEWNI", "indent"),
                         ("CEWNID", "time"), ("CEWNIDX", "time") ]
        verbosityLevel = SWN_VERBOSITY
        #
        if (( message is None ) or ( len(message) == 0 )):
            return
        #
        msg = None
        try:
            level = levelNames[level][0]
        except:
            level = '?'
        try:
            indent = min( 8, max(0, int(indent)))
        except:
            indent = 0
        newline = 0
        while( message[0] == "\n" ):
            message = message[1:]
            newline += 1
        trailing = 0
        while( message[-1] == "\n" ):
            message = message[:-1]
            trailing += 1
        #
        if ( verbosityLevel >= 0 ):
            if ( verbosityLevel < len(printControl) ):
                if ( level not in printControl[verbosityLevel][0] ):
                    return
            else:
                verbosityLevel = len(printControl) - 1
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    if ( printControl[verbosityLevel][1] == "time" ):
                        msg += time.strftime("%H:%M:%S",
                                             time.gmtime(time.time())) \
                               + ( "   " * indent ) \
                               + ( " [%s] " % level ) + line
                    elif ( printControl[verbosityLevel][1] == "indent" ):
                        if ( level in "CEW" ):
                            msg += ( "   " * indent ) \
                                   + ( "[%s] " % level ) + line
                        else:
                            msg += ( "   " * indent ) + line
                    else:
                        msg += ( "[%s] " % level ) + line
                else:
                    if ( printControl[verbosityLevel][1] == "time" ):
                        msg += "\n" + "        " \
                               + ( "   " * indent ) \
                               + ( " [%s] " % level.lower() ) + line
                    elif ( printControl[verbosityLevel][1] == "indent" ):
                        if ( level in "CEW" ):
                            msg += "\n" + ( "   " * indent ) \
                                   + ( "[%s] " % level.lower() ) + line
                        else:
                            msg += "\n" + ( "   " * indent ) + line
                    else:
                        msg += "\n" + ( "[%s] " % level.lower() ) + line
                firstLine = False
        elif ( verbosityLevel < 0 ):
            if ( level not in "CE" ):
               return
            msg = "\n" * newline
            firstLine = True
            for line in message.split("\n"):
                if ( firstLine ):
                    msg += time.strftime("%Y-%b-%d %H:%M:%S",
                                         time.gmtime(time.time())) \
                           + ( " %s: " % levelNames[level] ) + line
                else:
                    msg += "\n                       " \
                           + ( " " * len(levelNames[level]) ) + line
                firstLine = False
        msg += "\n" * trailing
        #
        print( msg )


    @staticmethod
    def exit(code, summary):
        exitCode = None
        #
        if (( SWNDA_CENTRAL_ISSUE == True ) and
            ( code != swn.ok ) and ( code != swn.warning )):
            print("Central System Issue detected, status override triggered")
            code = swn.warning
        #
        if ( SWN_MODE == "sam" ):
            print("\n%s: %s\n</PRE>" %
                                   (swn.code2name(code).capitalize(), summary))
            #
            sys.stderr = SWN_STDERR
            sys.stdout = SWN_STDOUT
            #
            exitCode = swn.code2sam(code)
            #
            print("%s: %s\n" % (swn.sam2name(exitCode), summary))
            print( SWN_STRINGIO.getvalue() )
            SWN_STRINGIO.close()
        elif ( SWN_MODE == "psst" ):
            exitCode = swn.code2psst(code)
        else:
            exitCode = code
            #
            print("\n%s: %s" % (swn.code2name(code).upper(), summary))
        #
        sys.exit(exitCode)
# ########################################################################### #



class SamWorkerNodeDataAccessAbortException(Exception):
    """Exception to break out of sequential program execution"""
    pass
# ########################################################################### #



def swnda_signal_children(mySignal=signal.SIGKILL):
    """send a signal to all subprocesses, ignoring setuid subprocesses"""
    #
    lineRegex = re.compile(r"^\s*(\d+)\s+(\d+)\s*$")

    try:
        myEUID = str( os.geteuid() )
        myPID = os. getpid()
        #
        myCMD = ["/bin/ps", "-u", myEUID, "-o", "ppid=", "-o", "pid="]
        myPopen = subprocess.Popen(myCMD, stdout=subprocess.PIPE, \
                                          stderr=subprocess.DEVNULL)
        myOut,myErr = myPopen.communicate(timeout=3)
        if ( myPopen.returncode != 0 ):
            swn.log("E", 1, ("Failed to run /bin/ps to get subprocesses," + \
                                                " rc=%d") % myPopen.returncode)
            return swn.error
        #
        myProcs = []
        myChildren = []
        for myLine in myOut.decode("utf-8").splitlines():
            matchObj = lineRegex.match( myLine )
            if ( matchObj is not None ):
                myProcs.append( (int(matchObj.group(1)),
                                 int(matchObj.group(2))) )
                if ( int(matchObj.group(1)) == myPID ):
                    myChildren.append( int(matchObj.group(2)) )
        if ( len(myChildren) == 0 ):
            return swn.ok
        previousLen = 0
        while ( len(myChildren) > previousLen ):
            previousLen = len(myChildren)
            for myTuple in myProcs:
                if ( myTuple[0] in myChildren ):
                    myChildren.append( myTuple[1] )
        #
        for myChild in myChildren:
            try:
                os.kill( myChild, mySignal )
            except:
                pass

    except subprocess.TimeoutExpired:
        swn.log("E", 1, "execution of /bin/ps to get subprocesses timed out")
        return swn.error
    except Exception as excptn:
        swn.log("E", 1, "signaling subprocesses failed: %s" % str(excptn))
        return swn.error

    return swn.ok
# ########################################################################### #



def swnda_timeout_handler(signum, frame):
    """SIGALRM then SIGKILL subprocesses and raise TimeoutError"""

    swnda_signal_children(signal.SIGALRM)
    time.sleep(1)
    swnda_signal_children(signal.SIGKILL)
    #
    raise TimeoutError("Signal %d caught" % signum)

    return
# ########################################################################### #



def swnda_environment():
    """print environmental variable information relevant for data-access"""


    # print environmental variables relevant for data-access:
    # =======================================================
    for myVar in [ "PATH", "LD_PRELOAD", "LD_LIBRARY_PATH", \
                   "PYTHONPATH", "PYTHON3PATH", "PERL5LIB", \
                   "CMS_PATH", "SITECONFIG_PATH" ]:
        try:
            swn.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
        except KeyError:
            pass
    #
    varList = []
    for myVar in os.environ:
        if (( "GFAL" in myVar ) or
            ( "XRD_" in myVar ) or ( "Xrd" in myVar )):
            varList.append(myVar)
    for myVar in varList:
        if ( "GFAL" in myVar ):
            swn.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
    for myVar in varList:
        if (( "XRD_" in myVar ) or ( "Xrd" in myVar )):
            swn.log("I", 1, "%s: %s" % (myVar, os.environ[myVar]))
    del varList


    return swn.ok
# ########################################################################### #



def swnda_osidentifier():
    """identify OS and return standardized OS label"""
    global SWNDA_OSID
    #
    floatRegex = re.compile(r"^([1-9]+[0-9]*)[.]*[0-9]*$")


    # get machine architecture:
    # =========================
    osArch = "unknown"
    try:
        osArch = platform.machine()
    except Exception as excptn:
        swn.log("E", 1, "determining machine architecture failed: %s" %
                                                                   str(excptn))


    # try /etc/os-release
    # ===================
    osSlbl = osDesc = None
    try:
        osName = osVrsn = osFmly = None
        #
        with open("/etc/os-release", 'r') as myFile:
            myLines = myFile.readlines()
        swn.log("I", 1, "found /etc/os-release")
        #
        for myLine in myLines:
            if ( "=" not in myLine ):
                continue
            osVar = myLine.strip().split("=")[0]
            osVal = myLine.strip().split("=")[1]
            if (( osVal[0] == osVal[-1] ) and ( osVal[0] in "\"\'" )):
                osVal = osVal[1:-1]
            #
            if ( osVar == "NAME" ):
                osName = osVal
            elif ( osVar == "VERSION_ID" ):
                matchObj = floatRegex.match( osVal )
                if ( matchObj is not None ):
                    osVrsn = matchObj.group(1)
            elif ( osVar == "ID_LIKE" ):
                if ( "rhel" in osVal.split() ):
                    osFmly = "rhel"
                else:
                    osFmly = osVal.split()[0]
            elif ( osVar == "PRETTY_NAME" ):
                osDesc = osVal
        # analyse:
        if (( osFmly is not None ) and ( osVrsn is not None )):
            osSlbl = osFmly + osVrsn.split(".")[0]
        elif (( osName is not None ) and ( osVrsn is not None )):
            if (( osName[:18] == "Red Hat Enterprise" ) or
                ( osName[:6] == "CentOS" ) or
                ( osName[:4] == "Alma" ) or
                ( osName[:5] == "Rocky" )):
                osSlbl = "rhel" + osVrsn.split(".")[0]
        if ( osDesc is None ):
            if (( osName is not None ) and ( osVrsn is not None )):
                osDesc = osName + " " + osVrsn
            elif ( osName is not None ):
                osDesc = osName + " ?"
            elif ( osVrsn is not None ):
                osDesc = "Unknown OS " + osVrsn
            else:
                osDesc = "Unknown OS ?"
        #
        if ( osSlbl is not None ):
            SWNDA_OSID = (osArch, osSlbl)
            swn.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
            return swn.ok
    except Exception as excptn:
        swn.log("N", 1, "reading/interpreting /etc/os-release failed: %s" %
                                                                   str(excptn))


    # check for RedHat Enterprise Linux release files:
    # ================================================
    osSlbl = osDesc = None
    for releaseFile in ["/etc/redhat-release", "/etc/centos-release", \
                        "/etc/almalinux-release", "/etc/rocky-release" ]:
        try:
            with open(releaseFile, 'r') as myFile:
                myLine = myFile.readline()
            swn.log("I", 1, "found %s" % releaseFile)
            #
            osDesc = myLine.strip()
            for osVal in osDesc.split():
                matchObj = floatRegex.match( osVal )
                if ( matchObj is not None ):
                    osSlbl = "rhel" + matchObj.group(1)
            #
            if ( osSlbl is not None ):
                SWNDA_OSID = (osArch, osSlbl)
                swn.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
                return swn.ok
        except Exception as excptn:
            swn.log("N", 1, "reading/interpreting %s failed: %s" %
                                                     (releaseFile,str(excptn)))


    # try /etc/SuSE-release
    # =====================
    osSlbl = osDesc = None
    try:
        osName = osVrsn = osFmly = None
        #
        with open("/etc/SuSE-release", 'r') as myFile:
            myLine = myFile.readline()
        swn.log("I", 1, "found /etc/SuSE-release")
        #
        osDesc = myLine.strip()
        floatRegex = re.compile(r"^([1-9]+[0-9]*)[.]*[0-9]*$")
        for osVal in osDesc.split():
            matchObj = floatRegex.match( osVal )
            if ( matchObj is not None ):
                osSlbl = "suse" + matchObj.group(1)
        #
        if ( osSlbl is not None ):
            SWNDA_OSID = (osArch, osSlbl)
            swn.log("N", 1, "OS identified as %s/%s, %s" %
                                                      (osArch, osSlbl, osDesc))
            return swn.ok
    except Exception as excptn:
        swn.log("N", 1, "reading/interpreting /etc/SuSE-release failed: %s" %
                                                                   str(excptn))
        pass


    SWNDA_OSID = (osArch, "unknown")
    swn.log("E", 1, "OS not identified, %s/unknown" % osArch)
    return swn.critical
# ########################################################################### #



def swnda_x509setup():
    """check/setup x509 authentication environment"""
    global SWNDA_CENTRAL_ISSUE
    #
    returnCode = swn.ok


    # locate x509 certificate:
    # ========================
    if ( "X509_USER_PROXY" in os.environ ):
        swn.log("I", 1, "found X509_USER_PROXY set")
    else:
        x509File = "/tmp/x509up_u" + str(os.geteuid())
        if ( os.path.isfile(x509File) and os.access(x509File, os.R_OK) ):
            os.environ['X509_USER_PROXY'] = x509File
            x509Name = pwd.getpwuid(os.getuid()).pw_name
            swn.log("N", 1, "using effective uid x509 file %s of %s" %
                                                           (x509File,x509Name))
        else:
            swn.log("C", 1, "No x509 credentials accessible")
            return swn.critical


    # check x509 certificate is valid:
    # ================================
    now = int( time.time() )
    try:
        import OpenSSL
        #
        x509File = os.environ['X509_USER_PROXY']
        with open(x509File, "rb") as fd:
            cert = fd.read()
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM,
                                                                          cert)
        sbjct = "".join( [ "/%s=%s" % (c[0].decode(), c[1].decode()) \
                               for c in x509.get_subject().get_components() ] )
        swn.log("N", 1, "CMS certificate of %s" % sbjct)
        tTpl = time.strptime(x509.get_notAfter().decode('ascii'),
                                                               '%Y%m%d%H%M%SZ')
        swn.log("I", 2, "valid until %s" % \
                                      time.strftime("%Y-%m-%d %H:%M:%S", tTpl))
        if ( int( calendar.timegm(tTpl) ) < ( now + 300 ) ):
            swn.log("E", 2, "Expired CMS certificate, %s" %
                                      time.strftime("%Y-%m-%d %H:%M:%S", tTpl))
            SWNDA_CENTRAL_ISSUE = True
            returnCode = swn.error
        vomsFlag = False
        for i in range( x509.get_extension_count() ):
            if ( x509.get_extension(i).get_short_name() == b"UNDEF" ):
                extBytes = x509.get_extension(i).get_data()
                pAuth = extBytes.find(b"cms://")
                while ( pAuth >= 0 ):
                    try:
                        indx = extBytes.find(b":", pAuth + 6)
                        vomsSrv = extBytes[pAuth + 6 : indx]
                        if (( vomsSrv == b"voms2.cern.ch" ) or
                            ( vomsSrv == b"lcg-voms2.cern.ch" ) or
                            ( vomsSrv == b"voms-cms-auth.app.cern.ch" ) or
                            ( vomsSrv == b"voms-cms-auth.cern.ch" )):
                            swn.log("I", 2, "CMS VOMS extension issued by %s"
                                                     % vomsSrv.decode("utf-8"))
                            vomsFlag = True
                            sLen = extBytes[pAuth-1]
                            #
                            aLen = extBytes[pAuth+sLen+2]
                            atrBytes = extBytes[pAuth+sLen+3:\
                                                             pAuth+sLen+3+aLen]
                            indx = 0
                            while ( indx < len(atrBytes) ):
                                sLen = atrBytes[indx + 1]
                                swn.log("I", 3, "%s" % atrBytes[indx+2: \
                                                  indx+2+sLen].decode("utf-8"))
                                indx += 2 + sLen
                    except:
                        pass
                    pAuth = extBytes.find(b"cms://", pAuth + 6)
        if ( vomsFlag == False ):
            swn.log("E", 1, "seems to have no CMS VOMS extension, expect" + \
                                                       " access test failures")
            SWNDA_CENTRAL_ISSUE = True
            returnCode = swn.error
        del vomsFlag, tTpl, sbjct, x509, cert
    except ImportError:
        swn.log("N", 1, "Python3 OpenSSL module not available")
        #
        try:
            myCmd = [ "/usr/bin/voms-proxy-info", \
                      "-file", os.environ['X509_USER_PROXY'], "-all" ]
            myPopen = subprocess.Popen(myCmd, stdout=subprocess.PIPE, \
                                              stderr=subprocess.STDOUT)
            myOut,myErr = myPopen.communicate(timeout=5)
            if ( myPopen.returncode != 0 ):
                swn.log("E", 1, ("Failed to run /usr/bin/voms-proxy-info t" + \
                                 "o check certificate, rc=%d\n%s") %
                                    (myPopen.returncode,myOut.decode("utf-8")))
                returnCode = swn.error
            else:
                vomsFlag = None
                for myLine in myOut.decode("utf-8").splitlines():
                    if ( myLine.find(":") == -1 ):
                        if ( myLine.find("extension information") >= 0 ):
                            if ( vomsFlag != True ):
                                vomsFlag = False
                    else:
                        myKey,myValue = myLine.split(":",1)
                        if ( vomsFlag is None ):
                            if ( myKey.strip() == "subject" ):
                                swn.log("N", 1, "CMS certificate of %s" % \
                                                               myValue.strip())
                            elif ( myKey.strip() == "timeleft" ):
                                swn.log("I", 2, "remaining validity %s" % \
                                                               myValue.strip())
                                myTime = myValue.split(":")
                                myTIM = 60 * int( myTime[0].strip() ) + \
                                             int( myTime[1].strip() )
                                if ( myTIM < 5 ):
                                    swn.log("E", 2, ("Expired CMS certif" + \
                                                "icate, %s") % myValue.strip())
                                    SWNDA_CENTRAL_ISSUE = True
                                    returnCode = swn.error
                        elif ( vomsFlag == False ):
                            if ( myKey.strip() == "issuer" ):
                                for myField in myValue.split("/"):
                                    if ( myField[:3] == "CN=" ):
                                        myVOMS = myField[3:].strip()
                                        if (( myVOMS == "voms2.cern.ch" ) or
                                            ( myVOMS == "lcg-voms2.cern.ch" ) or
                                            ( myVOMS == "cms-auth.web.cern.ch" ) or
                                            ( myVOMS == "cms-auth.cern.ch" )):
                                            swn.log("I", 2, "CMS VOMS ex" + \
                                               "tension issued by %s" % myVOMS)
                                            vomsFlag = True
                            elif ( myKey.strip() == "attribute" ):
                                swn.log("I", 3, "%s" % myValue.strip())
                if ( vomsFlag != True ):
                    swn.log("W", 1, "seems to have no CMS VOMS extension" + \
                                               ", expect access test failures")
                    SWNDA_CENTRAL_ISSUE = True
                    returnCode = swn.warning
        except Exception as excptn:
            swn.log("E", 1, ("CMS x509 certificate voms-proxy-info decod" + \
                                               "ing failed: %s") % str(excptn))
            returnCode = swn.error
    except Exception as excptn:
        swn.log("E", 1, "CMS x509 certificate reading/decoding failed: %s" %
                                                                   str(excptn))
        returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnda_iamtoken():
    """check/setup x509 IAM-issued token environment"""
    global SWNDA_CENTRAL_ISSUE
    #
    returnCode = swn.ok


    # locate token:
    # =============
    if ( "BEARER_TOKEN" in os.environ ):
        swn.log("I", 1, "found BEARER_TOKEN set")
    elif (( "BEARER_TOKEN_FILE" in os.environ ) and
          ( os.path.isfile(os.environ['BEARER_TOKEN_FILE']) ) and
          ( os.access(os.environ['BEARER_TOKEN_FILE'], os.R_OK) )):
        swn.log("I", 1, "found BEARER_TOKEN_FILE set")
        with open(os.environ['BEARER_TOKEN_FILE'], "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    elif (( "XDG_RUNTIME_DIR" in os.environ ) and
          ( os.path.isfile(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + \
                                                       str(os.geteuid())) ) and
          ( os.access(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + \
                                                str(os.geteuid()), os.R_OK) )):
        swn.log("I", 1, "found XDG_RUNTIME_DIR set")
        with open(os.environ['XDG_RUNTIME_DIR'] + "/bt_u" + str(os.geteuid()),
                                                                    "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    elif (( os.path.isfile("/tmp/bt_u" + str(os.geteuid())) ) and
          ( os.access("/tmp/bt_u" + str(os.geteuid()), os.R_OK) )):
        swn.log("I", 1, "found /tmp/bt_u%s file" % str(os.geteuid()))
        with open("/tmp/bt_u" + str(os.geteuid()), "r") as fd:
            tokn = fd.read().strip()
        os.environ['BEARER_TOKEN'] = tokn
    else:
        swn.log("W", 1, "No IAM-issued token accessible")
        return swn.ok


    # clear lower priority token discovery settings (to avoid confusion):
    # ===================================================================
    try:
        del os.environ['BEARER_TOKEN_FILE']
    except KeyError:
        pass
    try:
        del os.environ['XDG_RUNTIME_DIR']
    except KeyError:
        pass


    # check token is valid:
    # =====================
    try:
        tokn = os.environ['BEARER_TOKEN']
        tknBytes = base64.urlsafe_b64decode( tokn.strip().split(".")[1] + "==" )
        tknJson = json.loads( tknBytes.decode("utf-8") )
        if (( tknJson['iss'] == "https://cms-auth.web.cern.ch/" ) or
            ( tknJson['iss'] == "https://cms-auth.cern.ch/" )):
            swn.log("N", 1, "CMS IAM-issued token, %s...%s" %
                                                         (tokn[:8], tokn[-8:]))
            swn.log("I", 2, "JWT id  : %s" % tknJson['jti'])
            swn.log("I", 2, "Subject : %s" % tknJson['sub'])
            try:
                swn.log("I", 2, "ClientID: %s" % tknJson['client_id'])
            except:
                pass
            swn.log("I", 2, "audience: %s" % tknJson['aud'])
            swn.log("I", 2, "scope   : %s" % tknJson['scope'].split(" ")[0])
            for scpe in tknJson['scope'].split(" ")[1:]:
                swn.log("I", 2, "          %s" % scpe)
            swn.log("I", 2, "valid   : %s to %s" %
                              (time.strftime("%Y-%b-%d %H:%M:%S",
                                                  time.gmtime(tknJson['nbf'])),
                               time.strftime("%Y-%b-%d %H:%M:%S",
                                                 time.gmtime(tknJson['exp']))))
            if ( tknJson['nbf'] > now ):
                swn.log("E", 2, "Unbegun CMS read-write token!")
                SWNDA_CENTRAL_ISSUE = True
                returnCode = swn.error
            elif ( tknJson['exp'] < ( now + 300 ) ):
                swn.log("E", 2, "Expired CMS read-write token")
                SWNDA_CENTRAL_ISSUE = True
                returnCode = swn.error
            try:
                swn.log("I", 2, "issued  : %s" %
                                             time.strftime("%Y-%b-%d %H:%M:%S",
                                                  time.gmtime(tknJson['iat'])))
            except:
                pass
            for key in tknJson:
                if key in ["iss", "jti", "sub", "client_id", "aud",
                           "scope", "nbf", "exp", "iat"]:
                    continue
                swn.log("I", 2, "%-8s: %s" % (str(key), str(tknJson[key])))
        else:
            swn.log("E", 1, "Not a CMS token, %s...%s" %
                                                         (tokn[:8], tokn[-8:]))
            try:
                swn.log("E", 2, "Issuer  : %s" % tknJson['iss'])
            except:
                pass
            SWNDA_CENTRAL_ISSUE = True
            returnCode = swn.error
        del tknJson, tknBytes, tokn
    except Exception as excptn:
        swn.log("E", 1, "CMS IAM-issued token reading/decoding failed: %s" %
                                                                   str(excptn))
        returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnda_source(shScript):
    """set/update environment variables set/changed by a shell script"""

    swn.log("N", 1, "Sourcing %s:" % shScript)


    # check file exists:
    # ==================
    if ( os.path.isfile(shScript) != True ):
        swn.log("E", 1, "script %s does not exist" % shScript)
        return swn.error


    # source script and print environment variables:
    # ==============================================
    try:
        myCmd = "/bin/sh -c \". %s && printenv -0\"" % shScript
        for myLine in subprocess.getoutput(myCmd).split("\0"):
            myVar, myVal = ( myLine.split("=",1) + [""] )[:2]
            #
            # set environment variables in current/python process:
            # ====================================================
            swn.log("D", 1, "setting %s to \"%s\"" % (myVar, myVal))
            os.environ[myVar] = myVal
    except Exception as excptn:
        swn.log("E", 1, ("sourcing script and updating environment faile" + \
                                                        "d, %s") % str(excptn))
        return swn.error


    return swn.ok
# ########################################################################### #



def swnda_cmssetup():
    """setup CMS environment, a la GitHub:dmwm/WMCore/etc/submit_py3.sh"""
    #
    returnCode = swn.ok


    try:
        # start with old LCG location check:
        # ==================================
        setupFile = os.environ.get("VO_CMS_SW_DIR", "") + "/cmsset_default.sh"
        if ( os.path.isfile( setupFile ) ):
            swn.log("I", 1, "found CMS setup file at LCG location")
        else:
            setupFile = os.environ.get("OSG_APP", "") + \
                                               "/cmssoft/cms/cmsset_default.sh"
            if ( os.path.isfile( setupFile ) ):
                swn.log("I", 1, "found CMS setup file at OSG location")
            else:
                if ( "CVMFS_MOUNT_DIR" in os.environ ):
                   setupFile = os.environ['CVMFS_MOUNT_DIR'] + \
                                               "/cms.cern.ch/cmsset_default.sh"
                elif ( "CVMFS" in os.environ ):
                   setupFile = os.environ["CVMFS"] + \
                                               "/cms.cern.ch/cmsset_default.sh"
                else:
                   setupFile = "/cvmfs/cms.cern.ch/cmsset_default.sh"
                if ( os.path.isfile( setupFile ) ):
                    swn.log("I", 1, "found CMS setup file at CVMFS location")
                else:
                    swn.log("C", 1, "no CMS setup file found")
                    return swn.critical


        # source setup file:
        # ==================
        returnCode = swnda_source( setupFile )
        #
        try:
            swn.log("I", 1, "SCRAM_ARCH: %s" % os.environ['SCRAM_ARCH'])
        except KeyError:
            pass


    except Exception as excptn:
        swn.log("E", 1, "locating and sourcing CMS setup file failed: %s" %
                                                                   str(excptn))
        returnCode = swn.error


    return returnCode
# ########################################################################### #



def swnda_siteconf():
    """examine SITECONF area and compile XML and JSON-based data-access list"""
    global SWNDA_SC_SITE
    global SWNDA_SC_SUBSITE
    global SWNDA_SC_XML
    global SWNDA_SC_JSON
    #
    tfcRegex = re.compile(
                       r".*trivialcatalog_file:([^:?]+)\?.*protocol=([^&]+).*")
    #
    returnCode = swn.ok


    # we are after a cmsset_default.sh, SITECONFIG_PATH should be set:
    # ================================================================
    try:
        # resolve symbolic links:
        scPath = os.path.realpath( os.environ['SITECONFIG_PATH'] )
        if ( scPath[-1] != "/" ):
            scPath += "/"


        # examine JobConfig/site-local-config.xml:
        # ========================================
        slcFile = scPath + "JobConfig/site-local-config.xml"
        if ( os.path.isfile( slcFile ) == False ):
            swn.log("C", 1, "No JobConfig/site-local-config.xml file")
            return swn.critical
        #
        with open( slcFile, 'r') as myFile:
            slcData = myFile.read()
        #
        slcXML = xml.etree.ElementTree.fromstring( slcData )
        #
        siteElemnt = slcXML.find("site")
        SWNDA_SC_SITE = siteElemnt.attrib['name']
        try:
            SWNDA_SC_SUBSITE = siteElemnt.find("subsite").attrib['name']
        except:
            SWNDA_SC_SUBSITE = ""
        #
        # PhEDEx/storage.xml based access:
        # --------------------------------
        SWNDA_SC_XML = set()
        try:
            catTags = siteElemnt.find("event-data").findall("catalog")
            if ( len(catTags) > 0 ):
                for myTag in catTags:
                    matchObj = tfcRegex.match( myTag.attrib['url'] )
                    if ( matchObj is None ):
                        swn.log("W", 1, "Bad <catalog> tag in <event-dat" + \
                               "a> section of JobConfig/site-local-config.xml")
                        if ( returnCode == swn.ok ):
                            returnCode = swn.warning
                        continue
                    SWNDA_SC_XML.add( (matchObj.group(1), matchObj.group(2)) )
            else:
                swn.log("E", 1, "No <catalog> tag in <event-data> sectio" + \
                                        "n of JobConfig/site-local-config.xml")
                returnCode = swn.error
        except Exception as excptn:
            swn.log("E", 1, ("Error finding PhEDEx/storage.xml based dat" + \
              "a access in JobConfig/site-local-config.xml, %s") % str(excptn))
            returnCode = swn.error
        #
        # storage.json based access:
        # --------------------------
        SWNDA_SC_JSON = set()
        try:
            catTags = siteElemnt.find("data-access").findall("catalog")
            if ( len(catTags) > 0 ):
                for myTag in catTags:
                    try:
                        mySite = myTag.attrib['site']
                    except KeyError:
                        mySite = SWNDA_SC_SITE
                    try:
                        myVolm = myTag.attrib['volume']
                        myProt = myTag.attrib['protocol']
                    except KeyError:
                        swn.log("W", 1, "Bad <catalog> tag in <data-acce" + \
                              "ss> section of JobConfig/site-local-config.xml")
                        if ( returnCode == swn.ok ):
                            returnCode = swn.warning
                        continue
                    SWNDA_SC_JSON.add( (mySite, myVolm, myProt) )
            else:
                swn.log("E", 1, "No <catalog> tag in <data-access> secti" + \
                                       "on of JobConfig/site-local-config.xml")
                returnCode = swn.error
        except Exception as excptn:
            swn.log("E", 1, ("Error finding storage.json based data acce" + \
                    "ss in JobConfig/site-local-config.xml, %s") % str(excptn))
            returnCode = swn.error


    except Exception as excptn:
        swn.log("E", 1, "SITECONF area examination failed: %s" % str(excptn))
        returnCode = swn.error


    if ((( SWNDA_SC_XML is None ) and ( SWNDA_SC_JSON is None )) or
        (( SWNDA_SC_XML is None ) and ( SWNDA_SC_JSON is not None ) and
                                      ( len(SWNDA_SC_JSON) == 0 )) or
        (( SWNDA_SC_XML is not None ) and
         ( len( SWNDA_SC_XML ) == 0 ) and ( SWNDA_SC_JSON is None ))):
        # require at least either XML or JSON data access:
        returnCode = swn.critical


    return returnCode
# ########################################################################### #



def swnda_cmsrun(cmsswTpl, lfnFile):
    """run a data-access to lfnFile using CMSSW of cmsswTpl"""
    #
    returnCode = swn.ok


    try:
        #
        # create a CMSSW release area:
        myCmd = [ "scram", "-a", cmsswTpl[1], "project", "CMSSW", cmsswTpl[0] ]
        myPopen = subprocess.Popen(myCmd, stdout=subprocess.PIPE, \
                                          stderr=subprocess.STDOUT)
        myOut,myErr = myPopen.communicate(timeout=120)
        if ( myPopen.returncode != 0 ):
            swn.log("E", 1, "Error creating CMSSW release area:\n%s\n%s" %
                                           (str(myCmd), myOut.decode("utf-8")))
            return swn.error


        # change into CMSSW release area:
        os.chdir(cmsswTpl[0])
        #
        # loop over JobConfig/site-local-config.xml data-access list:
        if ( cmsswTpl[2] == "xml" ):
            daList = list( SWNDA_SC_XML )
        elif ( cmsswTpl[2] == "json" ):
            daList = list( SWNDA_SC_JSON )
        else:
            os.chdir("..")
            return swn.unknown
        daSuccess = 0
        for daItem in daList:
            #
            # write cmsRun config file:
            if ( cmsswTpl[2] == "xml" ):
                ocStrng = "trivialcatalog_file:%s?protocol=%s" % \
                                                         (daItem[0], daItem[1])
            elif ( cmsswTpl[2] == "json" ):
                ocStrng = "%s,%s,%s,%s,%s" % (SWNDA_SC_SITE, SWNDA_SC_SUBSITE,
                                               daItem[0], daItem[1], daItem[2])
            #
            swn.log("N", 1, "data-access: %s" % ocStrng)
            #
            #
            with open("swnda_cfg.py", 'w') as myFile:
                myFile.write(("#\nimport FWCore.ParameterSet.Config as cms" + \
                              "\nprocess = cms.Process(\"SAMtest\")\nproce" + \
                              "ss.source = cms.Source('PoolSource',\n   ov" + \
                              "errideCatalog = cms.untracked.string(\"%s\"" + \
                              "),\n   fileNames = cms.untracked.vstring(\"" + \
                              "%s\")\n)\n\nprocess.SiteLocalConfigService " + \
                              "= cms.Service(\"SiteLocalConfigService\",\n" + \
                              "   debugLevel = cms.untracked.uint32(1),\n " + \
                              "  overrideSourceCacheHintDir = cms.untracke" + \
                              "d.string(\"application-only\"),\n)\n\nproce" + \
                              "ss.dump = cms.EDAnalyzer(\"EventContentAnal" + \
                              "yzer\",\n   listContent = cms.untracked.boo" + \
                              "l(False),\n   getData = cms.untracked.bool(" + \
                              "True)\n)\nprocess.load(\"FWCore.MessageServ" + \
                              "ice.MessageLogger_cfi\")\nprocess.MessageLo" + \
                              "gger.cerr.FwkReport.reportEvery = 1\n\nproc" + \
                              "ess.maxEvents = cms.untracked.PSet(\n   inp" + \
                              "ut = cms.untracked.int32(0)\n)\n\nprocess.p" + \
                              " = cms.EndPath(process.dump)") % \
                                                             (ocStrng,lfnFile))
            #
            #
            # setup CMSSW environment and execute cmsRun:
            myCmd = [ "/bin/sh", "-c", \
                              "eval `scram runtime -sh`; cmsRun swnda_cfg.py" ]
            myPopen = subprocess.Popen(myCmd, stdout=subprocess.PIPE, \
                                              stderr=subprocess.STDOUT)
            myOut,myErr = myPopen.communicate(timeout=300)
            if ( myPopen.returncode == 0 ):
                swn.log("N", 2, "--> success")
                daSuccess += 1
            else:
                swn.log("E", 2, ("Error setting up CMSSW and executing cms" + \
                         "Run:\n%s\n%s") % (str(myCmd), myOut.decode("utf-8")))
                swn.log("N", 2, "--> failure")
        #
        # cd out of CMSSW release area:
        os.chdir("..")
        #
        #
        # evaluate data-access results:
        if ( daSuccess == 0 ):
            returnCode = swn.error
        elif ( daSuccess < len( daList ) ):
            returnCode = swn.warning

    except Exception as excptn:
        swn.log("E", 1, "setup of %s and/or cmsRun failed: %s" %
                                                    (cmsswTpl[0], str(excptn)))
        returnCode = swn.critical

    return returnCode
# ########################################################################### #



if __name__ == '__main__':
    #
    try:
        parserObj = argparse.ArgumentParser(description="CMS SAM WorkerNode " +
                                                           "data-access probe")
        parserObj.add_argument("-S", dest="sam", action="store_true",
                                     default=False,
                                     help="ETF configuration, SAM mode")
        parserObj.add_argument("-P", dest="psst", action="store_true",
                                     default=False,
                                     help="fast, minimal-output probing, PSS" +
                                                                      "T mode")
        parserObj.add_argument("-t", dest="timeout", type=int, default=300,
                                     help="maximum time probe is allowed to " +
                                                       "take in seconds [300]")
        parserObj.add_argument("-v", dest="verbose", action="count",
                                     help="increase verbosity")
        argStruct = parserObj.parse_args()
        #
        #
        #
        if ( argStruct.sam == True ):
            SWN_STDOUT = sys.stdout
            SWN_STDERR = sys.stderr
            sys.stderr = sys.stdout = SWN_STRINGIO = io.StringIO()
            #
            print("<PRE>")
            #
            SWN_MODE = "sam"
            SWN_VERBOSITY = 2
        elif ( argStruct.psst == True ):
            SWN_MODE = "psst"
            SWN_VERBOSITY = -1
        elif ( len(os.getenv("SAME_SENSOR_HOME", default="")) > 0 ):
            SWN_STDOUT = sys.stdout
            SWN_STDERR = sys.stderr
            sys.stderr = sys.stdout = SWN_STRINGIO = io.StringIO()
            #
            print("<PRE>")
            #
            SWN_MODE = "sam"
            SWN_VERBOSITY = 2
        if ( argStruct.verbose is not None ):
            SWN_VERBOSITY = argStruct.verbose
        #
        #
        #
        # set timeout:
        start_tis = time.time()
        signal.signal(signal.SIGALRM, swnda_timeout_handler)
        signal.alarm( argStruct.timeout )
        #
        #
        #
        # initialize to ok:
        returnCode = swn.ok
        summaryMSG = "data-access check ok"
        swn.log("N", 0, "Starting CMS data-access check of %s at %s UTC" %
                          (socket.gethostname(),
                   time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(start_tis))))
        swn.log("N", 1, "WN-25dataaccess version %s" % SWNDA_VERSION)
        swn.log("N", 2, "python version %d.%d.%d\n" % sys.version_info[0:3])
        #
        #
        #
        # check/print environment settings relevant for data-access:
        swn.log("N", 0, "\nInitial environment:")
        swnda_environment()
        #
        #
        # identify OS version:
        # ===============
        swn.log("N", 0, "\nIdentifying OS:")
        returnCode = swnda_osidentifier()
        swn.log("I", 1, "==> %s" % swn.code2name(returnCode) )
        if ( returnCode == swn.critical ):
            swn.exit(swn.critical, "OS identification failure")
        #
        if ( SWNDA_OSID not in SWNDA_CMSSW_LIST ):
            swn.log("N", 0, "No CMSSW configured for %s/%s data access" %
                                                 (SWNDA_OSID[0],SWNDA_OSID[1]))
            swn.exit(swn.warning, "no configuration for OS")
        #
        #
        # check/setup x509 environment:
        # =============================
        swn.log("N", 0, "\nCheck/setup of x509 authentication:")
        status = swnda_x509setup()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( status == swn.critical ):
            swn.exit(swn.critical, "x509 certificate issue")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
            summaryMSG = "x509 certificate issue"
        #
        #
        # check/setup IAM-issued token environment:
        # =========================================
        swn.log("N", 0, "\nCheck/setup of IAM-issued token authentication:")
        status = swnda_iamtoken()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( status == swn.critical ):
            swn.exit(swn.critical, "IAM-issued token issue")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
            summaryMSG = "IAM-issued token issue"
        #
        #
        # setup CMS environment:
        # ======================
        swn.log("N", 0, "\nCheck/setup of CMS environment:")
        status = swnda_cmssetup()
        swn.log("I", 1, "==> %s" % swn.code2name(status) )
        if ( status == swn.critical ):
            swn.exit(swn.critical, "CMS setup issue")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
            summaryMSG = "CMS setup issue"
        #
        #
        # read SITECONF and get storage.xml and storage.json data-access:
        # ===============================================================
        swn.log("N", 0, "\nFetching data-access information from SITECONF:")
        status = swnda_siteconf()
        swn.log("N", 1, "==> %s" % swn.code2name(status) )
        if ( status == swn.critical ):
            swn.exit(swn.critical, "SITECONF access issue")
        elif ( swn.code2order(status) > swn.code2order(returnCode) ):
            returnCode = status
            summaryMSG = "SITECONF access issue"
        #
        #
        # create a subdirectory for our test and cd into it:
        # ==================================================
        iniDir = os.getcwd()
        swn.log("N", 0, "\nCreating \"swnda\" subdirectory in %s:" % iniDir)
        if ( os.path.exists("swnda") ):
            swn.log("W", 1, "removing existing \"swnda\" path")
            shutil.rmtree("swnda")
        os.mkdir("swnda", mode=0o755)
        # cd into it:
        os.chdir("swnda")
        tstDir = os.getcwd()
        #
        #
        # pick a file in the SAM dataset:
        indxFile = int( len(SWNDA_FILES) * random.random() )
        #
        for tstCMSSW in SWNDA_CMSSW_LIST[SWNDA_OSID]:
            #
            #
            # check data-access in cmsRun of CMSSW version:
            now_tis = time.time()
            swn.log("N", 0, "\nChecking data-access in %s (@%d sec):" %
                                       (tstCMSSW[0], int(now_tis - start_tis)))
            status = swnda_cmsrun( tstCMSSW, SWNDA_FILES[indxFile] )
            swn.log("I", 1, "==> %s" % swn.code2name(status) )
            if ( swn.code2order(status) > swn.code2order(returnCode) ):
                returnCode = status
                summaryMSG = "%s cmsRun issue" % tstCMSSW[0]
            if ( status == swn.critical ):
                break
            #
            #
            # use next file in SAM dataset
            indxFile = ( indxFile + 1 ) % len(SWNDA_FILES)
        #
        #
        # change back to initial directory and delete test subdirectory:
        swn.log("N", 0, "\nRemoving \"swnda\" subdirectory:")
        os.chdir(iniDir)
        shutil.rmtree("swnda", ignore_errors=True)


        end_tis = time.time() 
        swn.log("I", 0, "\nEnding CMS data-access check at %s UTC (%d sec)" %
                     (time.strftime("%Y-%b-%d %H:%M:%S", time.gmtime(end_tis)),
                                                   int( end_tis - start_tis )))

    except TimeoutError as excptn:
        delta = time.time() - start_tis
        swn.log("E", 0, "Maximum execution time exceeded, %.3f sec" % delta)
        #
        returnCode = swn.error
        summaryMSG = "execution timeout"

    except Exception as excptn:
        swn.log("E", 0, "Execution failed, %s" % str(excptn))
        swn.log("E", 0, traceback.format_exc() )
        #
        returnCode = swn.error
        summaryMSG = "execution failure"


    swn.exit(returnCode, summaryMSG)
    #import pdb; pdb.set_trace()
