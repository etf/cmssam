#!/bin/sh
# ########################################################################### #
#
# SAM WorkerNode apptainer probe of CMS
#
# ########################################################################### #
# cancel timeout_handler if running:
trap '(/usr/bin/ps -p ${ALARM_PID}; if [ $? -eq 0 ]; then kill -ABRT ${ALARM_PID}; wait ${ALARM_PID}; fi) 1>/dev/null 2>&1' 0
# trap SIGALRM to handle timeouts properly:
trap 'swn_exit ${SWN_CRITICAL} "Execution timeout"' 14
#
#
#
if [ -z "${CVMFS_MOUNT_DIR}" ]; then
   CVMFS_MOUNT_DIR="/cvmfs"
fi
#
export PATH
#
#
SWNAT_VERSION="v1.01.05"
#
SWN_APPTNR_EXEC="${GLIDEIN_SINGULARITY_BINARY_OVERRIDE} \
                 ${CVMFS_MOUNT_DIR}/oasis.opensciencegrid.org/mis/singularity/current/bin/singularity \
                 `/usr/bin/which apptainer 2>/dev/null` \
                 `/usr/bin/which singularity 2>/dev/null` \
                 MODULE:singularitypro:singularity MODULE:singularity:singularity"
#
SWN_APPTNR_BIND="/cms /lfs_roots /storage \
                 /etc/cvmfs/SITECONF \
                 ${CVMFS_MOUNT_DIR}/grid.cern.ch/etc/grid-security:/etc/grid-security \
                 ${CVMFS_MOUNT_DIR}:/cvmfs /etc/hosts /etc/localtime"
#
SWN_APPTNR_IMAGE="${CVMFS_MOUNT_DIR}/singularity.opensciencegrid.org/cmssw/cms:rhel6"
#
SWN_APPTNR_CVMFS="${CVMFS_MOUNT_DIR}/oasis.opensciencegrid.org/mis/singularity/current/bin/singularity \
                  ${CVMFS_MOUNT_DIR}/grid.cern.ch/etc/grid-security \
                  ${CVMFS_MOUNT_DIR}/singularity.opensciencegrid.org/cmssw/cms:rhel7 \
                  ${CVMFS_MOUNT_DIR}/cms.cern.ch/cmsset_default.sh"
#
#
#
SWN_OK=0
SWN_WARNING=1
SWN_UNKNOWN=2
SWN_ERROR=3
SWN_CRITICAL=4
#
swn2sam () {
   if [ $1 -eq 0 ]; then
      echo 0
   elif [ $1 -eq 1 ]; then
      echo 1
   elif [ $1 -eq 3 -o $1 -eq 4 ]; then
      echo 2
   else
      echo 3
   fi
}
#
swn2psst () {
   if [ $1 -eq 4 ]; then
      echo 1
   else
      echo 0
   fi
}
#
swn2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Unknown" ;;
   3) echo "Error" ;;
   *) echo "Critical" ;;
   esac
}
#
sam2name () {
   case $1 in
   0) echo "Ok" ;;
   1) echo "Warning" ;;
   2) echo "Critical" ;;
   *) echo "Unknown" ;;
   esac
}
#
swn_exit () {
   if [ "${SWN_MODE}" = "sam" ]; then
      echo -e "\n</PRE>"
      #
      # switch stdout/stderr back:
      exec 1>&3 2>&4
      cd ${SWN_HOME}
      #
      # translate SWN code into SAM codes:
      MYRC=`swn2sam $1`
      #
      # print summary followed by detailed log:
      MYNAME=`sam2name ${MYRC}`
      echo -e "${MYNAME^^}: $2\n"
      if [ -f swnap.$$.log ]; then
         /bin/cat swnap.$$.log
         /bin/rm -f swnap.$$.log
      fi
      #
      exit ${MYRC}

   elif [ "${SWN_MODE}" = "psst" ]; then
      # translate return code into PSST codes:
      MYRC=`swn2psst $1`
      #
      exit ${MYRC}

   else
      exit $1 
   fi
}
#
timeout_handler () {
   /bin/sleep ${SWN_TIMEOUT}
   echo -e "\ntimeout of ${SWN_TIMEOUT} seconds reached\n"
   ALARM_PID=${BASHPID}
   PID_LIST=`/bin/ps --ppid $1 -o pid= | /bin/grep -v ${ALARM_PID}`
   if [ -n "${PID_LIST}" ]; then
      kill -9 ${PID_LIST//$'\n'/ }
   fi
   kill -ALRM $1
}
#
SWNRC=${SWN_OK}
SUMMRY="Apptainer check ok"
# ########################################################################### #



# handle command line options, setup timeout, and print basic information:
if [ -n "${SAME_SENSOR_HOME}" ]; then
   SWN_MODE="sam"
   SWN_VERBOSE=2
else
   SWN_MODE="interactive"
   SWN_VERBOSE=0
fi
SWN_TIMEOUT=180
IARG=1
while [ ${IARG} -le $# ]; do
   case "${!IARG}" in
   -S) SWN_MODE="sam"
       SWN_VERBOSE=2
       ;;
   -P) SWN_MODE="psst"
       SWN_VERBOSE=-1
       ;;
   -t) IARG=`expr ${IARG} + 1`
       SWN_TIMEOUT=${!IARG}
       ;;
   -v) SWN_VERBOSE=`expr ${SWN_VERBOSE} + 1`
       ;;
   -vv) SWN_VERBOSE=2
       ;;
   -vvv) SWN_VERBOSE=3
       ;;
   -vvvv) SWN_VERBOSE=4
       ;;
   *) echo "usage: $0 [-h] [-S] [-P] [-t TIMEOUT] [-v]"
      echo -e "\nCMS SAM WorkerNode Apptainer probe\n\noptional arguments:"
      echo "  -h, --help  show this help message and exit"
      echo "  -S          ETF configuration, SAM mode"
      echo "  -P          fast, minimal-output probing, PSST mode"
      echo "  -t TIMEOUT  maximum time probe is allowed to take in seconds [180]"
      echo "  -v          increase verbosity"
      exit 0
   esac
   IARG=`expr ${IARG} + 1`
done
#
if [ "${SWN_MODE}" = "sam" ]; then
   # switch stdout/stderr to file (for later output after summary message):
   SWN_HOME=`/bin/pwd`
   exec 3>&1 4>&2 1>swnap.$$.log 2>&1
   #
   echo "<PRE>"
fi
#
timeout_handler $$ &
ALARM_PID=$!
#
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   TMP1=`/bin/hostname -f`
   TMP2=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   echo -e "\nStarting CMS Apptainer check of ${TMP1} at ${TMP2} UTC"
   echo -e "   WN-05apptainer version ${SWNAT_VERSION}"
   unset TMP2
   unset TMP1
   SWN_START=`/bin/date -u +'%s' 2>/dev/null`
fi
#
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   echo -e "\n\nenvironment information:"
   echo "------------------------"
   /bin/uname -a
   echo ""
   if [ "${GLIDEIN_SINGULARITY_BINARY_OVERRIDE+x}" = "x" ]; then
      echo "env.variable GLIDEIN_SINGULARITY_BINARY_OVERRIDE is set to \"${GLIDEIN_SINGULARITY_BINARY_OVERRIDE}\""
      if [ ${SWN_VERBOSE} -ge 2 ]; then
         /bin/ls -ld ${GLIDEIN_SINGULARITY_BINARY_OVERRIDE}
      fi
   else
      echo "env.variable GLIDEIN_SINGULARITY_BINARY_OVERRIDE is not set"
   fi
   echo ""
   if [ "${APPTAINER_BINDPATH+x}" = "x" ]; then
      echo "env.variable APPTAINER_BINDPATH is set to \"${APPTAINER_BINDPATH}\""
      export APPTAINER_BINDPATH
   elif [ "${SINGULARITY_BINDPATH+x}" = "x" ]; then
      echo "env.variable SINGULARITY_BINDPATH is set to \"${SINGULARITY_BINDPATH}\""
      export SINGULARITY_BINDPATH
   else
      echo "env.variable APPTAINER_BINDPATH and SINGULARITY_BINDPATH are not set"
   fi
   echo ""
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo "env.variable PATH includes"
      echo -e "   ${PATH//:/$'\n   '}"
   fi
   echo ""
   /usr/bin/which apptainer singularity
   echo ""
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
#
SWN_APPTNR_TEST=""
if [ -x /usr/bin/modulecmd ]; then
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      echo "env.modules is installed"
   fi
   SWN_AVAIL=`/usr/bin/modulecmd sh -t avail 2>&1`
   if [ ${SWN_VERBOSE} -ge 3 ]; then
      echo -e "      ${SWN_AVAIL//$'\n'/$'\n      '}"
   fi
   for MY_BIN in ${SWN_APPTNR_EXEC}; do
      if [ "${MY_BIN%%:*}" != "MODULE" ]; then
         SWN_APPTNR_TEST="${SWN_APPTNR_TEST} ${MY_BIN}"
      else
         MY_MOD="${MY_BIN#MODULE:}"
         MY_MOD="${MY_MOD%:*}"
         echo "${SWN_AVAIL}" | /bin/grep "^${MY_MOD}" 1>/dev/null 2>&1
         if [ $? -eq 0 ]; then
            if [ ${SWN_VERBOSE} -ge 1 ]; then
               echo "   ${MY_MOD} available"
            fi
            SWN_APPTNR_TEST="${SWN_APPTNR_TEST} ${MY_BIN}"
         elif [ ${SWN_VERBOSE} -ge 2 ]; then
            echo "   ${MY_MOD} does not exist"
         fi
      fi
   done
   unset SWN_AVAIL
else
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo "no env.modules installed"
   fi
   for MY_BIN in ${SWN_APPTNR_EXEC}; do
      if [ "${MY_BIN%%:*}" != "MODULE" ]; then
         SWN_APPTNR_TEST="${SWN_APPTNR_TEST} ${MY_BIN}"
      fi
   done
fi
SWN_APPTNR_TEST="${SWN_APPTNR_TEST# }"
# ########################################################################### #



# check CVMFS areas are accessible:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nchecking CVMFS areas (@${MY_TMP} sec):"
   echo "------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
for MY_PATH in ${SWN_APPTNR_CVMFS}; do
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      /usr/bin/timeout 30 /bin/ls -ld ${MY_PATH}
      MY_RC=$?
   else
      /usr/bin/timeout 30 /bin/ls -ld ${MY_PATH} 1>/dev/null 2>&1
      MY_RC=$?
   fi
   if [ ${MY_RC} -eq 124 ]; then
      echo "${MY_PATH} timed out"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="CVMFS access timeout"
      fi
   elif [ ${MY_RC} -ne 0 ]; then
      echo "${MY_PATH} listing failed"
      if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="CVMFS access failure"
      fi
   fi
done
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
# ########################################################################### #



# check bind areas exist and remove from bind list otherwise:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nchecking bind paths exists on host (@${MY_TMP} sec):"
   echo "--------------------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
SWN_BIND=""
for MY_BIND in ${SWN_APPTNR_BIND}; do
   if [ ! -e ${MY_BIND%%:*} ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "   no path ${MY_BIND%%:*} on host, removing"
      fi
   else
      SWN_BIND="${SWN_BIND},${MY_BIND}"
      if [ ${SWN_VERBOSE} -ge 2 ]; then
         echo "   path ${MY_BIND%%:*} exists, adding to bind option"
      fi
   fi
done
# ########################################################################### #



# make subdirectory for Apptainer execution and bind x509/tokens into it:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\npreparing execution subdirectory (@${MY_TMP} sec):"
   echo "------------------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
umask 0022
if [ -e swnap -a ! -d swnap ]; then
   /bin/rm -rf swnap
   /bin/mkdir swnap
elif [ ! -d swnap ]; then
   /bin/mkdir swnap
else
   /bin/rm -rf swnap/cmd.sh 1>/dev/null 2>&1
fi
/bin/touch swnap/cmd.sh
echo "#!/bin/sh" 1>>swnap/cmd.sh
echo "#" 1>>swnap/cmd.sh
echo "umask 022" 1>>swnap/cmd.sh
echo "TZ=UTC;export TZ" 1>>swnap/cmd.sh
echo "LC_COLLATE=C;export LC_COLLATE" 1>>swnap/cmd.sh
echo "#" 1>>swnap/cmd.sh
#
if [ -f "${X509_USER_PROXY}" ]; then
   /bin/touch swnap/user_proxy.x509
   SWN_BIND="${SWN_BIND},${X509_USER_PROXY}:/srv/user_proxy.x509"
   echo "X509_USER_PROXY=/srv/user_proxy.x509;export X509_USER_PROXY" 1>>swnap/cmd.sh
elif [ -f /tmp/x509up_u`/usr/bin/id -u` ] ; then
   SWN_BIND="${SWN_BIND},/tmp/x509up_u`/usr/bin/id -u`:/srv/user_proxy.x509"
   echo "X509_USER_PROXY=/srv/user_proxy.x509;export X509_USER_PROXY" 1>>swnap/cmd.sh
fi
if [ -f "${BEARER_TOKEN_FILE}" ]; then
   SWN_BIND="${SWN_BIND},${BEARER_TOKEN_FILE}:/srv/user_token.b64"
   echo "BEARER_TOKEN_FILE=/srv/user_token.b64;export BEARER_TOKEN_FILE" 1>> swnap/cmd.sh
fi
echo "#" 1>>swnap/cmd.sh
#
# Apptainer startup validation section:
echo "/bin/ls -l /proc/\${PPID}/exe" 1>>swnap/cmd.sh
echo "echo \"\"" 1>>swnap/cmd.sh
echo "#" 1>>swnap/cmd.sh
echo "echo -e \"\nSWNAP \`/bin/pwd\` SWNAP\n\"" 1>>swnap/cmd.sh
echo "#" 1>>swnap/cmd.sh
echo "if [ -n \"\${SINGULARITY_CONTAINER}\" ]; then" 1>>swnap/cmd.sh
echo "   echo \"SINGULARITY_CONTAINER is set\"" 1>>swnap/cmd.sh
echo "   #" 1>>swnap/cmd.sh
echo "   if [ -e /proc/self/uid_map ]; then" 1>>swnap/cmd.sh
echo "      /usr/bin/awk '{if(\$2==0){print \"SWNAP privileged mode\"}else{if(\$1!=0){print \"SWNAP unprivileged mode\"}else{print \"SWNAP fakeroot mode\"}}}' /proc/self/uid_map" 1>>swnap/cmd.sh
echo "   else" 1>>swnap/cmd.sh
echo "      echo -e \"\nSWNAP privileged mode\"" 1>>swnap/cmd.sh
echo "   fi" 1>>swnap/cmd.sh
echo "   echo \"\"" 1>>swnap/cmd.sh
echo "fi" 1>>swnap/cmd.sh
echo "#" 1>>swnap/cmd.sh
echo "exit 0" 1>>swnap/cmd.sh
#
/bin/chmod a+x swnap/cmd.sh
#
if [ ${SWN_VERBOSE} -ge 1 ]; then
   /bin/ls -l swnap
   if [ ${SWN_VERBOSE} -ge 4 ]; then
      /bin/cat swnap/cmd.sh
   fi
fi
# ########################################################################### #



# function to clear some env.variables before invoking Apptainer:
swn_env_clear() {
   PATH="/usr/bin:/bin";export PATH
   #
   unset TMP
   unset TMPDIR
   unset TEMP
   unset TEMPDIR
   unset OSG_WN_TMP
   #
   unset X509_USER_PROXY
   unset X509_USER_KEY
   unset X509_USER_CERT
   unset X509_CERT_DIR
   #
   unset XDG_RUNTIME_DIR
   #
   unset LD_LIBRARY_PATH   
   unset LD_PRELOAD   
   #
   unset PYTHONPATH
   #
   unset PERL5LIB
   #
   unset GFAL_CONFIG_DIR
   unset GFAL_PLUGIN_DIR
   #
   unset SCRAM_ARCH
   #
   TZ="UTC";export TZ
}
# ########################################################################### #



# loop over potential Apptainer binaries and take first working one:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   echo -e "\n\nlooking for working apptainer/singularity binary:"
   echo "-------------------------------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
SWN_APPTNR_BINARY=""
SWN_OPTIONS="--home `/bin/pwd`/swnap:/srv --pwd /srv --bind ${SWN_BIND#,} --ipc --contain --pid"
#
for MY_BIN in ${SWN_APPTNR_TEST}; do
   if [ ${SWN_VERBOSE} -ge 1 ]; then
      MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
      MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
      echo -e "trying ${MY_BIN} (@${MY_TMP} sec):"
   fi
   #
   if [ "${MY_BIN%%:*}" = "MODULE" ]; then
      MY_TMP="${MY_BIN#MODULE:}"
      MY_MOD="${MY_TMP%:*}"
      /usr/bin/modulecmd sh load ${MY_MOD}
      MY_CMD="${MY_TMP#*:}"
      MY_BIN=`/usr/bin/which ${MY_CMD}`
      if [ $? -ne 0 -o -z "${MY_BIN}" ]; then
         if [ ${SWN_VERBOSE} -ge 0 ]; then
            echo "   no ${MY_CMD} in PATH after ${MY_MOD} load"
         fi
         continue
      fi
   fi
   #
   if [ ${SWN_VERBOSE} -ge 3 ]; then
      echo -e "\n   ${MY_BIN} -d exec ${SWN_OPTIONS} ${SWN_APPTNR_IMAGE} /srv/cmd.sh\n"
      SWN_OUTPUT=`(swn_env_clear; /usr/bin/timeout -k 5 120 ${MY_BIN} -d exec ${SWN_OPTIONS} ${SWN_APPTNR_IMAGE} /srv/cmd.sh) 2>&1`
      MY_RC=$?
   else
      if [ ${SWN_VERBOSE} -ge 2 ]; then
         echo -e "\n   ${MY_BIN} exec ${SWN_OPTIONS} ${SWN_APPTNR_IMAGE} /srv/cmd.sh\n"
      fi
      SWN_OUTPUT=`(swn_env_clear; /usr/bin/timeout -k 5 120 ${MY_BIN} exec ${SWN_OPTIONS} ${SWN_APPTNR_IMAGE} /srv/cmd.sh) 2>&1`
      MY_RC=$?
   fi
   if [ ${MY_RC} -eq 124 -o ${MY_RC} -eq 137 ]; then
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "   timed out"
      elif [ ${SWN_VERBOSE} -eq 0 ]; then
         echo "${MY_BIN} timed out"
      fi
   elif [ ${MY_RC} -ne 0 ]; then
      echo -e "      ${SWN_OUTPUT//$'\n'/$'\n      '}"
      if [ ${SWN_VERBOSE} -ge 1 ]; then
         echo "   failed with rc=${MY_RC}"
      elif [ ${SWN_VERBOSE} -eq 0 ]; then
         echo "${MY_BIN} failed with rc=${MY_RC}"
      fi
   else
      echo "${SWN_OUTPUT}" | /bin/grep '^SWNAP /srv SWNAP$' 1>/dev/null 2>&1
      if [ $? -ne 0 ]; then
         if [ ${SWN_VERBOSE} -ge 2 ]; then
            echo -e "      ${SWN_OUTPUT//$'\n'/$'\n      '}"
         fi
         if [ ${SWN_VERBOSE} -ge 1 ]; then
            echo "   did not produce expected output"
         elif [ ${SWN_VERBOSE} -eq 0 ]; then
            echo "${MY_BIN} did not produce expected output"
         fi
      else
         if [ ${SWN_VERBOSE} -ge 2 ]; then
            echo -e "      ${SWN_OUTPUT//$'\n'/$'\n      '}"
         fi
         if [ ${SWN_VERBOSE} -ge 1 ]; then
            echo "   succeeded"
         fi
         SWN_APPTNR_BINARY=${MY_BIN}
         break
      fi
   fi
   if [ ${SWN_VERBOSE} -ge 2 ]; then
      echo ""
   fi
done
if [ -z "${SWN_APPTNR_BINARY}" ]; then
   echo "[C] no working Apptainer/Singularity found"
   if [ ${SWNRC} -ne ${SWN_CRITICAL} ]; then
         SWNRC=${SWN_CRITICAL}
         SUMMRY="no Apptainer/Singularity"
   fi
   #
fi
if [ ${SWNRC} -eq ${SWN_CRITICAL} ]; then
   swn_exit ${SWNRC} "${SUMMRY}"
fi
# ########################################################################### #



# get Apptainer/Singularity version:
if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_NOW=`/bin/date -u +'%s' 2>/dev/null`
   MY_TMP=`echo "${MY_NOW} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nquerying version (@${MY_TMP} sec):\n--------------------------"
elif [ ${SWN_VERBOSE} -eq 1 ]; then
   echo ""
fi
if [ ${SWN_VERBOSE} -ge 1 ]; then
   ${SWN_APPTNR_BINARY} --version 2>/dev/null
fi
# ########################################################################### #



if [ ${SWN_VERBOSE} -ge 2 ]; then
   MY_END=`/bin/date -u +'%s' 2>/dev/null`
   TMP1=`/bin/date -u +'%Y-%b-%d %H:%M:%S' 2>/dev/null`
   TMP2=`echo "${MY_END} - ${SWN_START}" | /usr/bin/bc`
   echo -e "\n\nEnding CMS Apptainer check at ${TMP1} UTC (${TMP2} sec)"
   MY_TMP=`swn2name ${SWNRC}`
   echo "   ${MY_TMP}: ${SUMMRY}"
else
   MY_TMP=`swn2name ${SWNRC}`
   echo "${MY_TMP^^}: ${SUMMRY}"
fi
swn_exit ${SWNRC} "${SUMMRY}"
