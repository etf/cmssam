#!/bin/sh
#
# Update WMCore
#

GIT_REPO="https://github.com/dmwm/WMCore.git"
TMPDIR=$(mktemp -dt XXXX)
BASEDIR=$PWD

git clone -q --depth 1 ${GIT_REPO} ${TMPDIR}

(cd $TMPDIR && python setup.py install_system -s wmc-runtime --prefix=$BASEDIR)
cp -r $TMPDIR/src/python/WMCore/Storage $BASEDIR/lib/python2.6/site-packages/WMCore
cp $TMPDIR/src/python/WMCore/Algorithms/ParseXMLFile.py $BASEDIR/lib/python2.6/site-packages/WMCore/Algorithms

rm -rf $BASEDIR/bin $TMPDIR
find $BASEDIR -name '*.pyc' -delete
