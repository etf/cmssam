FROM gitlab-registry.cern.ch/etf/docker/etf-exp:qa

ENV NSTREAM_ENABLED=0

# OSG Middleware
#RUN yum -y install yum-priorities
#RUN yum -y install //repo.opensciencegrid.org/osg/3.6/osg-3.6-el9-release-latest.rpm
# RUN rpm -Uvh https://repo.opensciencegrid.org/osg/3.4/osg-3.4-el7-release-latest.rpm
#RUN sed "7i priority=99" -i /etc/yum.repos.d/epel.repo

# FIX: gfal2 (osg override)
# RUN cd /etc/yum.repos.d && wget http://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo
# RUN sed "7i priority=97" -i /etc/yum.repos.d/dmc-el7.repo

# FIX: condor client (osg override)
#RUN yum -y install https://research.cs.wisc.edu/htcondor/repo/10.0/htcondor-release-current.el7.noarch.rpm

# FIX: oidc-agent (epel override)
# RUN cd /etc/yum.repos.d && wget https://repo.data.kit.edu/data-kit-edu-centos7.repo
# RUN sed "7i priority=97" -i /etc/yum.repos.d/data-kit-edu-centos7.repo

# Core deps
RUN yum -y -x 'oidc-agent-libs-4.5.2*' install voms voms-clients-java oidc-agent-cli

# Condor client
RUN yum -y install condor condor-python
# HTCondor-CE client
RUN yum -y install htcondor-ce-client
COPY ./docker/etf-cms/config/ce-client.conf /etc/condor-ce/config.d/ce-client.conf

# Xroot
# osg-testing enabled to get xroot 5.5.3 (to be removed once in prod)
RUN yum -y --enablerepo=osg-testing install xrootd-python xrootd-client xrootd-libs xrootd-client-libs python3-xrootd


ADD https://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo /etc/yum.repos.d/dmc-el7.repo
# SRM todo: test removing globus deps
RUN yum -y install python2-gfal2 python2-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp gfal2-plugin-xrootd


# HTTP/webdav
RUN yum -y install python3-gfal2 python3-nap python36-pyOpenSSL

# ARC
RUN rpm -ivh https://download.nordugrid.org/packages/nordugrid-release/releases/6/centos/el7/x86_64/nordugrid-release-6-1.el7.noarch.rpm
RUN yum -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus
# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
COPY docker/etf-cms/config/client.conf /opt/omd/sites/$CHECK_MK_SITE/.arc/
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY docker/etf-cms/config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile
# IAM VOMS
RUN mkdir -p /etc/vomses
COPY docker/etf-cms/config/voms-cms-auth.app.cern.ch.vomses /etc/vomses

# ETF base plugins
RUN yum -y update python-nap ncgx python-vofeed-api nagios-stream
RUN yum -y install python-jess python-wnfm nagios-plugins-globus nagios-plugins-tokens nagios-plugins python-GridMon
RUN yum -y install python36-pexpect python36-ptyprocess


# ETF WN-qFM payload
#RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
#RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
#RUN cp -r /usr/lib/python2.7/site-packages/pexpect.py /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN cp -r /usr/lib/python2.7/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN cp -r /usr/lib/python3.6/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN cp -r /usr/lib/python3.6/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN cp -r /usr/lib/python2.7/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
#RUN cp /usr/lib/python2.7/site-packages/argparse.py /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages/

# ETF streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY docker/etf-cms/config/ocsp_handler.cfg /etc/nstream/

# CMS config
COPY docker/etf-cms/config/cms_checks.cfg /etc/ncgx/conf.d/
COPY nagios/config/etf_plugin_cms.py /usr/lib/ncgx/x_plugins/
COPY nagios/config/wlcg_cms.cfg /etc/ncgx/metrics.d/

# CMS payload
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.cms/wnjob
COPY SiteTests/SE/* /usr/libexec/grid-monitoring/probes/org.cms/
COPY nagios/config/org.cms.lcgadmin /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.lcgadmin
COPY nagios/config/org.cms.production /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.production
COPY nagios/org.cms.glexec /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec
COPY SiteTests/MonteCarlo /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/
COPY SiteTests/testjob/tests /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests
COPY SiteTests/FroNtier/tests /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests
COPY SiteTests/WN /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests
COPY nagios/config/cms_glexec-etf /etc/cron.d/cms_glexec

# ETF config
#COPY ./config/service_template.tpl /etc/ncgx/templates/
COPY docker/etf-cms/config/ncgx.cfg /etc/ncgx/

EXPOSE 443 6557
COPY docker/etf-cms/docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
