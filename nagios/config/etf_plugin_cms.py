import logging
import json

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

SAME_CODES = {'OK': 10, 'INFO': 20, 'NOTICE': 30, 'WARNING': 40, 'ERROR': 50, 'CRITICAL': 60, 'MAINTENANCE': 100}

FLAVOR_MAP = {'CREAM-CE': 'cream',
              'ARC-CE': 'nordugrid',
              'HTCONDOR-CE': 'condor',
              'GLOBUS': 'gt',
              'OSG-CE': 'gt'}

CE_PING = (
    'org.cms.CONDOR-Ping-/cms-ce-token',
)

XROOT_TOKEN_METRICS = (
    "org.cms.SE-XRootD-1connection",
    "org.cms.SE-XRootD-3version",
    "org.cms.SE-XRootD-4crt-read",
    "org.cms.SE-XRootD-5crt-contain",
    "org.cms.SE-XRootD-6crt-access",
    "org.cms.SE-XRootD-7crt-write",
    "org.cms.SE-XRootD-8crt-directory",
    "org.cms.SE-XRootD-9federation",
    "org.cms.SE-XRootD-14tkn-read",
    "org.cms.SE-XRootD-15tkn-contain",
    "org.cms.SE-XRootD-16tkn-access",
    "org.cms.SE-XRootD-17tkn-write",
    "org.cms.SE-XRootD-18tkn-directory"
)

WEBDAV_METRICS = (
    'org.cms.SE-WebDAV-1connection',
    'org.cms.SE-WebDAV-2ssl',
    'org.cms.SE-WebDAV-3crt_extension',
    'org.cms.SE-WebDAV-4crt-read',
    'org.cms.SE-WebDAV-6crt-access',
    'org.cms.SE-WebDAV-7crt-write',
    'org.cms.SE-WebDAV-8crt-directory',
    'org.cms.SE-WebDAV-10macaroon',
    'org.cms.SE-WebDAV-14tkn-read',
    'org.cms.SE-WebDAV-16tkn-access',
    'org.cms.SE-WebDAV-17tkn-write',
    'org.cms.SE-WebDAV-18tkn-directory'
)

GSIFTP_METRICS = (
    'org.cms.SE-GSIftp-1connection',
    'org.cms.SE-GSIftp-2ssl',
    'org.cms.SE-GSIftp-4crt-read',
    'org.cms.SE-GSIftp-5open-access',
    'org.cms.SE-GSIftp-6crt-write'
)

WN_METRICS = {
    'WN-cvmfs': 'org.cms.WN-cvmfs-/cms/Role=lcgadmin',
    'CE-cms-analysis.sing': 'org.cms.WN-analysis-/cms/Role=lcgadmin',
    'CE-cms-singularity': 'org.cms.WN-isolation-/cms/Role=lcgadmin',
    'CE-cms-basic.sing': 'org.cms.WN-basic-/cms/Role=lcgadmin',
    'CE-cms-env': 'org.cms.WN-env-/cms/Role=lcgadmin',
    'CE-cms-frontier.sing': 'org.cms.WN-frontier-/cms/Role=lcgadmin',
    'CE-cms-mc.sing': 'org.cms.WN-mc-/cms/Role=lcgadmin',
    'CE-cms-squid.sing': 'org.cms.WN-squid-/cms/Role=lcgadmin',
    'CE-cms-xrootd-access.sing': 'org.cms.WN-xrootd-access-/cms/Role=lcgadmin',
    'CE-cms-xrootd-fallback.sing': 'org.cms.WN-xrootd-fallback-/cms/Role=lcgadmin',
    'WN-psst-test': 'org.cms.WN-psst-test-/cms/Role=lcgadmin'
}

NWN_METRICS = {
    'wn_basic.sh': 'org.cms.WN-01basic-/cms-ce-token',
    'wn_cvmfs.sh': 'org.cms.WN-02cvmfs-/cms-ce-token',
    'wn_siteconf.py': 'org.cms.WN-03siteconf-/cms-ce-token',
    'wn_apptainer.sh': 'org.cms.WN-05apptainer-/cms-ce-token',
    'wn_runsum.py': 'org.cms.WN-99summary-/cms-ce-token'
}

NWN_PASSIVE = {
    'wn_squid': 'org.cms.WN-21squid-/cms-ce-token',
    'wn_frontier': 'org.cms.WN-22frontier-/cms-ce-token',
    'wn_dataaccess': 'org.cms.WN-25dataaccess-/cms-ce-token'
}

# legacy SFT/SAME metrics which are not nagios compliant
WN_METRICS_LEGACY = [x for x in WN_METRICS.keys() if 'CE' in x]


def run(url, ipv6=False):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add host groups
    sites = feed.get_groups("CMS_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.items():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    # c.add_all(CE_PING, tags=["HTCONDOR-CE"])
    c.add_all(WN_METRICS.values(), tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(NWN_METRICS.values(), tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(NWN_PASSIVE.values(), tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(GSIFTP_METRICS, tags=["SRM"])
    c.add_all(XROOT_TOKEN_METRICS, tags=["XROOTD"])
    c.add_all(WEBDAV_METRICS, tags=["WEBDAV"])
    # IPv6
    c.add("org.cms.DNS-IPv6", tags=["SRM"], params={'extends': 'check_dig'})
    # Special proxy
    c.add("org.globus.GridProxy-Get-/cms/Role=production", hosts=("localhost",))
    c.add("org.globus.GridProxy-Valid-/cms/Role=production", hosts=("localhost",))

    # XRoot
    for service in services:
        flavor = service[1]
        if flavor not in ["XROOTD"]:
            continue
        host = service[0]
        endpoint = service[2]
        site = hg.exact_match(host)
        if not site:
            log.warning("Unable to find site for host %s, skipping" % host)
            continue
        if len(site) > 1:
            log.warning("Host assigned to multiple sites: %s, %s picking first one" % (host, site))

    # XRoot with SciTokens
    for service in services:
        flavor = service[1]
        if flavor not in ["XROOTD"]:
            continue
        host = service[0]
        endpoint = service[2]
        site = hg.exact_match(host)
        se_resources = feed.get_se_resources(host, flavor)
        args_dicts = dict()

        if se_resources:
            targets = ''
            for en in se_resources:
                targets += '-T {} {} '.format(en[0], en[1])
            if targets:
                args_dicts['-T'] = targets[2:]    # removes initial -T
        if ipv6:
            args_dicts['-6'] = ''
        else:
            args_dicts['-4'] = ''
        args_dicts['-E'] = endpoint
        args_dicts['-S'] = site.pop()
        c.add("org.cms.SE-XRootD-99summary", hosts=(host,), params={'args': args_dicts, '_tags': 'XROOTD'})

    # WEBDAV, GSIFTP
    for service in services:
        flavor = service[1]
        if flavor not in ["WEBDAV", "SRM"]:
            continue
        host = service[0]
        endpoint = service[2]
        se_resources = feed.get_se_resources(host, flavor)
        args_dicts = dict()

        if se_resources:
            targets = ''
            for en in se_resources:
                targets += '-T {} {} '.format(en[0], en[1])
            if targets:
                args_dicts['-T'] = targets[2:]    # removes initial -T
        if ipv6:
            args_dicts['-6'] = ''
        else:
            args_dicts['-4'] = ''
        args_dicts['-E'] = endpoint

        if flavor == 'WEBDAV':
            c.add("org.cms.SE-WebDAV-99summary", hosts=(host,), params={'args': args_dicts, '_tags': flavor})
        if flavor == 'SRM':
            c.add("org.cms.SE-GSIftp-9summary", hosts=(host,), params={'args': args_dicts, '_tags': flavor})

    # ETF env - environment variables to export on the worker node (global for all sites), such as:
    # ETF_TESTS - points to a list of WN tests to execute (stored in WN_METRICS)
    # ETF_LEGACY should be a subset of ETF_TESTS that identifies SFT tests (those that are not nagios compliant)
    # SAME* environment needed by the legacy/SFT tests
    with open('/tmp/etf-env.sh', 'w') as etf_env:
        etf_env.write('ETF_TESTS={}\n'.format(
            ','.join(['etf/probes/org.cms/testjob/tests/' + m for m in WN_METRICS.keys()])))
        etf_env.write('ETF_LEGACY={}\n'.format(
            ','.join(['etf/probes/org.cms/testjob/tests/' + m for m in WN_METRICS_LEGACY])))
        for code, value in SAME_CODES.items():
            etf_env.write('SAME_{}={}\n'.format(code, value))
        etf_env.write('SAME_VO=cms\n')
        etf_env.write('SAME_TEST_DIRNAME=$ETFROOT/probes/org.cms/testjob/tests\n')
        etf_env.write('SAME_SENSOR_HOME=$ETFROOT/probes/org.cms/testjob\n')

    # ETF env - new worker node probes env. setup
    # (once old wn tests are retired L195-208 can be deleted)
    with open('/tmp/etf-env-new.sh', 'w') as etf_env:
        etf_tests = ['etf/probes/org.cms/testjob/tests/' + p for p in sorted(NWN_METRICS.keys(), key=lambda m: NWN_METRICS[m])]
        etf_env.write('ETF_TESTS={}\n'.format(','.join(etf_tests)))
        # etf_env.write('ETF_LEGACY={}\n'.format(','.join(etf_tests)))
        etf_env.write('SAME_SENSOR_HOME=$ETFROOT/probes/org.cms/testjob\n')

    # ETF WN-qFM config - maps WN scripts back to metrics (WN-cvmfs -> org.lhcb.WN-cvmfs-/lhcb/Role=production)
    with open('/tmp/etf_wnfm.json', 'w') as etf_wnfm:
        json.dump({'wn_metric_map': WN_METRICS, 'counter_enabled': True}, etf_wnfm)
    # ETF WN-qFM config - new reverse mapping for new WN tests
    #                   - removes .sh/.py extensions + adds passive metrics
    # (once old wn tests are retired L217-219 can be deleted)
    with open('/tmp/etf_wnfm_new.json', 'w') as etf_wnfm:
        wn_metrics_map = dict()
        #for k, v in NWN_METRICS.items():
        #    wn_metrics_map[k.replace('.sh', '').replace('.py', '')] = v
        wn_metrics_map.update(NWN_METRICS)
        wn_metrics_map.update(NWN_PASSIVE)
        json.dump({'wn_metric_map': wn_metrics_map, 'counter_enabled': False}, etf_wnfm)

    # Queues
    for service in services:
        host = service[0]
        flavor = service[1]
        endpoint = service[2] if len(service) > 2 else None
        if flavor not in ["ARC-CE", "HTCONDOR-CE"]:
            continue
        if flavor == 'HTCONDOR-CE':
            # special handling for HTCONDOR-CE, no queues
            c.add('org.sam.CONDOR-JobState-/cms/Role=lcgadmin', hosts=(host,),
                  params={'args': {'--resource': 'htcondor://{}'.format(host),
                                   '--jdl-ads': '\'request_memory = 2000\' --jdl-ads \'+maxWallTime=30\''}})
            c.add('org.sam.CONDOR-JobSubmit-/cms/Role=lcgadmin', hosts=(host,), params={'_tags': flavor})
            # HTCONDOR-CE with scitoken-only
            c.add('org.sam.CONDOR-JobState-/cms-ce-token', hosts=(host,),
                  params={'args': {'--resource': 'htcondor://{}'.format(host),
                                   '--jdl-ads': '\'request_memory = 2000\' --jdl-ads \'+maxWallTime=30\''}})
            c.add('org.sam.CONDOR-JobSubmit-/cms-ce-token', hosts=(host,), params={'_tags': flavor})
            continue
        ce_resources = feed.get_ce_resources(host, flavor)
        if ce_resources:
            batch = ce_resources[0][0]
            queue = ce_resources[0][1]
            if not batch:
                batch = "nopbs"
            if flavor not in FLAVOR_MAP.keys():
                log.warning("Unable to determine type for flavour %s" % flavor)
                continue
            if endpoint:
	            res = "://%s/%s/%s/%s" % (endpoint, 'nosched', batch, queue)
            else:
                res = "://%s/%s/%s/%s" % (host, 'nosched', batch, queue)
        else:
            # no queues
            res = "://%s/%s/%s/%s" % (host, 'nosched', 'nobatch', 'noqueue')

        # ARC-CEs - tokens via HTCondor ARC backend
        c.add('org.sam.CONDOR-JobState-/cms-ce-token', hosts=(service[0],),
                params={'args': {'--resource': 'arc{}'.format(res), '--arc-rte': 'ENV/PROXY',
                                 '--jdl-ads': '\'request_memory = 2000\' --jdl-ads \'+maxWallTime=30\''}})
        c.add('org.sam.CONDOR-JobSubmit-/cms-ce-token', hosts=(service[0],), params={'_tags': flavor})

        # ARC-CEs - x509 via native ARC (a-rex/emies) backend
        # if host not in ARC_GSIFTP_LIST:  # ARC native client using A-REX except for ARC_GSIFTP_LIST
        if endpoint:
            c.add('org.sam.ARC-JobState-/cms/Role=lcgadmin', hosts=(service[0],),
                  params={'args': {'--resource': 'nordugrid{}'.format(res), '--arc-rsl': '\'(memory=2000)\'',
                                   '--arc-debug': 'VERBOSE',
                                   '--arc-ce': 'https://{}/arex'.format(endpoint)}})
        else:
            c.add('org.sam.ARC-JobState-/cms/Role=lcgadmin', hosts=(service[0],),
                  params={'args': {'--resource': 'nordugrid{}'.format(res), '--arc-rsl': '\'(memory=2000)\'',
                                   '--arc-debug': 'VERBOSE'}})
        c.add('org.sam.CONDOR-JobSubmit-/cms/Role=lcgadmin', hosts=(service[0],), params={'_tags': flavor})
        # else:  # ARC GSIFTP via HT-Condor for others
        #     c.add('org.sam.CONDOR-JobState-/cms/Role=lcgadmin', hosts=(service[0],),
        #           params={'args': {'--resource': 'nordugrid'.format(res), '--arc-rsl': '\'(memory=2000)\''}})

    c.serialize()
